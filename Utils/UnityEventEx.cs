using UnityEngine.Events;

namespace MGU {
    public static class UnityEventEx {
        public static void AddListenerSafe(this UnityEvent uEvent, UnityAction callback) {
            if (callback == null) {
                return;
            }
            
            uEvent.AddListener(callback);
        }
    }
}