﻿using UnityEngine;
using System.Collections.Generic;

namespace MGU {
    public static class Yielders {
        private static readonly Dictionary<float, WaitForSeconds> timeInterval =
                new Dictionary<float, WaitForSeconds>(100);

        private static readonly Dictionary<float, WaitForSecondsRealtime> timeIntervalRealTime =
                new Dictionary<float, WaitForSecondsRealtime>(100);

        public static WaitForEndOfFrame EndOfFrame { get; } = new WaitForEndOfFrame();
        public static WaitForFixedUpdate FixedUpdate { get; } = new WaitForFixedUpdate();

        public static WaitForSeconds WaitForSeconds(float seconds) {
            if (!timeInterval.ContainsKey(seconds)) {
                timeInterval.Add(seconds, new WaitForSeconds(seconds));
            }

            return timeInterval[seconds];
        }

        public static WaitForSecondsRealtime WaitForSecondsRealtime(float seconds) {
            if (!timeIntervalRealTime.ContainsKey(seconds)) {
                timeIntervalRealTime.Add(seconds, new WaitForSecondsRealtime(seconds));
            }

            return timeIntervalRealTime[seconds];
        }
    }
}