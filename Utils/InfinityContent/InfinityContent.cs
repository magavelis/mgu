﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using MGU;
using MGU.Common;

public class InfinityContent: MonoBehaviour {
    [SerializeField] private List<InfinityContentItem> itemList = new List<InfinityContentItem>();
    [SerializeField] private Transform target = null;
    [SerializeField] private Vector3 translateValue = Vector3.zero;
    private float TargetPosition => target.position.x;

    private Coroutine updater;
    private Coroutine animator;

    private Transform t;

    private void Awake() {
        t = transform;
    }
    
    private void OnDestroy() {
        StopUpdating();
    }

    public void Construct(Transform followObject) {
        target = followObject;
        
        StartAnimatingIfNeeded();
    }

    public void StartUpdating() {
        if (target.IsNull()) {
            Debug.LogError($"Can't start {nameof(InfinityContent)} updates. Unknown target.");
            return;
        }

        if (itemList.Count <= 1) {
            Debug.LogError($"Can't start {nameof(InfinityContent)} updates. Not enough content items.");
            return;
        }

        updater = StartCoroutine(IE_Update());
    }

    public void StopUpdating() {
        if (updater != null) {
            StopCoroutine(updater);
        }

        if (animator != null) {
            StopCoroutine(animator);
        }
    }

    private void StartAnimatingIfNeeded() {
        if (Mathf.Abs(translateValue.magnitude) < K.Math.Tolerance) {
            return;
        }

        animator = StartCoroutine(IE_Animate());
    }

    private IEnumerator IE_Animate() {
        while (ObjectEx.IsNotNull(target)) {
            t.Translate(translateValue);
            yield return Yielders.EndOfFrame;
        }
    }
    
    private IEnumerator IE_Update() {
        InfinityContentItem nextItem = NextContentItem(0.0f);
        while (ObjectEx.IsNotNull(target)) {
            if (TargetPosition >= nextItem.X) {
                MakeSwitch();
                nextItem = NextContentItem(TargetPosition);
            }

            yield return Yielders.WaitForSeconds(1f);
        }
    }

    private InfinityContentItem NextContentItem(float targetPosition) {
        InfinityContentItem currentItem = itemList
                .FirstOrDefault(item => item.IsCoveringPositionHorizontally(targetPosition));
        if (currentItem == null) {
            currentItem = itemList.FirstOrDefault();
        }

        InfinityContentItem nextItem = itemList.ElementAfterOrDefault(currentItem);
        return nextItem;
    }

    private void MakeSwitch() {
        List<InfinityContentItem> orderedItemList = itemList.OrderBy(item => item.MaxX).ToList();
        InfinityContentItem firstItem = orderedItemList.FirstOrDefault();
        InfinityContentItem lastItem = orderedItemList.LastOrDefault();
        if (firstItem == null || lastItem == null) {
            return;
        }

        firstItem.MoveAfter(lastItem);
    }
}