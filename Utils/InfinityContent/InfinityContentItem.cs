﻿using UnityEngine;

public class InfinityContentItem: MonoBehaviour {
    [SerializeField] private float size = 100;

    private Transform t;

    public float X => t.position.x;
    public float MaxX => X + size;
    
    private void Awake() {
        t = transform;
    }
    
    public bool IsCoveringPositionHorizontally(float position) {
        float halfSize = size / 2.0f;
        float x = X - halfSize;
        float maxX = MaxX - halfSize;
        return (position > x) && (position < maxX);
    }

    public void MoveAfter(InfinityContentItem item) {
        Vector3 position = t.position; 
        position.x = item.MaxX;
        t.position = position;
    }
}