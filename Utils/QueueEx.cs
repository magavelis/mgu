using System.Collections.Generic;

namespace MGU {
    public static class QueueEx {
        public static T DequeueOrDefault<T>(this Queue<T> queue) {
            return (queue.Count > 0) ? queue.Dequeue() : default;
        }
    }
}