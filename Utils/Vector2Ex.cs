﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace MGU {
    public static class Vector2Utils {
        public static Vector3 Lerp(Vector3 a, Vector3 b, float t) {
            Vector3 newVector = new Vector2(
                    MathUtils.Lerp(a.x, b.x, t),
                    MathUtils.Lerp(a.y, b.y, t));
            return newVector;
        }
        
        public static Vector2 NaN => new Vector3(float.NaN, float.NaN, float.NaN);
    }
    
    public static class Vector2Ex {
        public static Vector2 Average(this IEnumerable<Vector2> currentList) {
            Vector2 sum = Vector2.zero;
            List<Vector2> vectorList = currentList.ToList();
            vectorList.ForEach(item => sum += item);
            return sum / vectorList.Count;
        }

        public static Vector2 Direction(this Vector2 vector2) {
            float x = (vector2.x > 0) ? 1f : (vector2.x < 0) ? -1f : 0f;
            float y = (vector2.y > 0) ? 1f : (vector2.y < 0) ? -1f : 0f;
            return new Vector2(x, y);
        }

        public static Vector2 Abs(this Vector2 vector2) {
            return new Vector2(
                    Mathf.Abs(vector2.x),
                    Mathf.Abs(vector2.y)
            );
        }

        public static Vector2 Clamp01(this Vector2 vector2) {
            return new Vector2(
                    Mathf.Clamp01(vector2.x),
                    Mathf.Clamp01(vector2.y)
            );
        }

        public static Vector2 Clamp(this Vector2 vector2, Vector2 min, Vector2 max) {
            return new Vector2(
                    Mathf.Clamp(vector2.x, min.x, max.x),
                    Mathf.Clamp(vector2.y, min.y, max.y)
            );
        }

        public static Vector2 Remap01(this Vector2 vector2, Vector2 from, Vector2 to) {
            return new Vector2(
                    MathUtils.Remap01(vector2.x, from.x, to.x),
                    MathUtils.Remap01(vector2.y, from.y, to.y)
            );
        }
    }
}