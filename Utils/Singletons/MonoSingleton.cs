using System;
using UnityEngine;

public abstract class MonoSingleton<T>: MonoBehaviour where T : MonoSingleton<T> {
    private static T instance;
    public static T Default {
        get {
            if (instance == null) {
                instance = FindObjectOfType(typeof(T)) as T;
                if (instance == null) {
                    GameObject gameObject = new GameObject(typeof(T).Name);
                    instance = gameObject.AddComponent<T>();
                }

                if (instance == null) {
                    Debug.LogError($"Problem during the creation of {typeof(T).Name}");
                } else {
                    instance.KeepAliveIfNeeded();
                }
            }
            return instance;
        }
    }

    private bool isApplicationRunning = true;
    
    // If no other MonoBehaviour request the instance in an awake function
    // executing before this one, no need to search the object.
    private void Awake() {
        if (instance == null) {
            CreateSingleton();
            KeepAliveIfNeeded();
        } else if (instance == this) {
        } else {
            DestroyDuplicate();
        }
    }

    private void OnDestroy() {
        if (isApplicationRunning == false) {
            DestroySingleton();
        }
    }

    private void OnApplicationQuit() {
        isApplicationRunning = false;
    }

    private void CreateSingleton() {
        instance = this as T;
        OnSingletonCreate();
    }
    
    private void DestroySingleton() {
        instance = null;
        OnSingletonDestroy();
    }
    
    protected virtual void OnSingletonCreate() {
    }
    
    protected virtual void OnSingletonDestroy() {
    }

    protected bool IsSingleton() {
        return Default == this;
    }
    
    private void KeepAliveIfNeeded() {
        Transform parent = transform.parent;
        if (parent == null) {
            DontDestroyOnLoad(this);
        }
    }

    private void DestroyDuplicate() {
        DestroyImmediate(gameObject);
    }
}