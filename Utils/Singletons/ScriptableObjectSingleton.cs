﻿using System;
using System.Linq;
using Sirenix.Utilities;
using UnityEngine;

public abstract class ScriptableObjectSingleton<T>: ScriptableObject where T: ScriptableObject {
    private static T instance = null;
    public static T Default {
        get {
            if (instance == null) {
                Type type = typeof(T);
                T [] instances = Resources.LoadAll<T>(string.Empty);
                instance = instances.FirstOrDefault();
                if (instances.IsNullOrEmpty()) {
                    Debug.LogError($"[{type}]: Failed to load instance from resources!");
                } else {
                    Debug.Log($"[{type}]: Did load instance from resources!");
                }
            }

            return instance;
        }
    }
}