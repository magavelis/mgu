using System;
using System.Collections.Generic;
using System.Linq;

namespace MGU {
    public static class Enumerable {
        public static T FirstOrDefaultOfType<T>(this IEnumerable<T> list, Type type) {
            return list.FirstOrDefault(view => type == view.GetType());
        }
    }
}