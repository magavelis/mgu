﻿using System.Timers;
using UnityEngine.Events;

namespace MGU {
    public class TimeCalculator {
        private class ElapseEvent: UnityEvent { }
        private readonly ElapseEvent onElapse = new ElapseEvent();

        private float calculationDuration;
        private float calculationStartTime;
        private Timer timer;

        public void Start(float interval, bool executeOnce, UnityAction elapseAction) {
            Stop();

            onElapse.AddListenerSafe(elapseAction);
            timer = new Timer {
                    Interval = interval,
                    AutoReset = !executeOnce
            };
            timer.Elapsed += OnTimerElapse;
            timer.Start();
        }

        public void Stop() {
            if (timer == null) {
                return;
            }

            onElapse.RemoveAllListeners();
            timer.Elapsed -= OnTimerElapse;
            timer.Stop();
            timer.Close();
        }

        private void OnTimerElapse(object sender, ElapsedEventArgs e) {
            onElapse.Invoke();
        }
    }
}