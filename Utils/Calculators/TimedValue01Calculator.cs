﻿using UnityEngine;

namespace MGU {
    public class TimedValue01Calculator {
        private float calculationDuration;
        private float calculationStartTime;

        public void Start(float duration) {
            calculationDuration = duration;
            calculationStartTime = Time.realtimeSinceStartup;
        }

        public float Result() {
            float timeDifference = Mathf.Clamp(
                    Time.realtimeSinceStartup - calculationStartTime, 
                    0, 
                    calculationDuration);
            float currentValue = MathUtils.Remap01(
                    timeDifference, 
                    0, 
                    calculationDuration);
            
            return currentValue;
        }
    }
}
