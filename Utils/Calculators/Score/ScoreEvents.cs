using UnityEngine.Events;

namespace MGU.Score {
    public static class ScoreEvents {
        public class ScoreGainEvent: UnityEvent<ScoreGainConfiguration> { }
        public static readonly ScoreGainEvent OnScoreGain = new ScoreGainEvent();

        public class ScoreAddEvent: UnityEvent<ScoreAddConfiguration> { }
        public static readonly ScoreAddEvent OnScoreAdd = new ScoreAddEvent();
    }
}