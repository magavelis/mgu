using UnityEngine;

namespace MGU.Score {
    public class ScoreGainConfiguration {
        public IScorable Scorable;
        public Vector3 ScorePosition;
        public bool ShouldCountCombo;
    }

    /// <summary>
    /// `ScoreCalculator` are using static `ScoreEvents` class to listen for `ScoreGainEvent`
    /// so in order to properly clean this class you've to call a `Destruct` method.
    /// Otherwise you may start receiving duplicated events.
    /// </summary>
    public class ScoreCalculator {
        private int scoreMultiplier;
        private int comboMultiplier;
        private int lastCalculatedScore;
        private int calculatedScore;

        public ScoreCalculator(int scoreMultiplier) {
            this.scoreMultiplier = scoreMultiplier;
            this.comboMultiplier = 1;
            this.calculatedScore = 0;

            ScoreEvents.OnScoreGain.AddListener(AddScore);
        }

        public void Destruct() {
            ScoreEvents.OnScoreGain.RemoveListener(AddScore);
        }

        public void SetScoreMultiplier(int multiplier) {
            scoreMultiplier = multiplier;
        }

        public void ResetCombo() {
            comboMultiplier = 1;
        }

        private void AddScore(ScoreGainConfiguration configuration) {
            IScorable scorable = configuration.Scorable;
            lastCalculatedScore = scorable.Score(scoreMultiplier) * comboMultiplier;
            if (lastCalculatedScore == 0) {
                return;
            }

            calculatedScore += lastCalculatedScore;

            UpdateComboMultiplier(configuration.ShouldCountCombo);
            ScoreAddConfiguration scoreAddConfiguration = ScoreConfiguration(configuration);
            ScoreEvents.OnScoreAdd.Invoke(scoreAddConfiguration);
        }

        public int TotalScore() {
            return calculatedScore;
        }

        private ScoreAddConfiguration ScoreConfiguration(ScoreGainConfiguration gainConfiguration) {
            ScoreAddConfiguration addConfiguration = new ScoreAddConfiguration {
                    LastScore = lastCalculatedScore,
                    TotalScore = TotalScore(),
                    ComboValue = comboMultiplier - 1,
                    ScorePosition = gainConfiguration.ScorePosition
            };
            return addConfiguration;
        }

        private void UpdateComboMultiplier(bool shouldCountCombo) {
            if (shouldCountCombo) {
                comboMultiplier += 1;
            } else {
                comboMultiplier = 1;
            }
        }
    }
}