using UnityEngine;

namespace MGU.Score {
    public interface IScorable {
        int Score(int multiplier);
    }
    
    public static class ScorableEx {
        public static void ApplyScore(this IScorable scorable, Transform transform, bool shouldCountCombo) {
            ScoreGainConfiguration configuration = new ScoreGainConfiguration {
                    Scorable = scorable,
                    ScorePosition = transform.position,
                    ShouldCountCombo = shouldCountCombo
            };
            
            ScoreEvents.OnScoreGain.Invoke(configuration);
        }
    }
}