using UnityEngine;

namespace MGU.Score {
    public class ScoreAddConfiguration {
        public int LastScore;
        public int TotalScore;
        public int ComboValue;
        public Vector3 ScorePosition;
    }
}

namespace MGU.Bonus {
    using Score;
    
    public static class ScoreConfigurationEx {
        public static FloatingTextPro.Priority Priority(this ScoreAddConfiguration addConfiguration) {
            const int highestPriorityValue = (int)FloatingTextPro.Priority.Highest;
            // We use -1, because our minimum `ComboValue` is 1 and our `Priority.Lowest` value is 0
            int comboValue = addConfiguration.ComboValue - 1;
            if (comboValue > highestPriorityValue) {
                comboValue = highestPriorityValue;
            }

            return (FloatingTextPro.Priority) comboValue;
        }
    }
}