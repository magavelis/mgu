using System;
using System.Collections.Generic;
using UnityEngine;

namespace MGU.Score {
    using Bonus;

    public class ScoreVisualizer: MonoBehaviour {
        [Serializable]
        private class PrioritizedEffect {
            public FloatingTextPro.Priority Priority = FloatingTextPro.Priority.Default;
            public ParticleEffect Effect = null;
        }

        [SerializeField] private FloatingTextPro floatingTextPrefab = null;
        [SerializeField] private List<PrioritizedEffect> effectList = new List<PrioritizedEffect>();

        private void OnEnable() {
            ScoreEvents.OnScoreAdd.AddListener(OnScoreAdd);
        }

        private void OnDisable() {
            ScoreEvents.OnScoreAdd.RemoveListener(OnScoreAdd);
        }

        private void OnScoreAdd(ScoreAddConfiguration configuration) {
            if (configuration.ComboValue == 0) {
                return;
            }

            ShowFloatingText(configuration);
            ShowParticleEffect(configuration);
        }

        private void ShowFloatingText(ScoreAddConfiguration configuration) {
            FloatingTextPro floatingText = Instantiate(
                    floatingTextPrefab,
                    configuration.ScorePosition,
                    Quaternion.identity);

            string text = $"+{configuration.LastScore}";
            floatingText.Construct(text);
            floatingText.Show(configuration.Priority());
        }

        private void ShowParticleEffect(ScoreAddConfiguration configuration) {
            ParticleEffect effect = ParticleEffect(configuration.Priority());
            if (effect.IsNull()) {
                return;
            }
            
            effect.Create(configuration.ScorePosition);
        }
        
        private ParticleEffect ParticleEffect(FloatingTextPro.Priority priority) {
            PrioritizedEffect prioritizedEffect = effectList.Find(effect => effect.Priority == priority);
            ParticleEffect particleEffect = prioritizedEffect.Effect;
            return particleEffect;
        }
    }
}