﻿using UnityEngine;

namespace MGU {
    public class DurationCalculator {

        // MARK: - Declarations
        private readonly float duration;
        private readonly float startValue;
        private readonly float endValue;
        private readonly float differenceModifier;

        // MARK: - Methods
        public DurationCalculator(float duration, float startValue, float endValue) {
            this.duration = duration;
            this.startValue = startValue;
            this.endValue = endValue;
            this.differenceModifier = startValue > endValue ? -1 : 1;
        }

        public float EnterDuration(float currentValue) {
            float sumValue = Mathf.Abs(startValue) + Mathf.Abs(endValue);
            float adjustedCurrentValue = sumValue - (currentValue - startValue) * differenceModifier;
            float newDuration = duration * (adjustedCurrentValue / sumValue);

            return newDuration;
        }

        public float ExitDuration(float currentValue) {
            return duration - EnterDuration(currentValue);
        }
    }
}
