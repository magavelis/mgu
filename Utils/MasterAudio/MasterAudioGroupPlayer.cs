#if MASTER_AUDIO
using UnityEngine;

namespace MGU.MasterAudio {
    
    using DarkTonic.MasterAudio;

    public class MasterAudioGroupPlayer: MonoBehaviour {

        public string SoundName = "";
        
        public void Play() {
            MasterAudio.PlaySoundAndForget(SoundName);
        }
    }
}
#endif