#if MASTER_AUDIO
using UnityEngine;

namespace MGU.MasterAudio {
    using DarkTonic.MasterAudio;

    public class MasterAudioCustomEventSender: MonoBehaviour {

        public CustomEvent Event;

        public void Send(Transform origin) {
            if (Event.EventName == MasterAudio.NoGroupName) {
                return;
            }

            MasterAudio.FireCustomEvent(Event.EventName, origin);
        }
    }
}    
#endif