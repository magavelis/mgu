﻿using UnityEngine;
using MGU;

/// <summary>
/// Sometimes does not work us expected...
/// Cause strange jumping
/// </summary>
public class ContinuesCollision2D: MonoBehaviour {
    private const string messageName = "OnTriggerEnter2D";

    public LayerMask layerMask = -1;

    private float minimumExtent;
    private float partialExtent;

    private Vector2 previousPosition;
    [SerializeField] private Rigidbody2D physicsBody = null;
    [SerializeField] private Collider2D colliderObject = null;

    private void Awake() {
        if (physicsBody.IsNull() || colliderObject.IsNull()) {
            Debug.LogError("Unable to check for continues collision detection without `Rigidbody2D` or `Collider2D`");
            enabled = false;
            return;
        }

        previousPosition = physicsBody.position;

        Bounds bounds = colliderObject.bounds;
        partialExtent = Mathf.Min(bounds.extents.x, bounds.extents.y);
        minimumExtent = partialExtent * partialExtent;
    }

    private void FixedUpdate() {
        // Have we moved more than our minimum extent? 
        Vector2 movementDelta = physicsBody.position - previousPosition;
        float movementDeltaMagnitude = movementDelta.sqrMagnitude;
        if (movementDeltaMagnitude > minimumExtent) {
            float movementMagnitude = Mathf.Sqrt(movementDeltaMagnitude);

            // Check for obstructions we might have missed 
            RaycastHit2D raycastHit = Physics2D.Raycast(
                    previousPosition,
                    movementDelta,
                    movementMagnitude,
                    layerMask.value);

            if (raycastHit) {
                if (raycastHit.collider == colliderObject) {
                    return;
                }

                if (raycastHit.collider.isTrigger) {
                    // Trigger did happen
                    raycastHit.collider.SendMessage(messageName,
                                                    colliderObject,
                                                    SendMessageOptions.DontRequireReceiver);
                    SendMessage(messageName,
                                raycastHit.collider,
                                SendMessageOptions.DontRequireReceiver);
                } else {
                    // Collision did happen
                    physicsBody.position = raycastHit.point - (movementDelta / movementMagnitude) * partialExtent;
                }
            }
        }

        previousPosition = physicsBody.position;
    }
}