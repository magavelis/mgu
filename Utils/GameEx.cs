using UnityEngine;

namespace MGU {
    public static class GameEx {
        public static void SetSlowMotion(float value) {
            Time.timeScale = value;
        
            // NOTE! It has effect on how physics works.
            Time.fixedDeltaTime = 0.02f * Time.timeScale;
        }
    }
}
