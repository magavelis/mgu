﻿namespace MGU {
    public static class MathUtils {
        public static float Lerp(float a, float b, float t) {
            return (1.0f - t) * a + t * b;
        }

        public static float Remap01(int value, int from, int to) {
            return Remap01((float) value, from, to);
        }

        public static float Remap01(float value, float from, float to) {
            return (value - from) / (to - from);
        }
    }
}