﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MGU {
    [Serializable]
    public abstract class SerializableDictionary<TKey, TValue>: Dictionary<TKey, TValue>, 
            ISerializationCallbackReceiver {
        
        protected abstract List<SerializableKeyValuePair<TKey, TValue>> KeyValuePairList { get; set; }

        public void OnBeforeSerialize() {
            KeyValuePairList.Clear();
            foreach (KeyValuePair<TKey, TValue> pair in this) {
                KeyValuePairList.Add(new SerializableKeyValuePair<TKey, TValue>(pair.Key, pair.Value));
            }
        }

        public void OnAfterDeserialize() {
            this.Clear();

            foreach (SerializableKeyValuePair<TKey, TValue> keyValuePair in KeyValuePairList) {
                this[keyValuePair.Key] = keyValuePair.Value;
            }
        }
    }

    [Serializable]
    public class SerializableKeyValuePair<TKey, TValue>: IEquatable<SerializableKeyValuePair<TKey, TValue>> {
        [SerializeField] private TKey key;
        public TKey Key => key;

        [SerializeField] private TValue value;
        public TValue Value => value;

        public SerializableKeyValuePair() { }

        public SerializableKeyValuePair(TKey key, TValue value) {
            this.key = key;
            this.value = value;
        }

        public bool Equals(SerializableKeyValuePair<TKey, TValue> other) {
            EqualityComparer<TKey> comparer1 = EqualityComparer<TKey>.Default;
            EqualityComparer<TValue> comparer2 = EqualityComparer<TValue>.Default;

            return comparer1.Equals(key, other.key) &&
                   comparer2.Equals(value, other.value);
        }

        public override int GetHashCode() {
            EqualityComparer<TKey> comparer1 = EqualityComparer<TKey>.Default;
            EqualityComparer<TValue> comparer2 = EqualityComparer<TValue>.Default;

            int h0 = comparer1.GetHashCode(key);
            h0 = (h0 << 5) + h0 ^ comparer2.GetHashCode(value);
            return h0;
        }

        public override string ToString() {
            return $"(Key: {key}, Value: {value})";
        }
    }
}