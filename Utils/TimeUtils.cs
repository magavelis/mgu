using System;

namespace MGU {
    public static class TimeUtils {
        public static double TimeIntervalSince1970() {
            DateTime epochStart = new DateTime(
                    1970,
                    1,
                    1,
                    0,
                    0,
                    0,
                    DateTimeKind.Utc);
            double timeIntervalSince1970 = (DateTime.UtcNow - epochStart).TotalSeconds;
            return timeIntervalSince1970;
        }
    }
}