﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace MGU {
    public class MyCoroutine {
        /// <summary>
        /// Is called when coroutine complete. Is not called for cancelled coroutines.
        /// </summary>
        public class CoroutineCompleteEvent: UnityEvent { }
        public readonly CoroutineCompleteEvent OnCoroutineComplete = new CoroutineCompleteEvent();

        /// <summary>
        /// Is called before coroutine starts. Is not called for cancelled coroutines.
        /// </summary>
        public class CoroutineStartEvent: UnityEvent { }
        public readonly CoroutineStartEvent OnCoroutineStart = new CoroutineStartEvent();

        private bool isExecuting;
        public bool IsExecuting => isExecuting;

        private bool isPaused;

        public bool IsPaused {
            get => isPaused;
            set => isPaused = value;
        }

        private bool isFinished;
        public bool IsFinished => isFinished;

        private bool isCancelled;
        public bool IsCancelled => isCancelled;

        private bool isSkipped;
        public bool IsSkipped => isSkipped;

        public float DelayUntilStartInSeconds = 0.0f;

        private readonly List<IEnumerator> coroutineList;
        private readonly List<bool> coroutineCompletionList;

        /// <summary>
        /// Is used for CoroutineQueue as a trigger to call next item in queue.
        /// </summary>
        private event UnityAction OnComplete;

        public MyCoroutine(IEnumerator coroutine): this(new[] { coroutine }) { }

        public MyCoroutine(IEnumerable<IEnumerator> coroutines): this() {
            AddCoroutines(coroutines);
        }

        public MyCoroutine() {
            coroutineList = new List<IEnumerator>();
            coroutineCompletionList = new List<bool>();
        }

        public void AddCoroutines(IEnumerable<IEnumerator> coroutines) {
            foreach (IEnumerator coroutine in coroutines) {
                AddCoroutine(coroutine);
            }
        }

        public void AddCoroutine(IEnumerator coroutine) {
            if (isExecuting || isFinished) {
                Debug.LogWarning("Can't add additional coroutine. Main coroutine already started executing");
                return;
            }

            coroutineList.Add(coroutine);
            coroutineCompletionList.Add(false);
        }

        public void StartCoroutine(MonoBehaviour owner, UnityAction onComplete) {
            if (isFinished) {
                Debug.LogError("Can't start finished coroutine");
                return;
            }

            if (isExecuting) {
                Debug.LogError("Coroutine is already executing");
                return;
            }

            OnComplete = onComplete;

            isExecuting = true;
            isFinished = false;

            owner.StartCoroutine(ExecuteAll(owner));
        }

        public void Cancel() {
            isCancelled = true;
        }

        public void Skip() {
            isSkipped = true;
        }

        private IEnumerator ExecuteAll(MonoBehaviour owner) {
            if (DelayUntilStartInSeconds > 0.0f) {
                yield return Yielders.WaitForSeconds(DelayUntilStartInSeconds);
            }

            OnCoroutineStart.Invoke();
            OnCoroutineStart.RemoveAllListeners();

            for (int i = 0; i < coroutineList.Count; i++) {
                owner.StartCoroutine(Execute(coroutineList[i], i));
            }

            while (true) {
                if (isCancelled) {
                    break;
                }

                if (isSkipped) {
                    break;
                }

                bool isSuccess = false;
                for (int i = 0; i < coroutineList.Count; i++) {
                    if (coroutineCompletionList[i] == false) {
                        isSuccess = false;
                        break;
                    }

                    isSuccess = true;
                }

                if (isSuccess) {
                    break;
                }

                yield return Yielders.EndOfFrame;
            }

            ExitCoroutine(!isCancelled);
        }

        private IEnumerator Execute(IEnumerator coroutine, int index) {
            yield return Yielders.EndOfFrame;

            while (isExecuting) {
                if (isCancelled) {
                    yield break;
                }

                if (isSkipped) {
                    yield break;
                }

                if (isPaused) {
                    yield return Yielders.EndOfFrame;
                } else {
                    if (coroutine.MoveNext()) {
                        yield return coroutine.Current;
                    } else {
                        coroutineCompletionList[index] = true;
                        yield break;
                    }
                }
            }
        }

        private void ExitCoroutine(bool shouldCallCompletionCallback) {
            isFinished = true;
            isExecuting = false;

            OnComplete?.Invoke();
            OnComplete = null;

            if (shouldCallCompletionCallback) {
                OnCoroutineComplete.Invoke();
                OnCoroutineComplete.RemoveAllListeners();
            }
        }
    }
}