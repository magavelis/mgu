﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace MGU {
    public class CoroutineQueue: MonoBehaviour {
        /// <summary>
        /// Is called when queue finishes all coroutines. Is not called, when queue is cancelled.
        /// </summary>
        public class QueueCompleteEvent: UnityEvent { }
        public readonly QueueCompleteEvent OnQueueComplete = new QueueCompleteEvent();        
        
        private bool isRunning;
        public bool IsRunning => isRunning;

        private bool isPaused;
        public bool IsPaused => isPaused;

        private readonly Queue<MyCoroutine> coroutineQueue = new Queue<MyCoroutine>();
        private MyCoroutine activeCoroutine;

        /// <summary>
        /// Create the CoroutineQueue Object. Use only this initialization method.
        /// </summary>
        /// <param name="obj">Scene object</param>
        public static CoroutineQueue Create(GameObject obj) {
            CoroutineQueue queue = obj.GetComponent<CoroutineQueue>();
            if (queue == null) {
                queue = obj.AddComponent<CoroutineQueue>();
            }

            return queue;
        }

        public void AddCoroutine(MyCoroutine coroutine) {
            coroutineQueue.Enqueue(coroutine);

            StartQueue();
        }

        public void Pause() {
            if (activeCoroutine != null) {
                activeCoroutine.IsPaused = true;
            }

            foreach (MyCoroutine coroutine in coroutineQueue) {
                coroutine.IsPaused = true;
            }

            isPaused = true;

            Debug.Log("Queue Paused");
        }

        public void Resume() {
            if (activeCoroutine != null) {
                activeCoroutine.IsPaused = false;
            }

            foreach (MyCoroutine coroutine in coroutineQueue) {
                coroutine.IsPaused = false;
            }

            isPaused = false;
        }

        public void CancelAllCoroutines() {
            activeCoroutine?.Cancel();
            foreach (MyCoroutine coroutine in coroutineQueue) {
                coroutine.Cancel();
            }
        }

        public void SkipAllCoroutines() {
            activeCoroutine?.Skip();
            foreach (MyCoroutine coroutine in coroutineQueue) {
                coroutine.Skip();
            }
        }

        private void StartQueue() {
            if (IsRunning) {
                // Our queue is already running
                return;
            }

            isRunning = true;

            PickAndStartCoroutine();
        }

        private void PickAndStartCoroutine() {
            if (coroutineQueue.Count > 0) {
                activeCoroutine = coroutineQueue.Dequeue();
                activeCoroutine.StartCoroutine(this, PickAndStartCoroutine);
            } else {
                isRunning = false;
                activeCoroutine = null;
                
                OnQueueComplete.Invoke();
                OnQueueComplete.RemoveAllListeners();
            }
        }
    }
}