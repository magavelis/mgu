﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace MGU {
    public static class CoroutineEx {
        public static IEnumerator Execute(
                float duration,
                UnityAction<float> onExecute,
                UnityAction onComplete) {
            return Execute(
                    0.0f,
                    duration,
                    () => Yielders.EndOfFrame,
                    onExecute,
                    onComplete);
        }
        
        public static IEnumerator Execute(
                float delay,
                float duration,
                UnityAction<float> onExecute,
                UnityAction onComplete) {
            return Execute(
                    delay,
                    duration,
                    () => Yielders.EndOfFrame,
                    onExecute,
                    onComplete);
        }

        public static IEnumerator Execute(
                float delay,
                float duration,
                Func<object> step,
                UnityAction<float> onExecute,
                UnityAction onComplete) {

            if (delay > 0.0f) {
                yield return Yielders.WaitForSeconds(delay);
            }
            
            if (duration <= 0f) {
                onExecute?.Invoke(1f);
                onComplete?.Invoke();
                yield break;
            }

            float startTime = Time.time;
            float endTime = startTime + duration;
            while (Time.time <= endTime) {
                float time = (Time.time - startTime) / duration;
                onExecute?.Invoke(time);
                yield return step();
            }

            onExecute?.Invoke(1f);
            onComplete?.Invoke();
        }
    }
}

