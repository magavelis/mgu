﻿using UnityEngine;

namespace MGU {
    [System.Serializable]
    public class EdgeInsets {
        public float Top;
        public float Bottom;
        public float Leading;
        public float Trailing;

        public static EdgeInsets Zero => new EdgeInsets(0, 0, 0, 0);

        public EdgeInsets(float top, float bottom, float leading, float trailing) {
            Top = top;
            Bottom = bottom;
            Leading = leading;
            Trailing = trailing;
        }
    }

    public static class ScreenUtils {
        public static float PixelsPerCentimeter {
            get {
                const float inchesToCentimeter = 2.54f;
                const float tolerance = 0.001f;
                #if UNITY_ANDROID

                // Android MDPI setting fallback
                // http://developer.android.com/guide/practices/screens_support.html
                const float fallbackDpi = 160f;
                #elif (UNITY_WP8 || UNITY_WP8_1 || UNITY_WSA || UNITY_WSA_8_0)
				// Windows phone is harder to track down
				// http://www.windowscentral.com/higher-resolution-support-windows-phone-7-dpi-262
				const float fallbackDpi = 92f;
                #elif UNITY_IOS
                // iPhone 4-6 range
                const float fallbackDpi = 326f;
                #else
                const float fallbackDpi = 72f;
                #endif

                return (Mathf.Abs(Screen.dpi) < tolerance)
                        ? fallbackDpi / inchesToCentimeter
                        : Screen.dpi / inchesToCentimeter;
            }
        }
        
        public static EdgeInsets SafeArea {
            get {
                float top = Screen.height - Screen.safeArea.yMax;
                float bottom = Screen.safeArea.yMin;
                float leading = Screen.safeArea.xMin;
                float trailing = Screen.width - Screen.safeArea.xMax;

                return new EdgeInsets(top, bottom, leading, trailing);
            }
        }

        public static Vector2 ScreenCenter => new Vector2(Screen.width / 2.0f, Screen.height / 2.0f);
    }
}