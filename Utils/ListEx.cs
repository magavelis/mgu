﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MGU {
    internal static class List {
        public static T ElementAfterOrDefault<T>(this IList<T> list, T element) {
            int index = list.IndexOf(element);
            T nextElement = list.ElementAtOrDefault(index + 1);
            return (nextElement == null) ? element : nextElement;
        }

        public static T ElementBeforeOrDefault<T>(this IList<T> list, T element) {
            int index = list.IndexOf(element);
            T previousElement = list.ElementAtOrDefault(index - 1);
            return (previousElement == null) ? element : previousElement;
        }

        public static void TryAdd<T>(this IList<T> list, T element) {
            if (element == null) {
                return;
            }
            
            list.Add(element);
        }

        /// <summary>
        /// Returns <c>true</c> if the list is either null or empty. Otherwise <c>false</c>.
        /// </summary>
        public static bool IsNullOrEmpty<T>(this IList<T> list) {
            if (list == null) {
                return true;
            }
            
            return list.Count == 0;
        }
        
        public static List<T> Shuffled<T>(this List<T> list) {  
            Random rng = new Random();  

            int n = list.Count;  
            while (n > 1) {  
                n--;  
                int k = rng.Next(n + 1);  
                T value = list[k];  
                list[k] = list[n];  
                list[n] = value;  
            }

            return list;
        }

        public static T RandomElement<T>(this List<T> list) {
            int index = UnityEngine.Random.Range(0, list.Count);
            return list[index];
        }
    }
}
