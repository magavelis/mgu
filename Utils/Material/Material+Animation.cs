using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace MGU {
    public static partial class MaterialEx {
        public static IEnumerator AdjustFloat(
                this Material mat,
                int nameId,
                float toValue,
                float duration,
                AnimationCurve animationCurve,
                UnityAction onComplete) {
            ICurveValueAccessor curve = new AnimationCurveValueWrapper(animationCurve);
            return AdjustFloat(mat, nameId, toValue, duration, curve, onComplete);
        }

        public static IEnumerator AdjustFloat(
                this Material mat,
                int nameId,
                float toValue,
                float duration,
                Func<float, float> easing,
                UnityAction onComplete) {
            ICurveValueAccessor curve = new EasingFunctionWrapper(easing);
            return AdjustFloat(mat, nameId, toValue, duration, curve, onComplete);
        }

        private static IEnumerator AdjustFloat(
                this Material mat,
                int nameId,
                float toValue,
                float duration,
                ICurveValueAccessor curve,
                UnityAction onComplete) {
            float fromValue = mat.GetFloat(nameId);
            yield return CoroutineEx.Execute(
                    duration,
                    (float time) => {
                        float position = curve.Position(time);
                        float newValue = MathUtils.Lerp(fromValue, toValue, position);
                        mat.SetFloat(nameId, newValue);
                    },
                    () => {
                        mat.SetFloat(nameId, toValue);
                        onComplete?.Invoke();
                    });
        }
    }
}