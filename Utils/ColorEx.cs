using UnityEngine;

namespace MGU {
    public static class ColorEx {
        public static Color AlphaAdjusted(this Color color, float alpha) {
            Color newColor = new Color(
                    color.r,
                    color.g,
                    color.b,
                    alpha);
            return newColor;
        }
    }
}