using UnityEngine;

namespace MGU {
    public static class CameraEx {
        public static Vector2 OrthographicFrameSize(this Camera camera) {
            if (camera.orthographic == false) {
                Debug.LogError("Resize is only supported for orthographic camera's");
                return Vector2.zero;
            }

            float height = camera.orthographicSize * 2f;
            float width = height / Screen.height * Screen.width;
            return new Vector2(width, height);
        }
    }
}