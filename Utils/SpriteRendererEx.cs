using UnityEngine;

namespace MGU {
    public static class SpriteRendererEx {
        public static void FitOrthographicCamera(this SpriteRenderer spriteRenderer, Camera camera) {
            Vector2 size = camera.OrthographicFrameSize();
            spriteRenderer.size = new Vector2(size.x, size.y); 
        }

        public static void SetAlpha(this SpriteRenderer spriteRenderer, float alpha) {
            Color currentColor = spriteRenderer.color;
            currentColor.a = alpha;
            spriteRenderer.color = currentColor;
        }
    }
}