﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace MGU {
    public static partial class CanvasGroupEx {
        public static IEnumerator FadeOut(this CanvasGroup canvasGroup, float duration, UnityAction onComplete) {
            return FadeTo(canvasGroup, 0, duration, onComplete);
        }

        public static IEnumerator FadeIn(this CanvasGroup canvasGroup, float duration, UnityAction onComplete) {
            return FadeTo(canvasGroup, 1, duration, onComplete);
        }

        public static IEnumerator FadeTo(this CanvasGroup canvasGroup, float toAlpha, float duration, UnityAction onComplete) {
            float fromAlpha = canvasGroup.alpha;
            yield return CoroutineEx.Execute(
                    duration,
                    (float time) => {
                        canvasGroup.alpha = MathUtils.Lerp(fromAlpha, toAlpha, time);
                    },
                    () => {
                        canvasGroup.alpha = toAlpha;
                        onComplete?.Invoke();
                    });
        }
    }
}