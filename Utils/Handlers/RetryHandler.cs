using System;
using System.Collections;
using UnityEngine;

namespace MGU {
    public class RetryHandler {
        private readonly MonoBehaviour owner;
        private readonly int retryAttemptsCount;
        private readonly float delayBetweenRetries;

        private int retryAttempt = 0;
        private IEnumerator activeRetryCoroutine;

        public RetryHandler(MonoBehaviour owner, int retryAttemptsCount, float delayBetweenRetries) {
            this.owner = owner;
            this.retryAttemptsCount = retryAttemptsCount;
            this.delayBetweenRetries = delayBetweenRetries;
        }

        public void Retry(Action retryHandler, Action cancelHandler) {
            if (retryAttempt >= retryAttemptsCount) {
                cancelHandler.Invoke();
                return;
            }

            activeRetryCoroutine = IE_DoAfter(
                    delayBetweenRetries,
                    () => {
                        if (retryAttempt < retryAttemptsCount) {
                            retryAttempt = Mathf.Min(retryAttemptsCount, retryAttempt + 1);
                            retryHandler.Invoke();
                        } else {
                            cancelHandler.Invoke();
                        }
                    });
            owner.StartCoroutine(activeRetryCoroutine);
        }

        public void Cancel() {
            retryAttempt = 0;
            if (activeRetryCoroutine != null) {
                owner.StopCoroutine(activeRetryCoroutine);
            }
        }

        private IEnumerator IE_DoAfter(float timeInterval, Action action) {
            yield return Yielders.WaitForSeconds(timeInterval);
            action?.Invoke();
        }
    }
}