﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace MGU {
    public class Collision2DConfiguration {
        private readonly Collision2D collision;

        public GameObject OwnerObject => collision.otherCollider.gameObject;
        public GameObject IncomingObject => collision.rigidbody.gameObject;

        public Collision2DConfiguration(Collision2D collision) {
            this.collision = collision;
        }

        public void IgnoreFutureCollisions() {
            Physics2D.IgnoreCollision(collision.collider, collision.otherCollider);
        }
    }

    public class Trigger2DConfiguration {
        public readonly GameObject OwnerObject;
        public readonly GameObject IncomingObject;

        public Trigger2DConfiguration(GameObject gameObject, Collider2D otherCollider) {
            OwnerObject = gameObject;
            IncomingObject = otherCollider.attachedRigidbody.gameObject;
        }
    }

    public class Unity2DEventSender: MonoBehaviour {
        [Serializable] public class CollisionEnterEvent: UnityEvent<Collision2DConfiguration> { }
        public CollisionEnterEvent OnCollisionEnter = new CollisionEnterEvent();

        [Serializable] public class CollisionStayEvent: UnityEvent<Collision2DConfiguration> { }
        public CollisionStayEvent OnCollisionStay = new CollisionStayEvent();

        [Serializable] public class CollisionExitEvent: UnityEvent<Collision2DConfiguration> { }
        public CollisionExitEvent OnCollisionExit = new CollisionExitEvent();

        [Serializable] public class TriggerEnterEvent: UnityEvent<Trigger2DConfiguration> { }
        public TriggerEnterEvent OnTriggerEnter = new TriggerEnterEvent();

        [Serializable] public class TriggerStayEvent: UnityEvent<Trigger2DConfiguration> { }
        public TriggerStayEvent OnTriggerStay = new TriggerStayEvent();

        [Serializable] public class TriggerExitEvent: UnityEvent<Trigger2DConfiguration> { }
        public TriggerExitEvent OnTriggerExit = new TriggerExitEvent();

        private void OnCollisionEnter2D(Collision2D other) {
            Collision2DConfiguration configuration = new Collision2DConfiguration(other);
            OnCollisionEnter?.Invoke(configuration);
        }

        private void OnCollisionStay2D(Collision2D other) {
            Collision2DConfiguration configuration = new Collision2DConfiguration(other);
            OnCollisionStay?.Invoke(configuration);
        }

        private void OnCollisionExit2D(Collision2D other) {
            Collision2DConfiguration configuration = new Collision2DConfiguration(other);
            OnCollisionExit?.Invoke(configuration);
        }

        private void OnTriggerEnter2D(Collider2D other) {
            Trigger2DConfiguration configuration = new Trigger2DConfiguration(gameObject, other);
            OnTriggerEnter?.Invoke(configuration);
        }

        private void OnTriggerStay2D(Collider2D other) {
            Trigger2DConfiguration configuration = new Trigger2DConfiguration(gameObject, other);
            OnTriggerStay?.Invoke(configuration);
        }

        private void OnTriggerExit2D(Collider2D other) {
            Trigger2DConfiguration configuration = new Trigger2DConfiguration(gameObject, other);
            OnTriggerExit?.Invoke(configuration);
        }
    }
}