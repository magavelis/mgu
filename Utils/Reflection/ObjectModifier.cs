﻿using System.Reflection;
using System.Collections.Generic;

namespace MGU.Reflection {
    public class ObjectModifier: ObjectExposer {
        private Dictionary<string, object> cachedFieldsDict;

        public ObjectModifier(IObjectExposable exposableObject) : base(exposableObject) {
        }

        public void SetFieldValue(FieldInfo field, object newValue) {
            field.SetValue(ExposableObject, newValue);
        }

        public void CacheObjectState() {
            cachedFieldsDict = new Dictionary<string, object>();
            foreach (FieldInfo field in Fields()) {
                cachedFieldsDict[field.Name] = field.GetValue(ExposableObject);
            }
        }

        public void ResetClass() {
            if (cachedFieldsDict == null) {
                return;
            }

            foreach (FieldInfo field in Fields()) {
                field.SetValue(ExposableObject, cachedFieldsDict[field.Name]);
            }
        }  
    }
}
