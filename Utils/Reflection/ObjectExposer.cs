using System.Linq;
using System.Reflection;
using System.Collections.Generic;

namespace MGU.Reflection {
    public class FieldParams {
        public string Description;
    }

    public class BoolFieldParams: FieldParams {
    }
    
    public class NumberFieldParams: FieldParams {
        public object MinValue;
        public object MaxValue;
    }

    public interface IObjectExposable {
        Dictionary<string, FieldParams> ExposableFieldDict { get; }
    }

    public class ObjectExposer {
        protected readonly IObjectExposable ExposableObject;

        protected ObjectExposer(IObjectExposable exposableObject) {
            this.ExposableObject = exposableObject;
        }

        public FieldInfo[] Fields() {
            List<FieldInfo> fieldList = ExposableObject.GetType().GetFields().ToList();
            Dictionary<string, FieldParams> exposableFieldDict = ExposableObject.ExposableFieldDict;
            fieldList.RemoveAll(field => exposableFieldDict.ContainsKey(field.Name) == false);
            return fieldList.ToArray();
        }
        
        public System.Type FieldType(FieldInfo field) {
            Dictionary<string, FieldParams> exposableFieldDict = ExposableObject.ExposableFieldDict;
            return exposableFieldDict[field.Name].GetType();
        }

        public string FieldDescription(FieldInfo field) {
            Dictionary<string, FieldParams> exposableFieldDict = ExposableObject.ExposableFieldDict;
            return exposableFieldDict[field.Name].Description;
        }

        public object FieldValue(FieldInfo field) {
            object value = field.GetValue(ExposableObject);
            return value;
        }

        public object FieldMinValue(FieldInfo field) {
            Dictionary<string, FieldParams> exposableFieldDict = ExposableObject.ExposableFieldDict;
            NumberFieldParams numberField = exposableFieldDict[field.Name] as NumberFieldParams;
            return numberField?.MinValue ?? 0;
        }

        public object FieldMaxValue(FieldInfo field) {
            Dictionary<string, FieldParams> exposableFieldDict = ExposableObject.ExposableFieldDict;
            NumberFieldParams numberField = exposableFieldDict[field.Name] as NumberFieldParams;
            return numberField?.MaxValue ?? 0;
        }
    }
}