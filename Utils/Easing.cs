using UnityEngine;

namespace MGU {
    public static class Easing {
        public static float Linear(float t) {
            return t;
        }

        public static class Quartic {
            public static float EaseIn(float t) {
                return Mathf.Pow(t, 4.0f);
            }

            public static float EaseOut(float t) {
                return (Mathf.Pow(t - 1, 4) - 1) * -1;
            }

            public static float EaseInOut(float t) {
                if (t <= 0.5f) {
                    return EaseIn(t * 2) / 2;
                }

                return (EaseOut((t - 0.5f) * 2.0f) / 2) + 0.5f;
            }
        }

        public static class Quintic {
            public static float EaseIn(float t) {
                return Mathf.Pow(t, 5.0f);
            }

            public static float EaseOut(float t) {
                return (Mathf.Pow(t - 1, 5) + 1);
            }

            public static float EaseInOut(float t) {
                if (t <= 0.5f) {
                    return EaseIn(t * 2) / 2;
                }

                return (EaseOut((t - 0.5f) * 2.0f) / 2) + 0.5f;
            }
        }

        public static class Sinusoidal {
            public static float EaseIn(float t) {
                return Mathf.Sin((t - 1) * (Mathf.PI / 2)) + 1;
            }

            public static float EaseOut(float t) {
                return Mathf.Sin(t * (Mathf.PI / 2));
            }

            public static float EaseInOut(float t) {
                if (t <= 0.5f) {
                    return EaseIn(t * 2) / 2;
                }

                return (EaseOut((t - 0.5f) * 2.0f) / 2) + 0.5f;
            }
        }

        public static class Exponential {
            public static float EaseIn(float t) {
                return Mathf.Pow(2, 10 * (t - 1));
            }

            public static float EaseOut(float t) {
                return 1 - Mathf.Pow(2, -10 * t);
            }

            public static float EaseInOut(float t) {
                if (t <= 0.5f) {
                    return EaseIn(t * 2) / 2;
                }

                return EaseOut(t * 2 - 1) / 2 + 0.5f;
            }
        }

        public static class Circular {
            public static float EaseIn(float t) {
                return (-1 * Mathf.Sqrt(1 - t * t) + 1);
            }

            public static float EaseOut(float t) {
                return Mathf.Sqrt(1 - Mathf.Pow(t - 1, 2));
            }

            public static float EaseInOut(float t) {
                if (t <= 0.5f) {
                    return EaseIn(t * 2) / 2;
                }

                return (EaseOut((t - 0.5f) * 2.0f) / 2) + 0.5f;
            }
        }

        public class Back {
            private readonly float strength;
            private readonly float secondStrength;

            public Back(float strengthMultiplier = 1.0f) {
                strength = 1.70158f * strengthMultiplier;
                secondStrength = strength * 1.525f;
            }

            public float EaseIn(float t) {
                return t * t * ((strength + 1) * t - 2);
            }

            public float EaseOut(float t) {
                t = t - 1;
                return (t * t * ((strength + 1) * t + strength) + 1);
            }

            public float EaseInOut(float t) {
                t = t * 2;

                if (t < 1) {
                    return 0.5f * (t * t * ((secondStrength + 1) * t - secondStrength));
                }

                t -= 2;
                return 0.5f * (t * t * ((secondStrength + 1) * t + secondStrength) + 2);
            }
        }

        public static class Bounce {
            private const float b = 0f;
            private const float c = 1f;
            private const float d = 1f;

            public static float EaseOut(float t) {
                if ((t /= d) < (1 / 2.75)) {
                    return c * (7.5625f * t * t) + b;
                }

                if (t < (2 / 2.75)) {
                    return c * (7.5625f * (t -= (1.5f / 2.75f)) * t + .75f) + b;
                }

                if (t < (2.5 / 2.75)) {
                    return c * (7.5625f * (t -= (2.25f / 2.75f)) * t + .9375f) + b;
                }

                return c * (7.5625f * (t -= (2.625f / 2.75f)) * t + .984375f) + b;
            }

            public static float EaseIn(float t) {
                return c - EaseOut(d - t) + b;
            }

            public static float EaseInOut(float t) {
                if (t < d / 2) {
                    return EaseIn(t * 2) * 0.5f + b;
                }

                return EaseOut(t * 2 - d) * .5f + c * 0.5f + b;
            }
        }

        public static class Elastic {
            public static float EaseIn(float t) {
                return 1 - EaseOut(1 - t);
            }

            public static float EaseOut(float t) {
                const float p = 0.3f;
                return Mathf.Pow(2, -10 * t) * Mathf.Sin((t - p / 4) * (2 * Mathf.PI) / p) + 1;
            }

            public static float EaseInOut(float t) {
                if (t <= 0.5f) {
                    return EaseIn(t * 2) / 2;
                }

                return EaseOut((t - 0.5f) * 2.0f) / 2 + 0.5f;
            }
        }
    }
}