﻿using UnityEngine;

namespace MGU {
    public static class MonoBehaviourEx {
        public static void AssignComponent<T>(this Behaviour behaviour, ref T myObject) where T : Behaviour {
            AssignComponent(behaviour, ref myObject, false);
        }

        public static void AssignComponent<T>(
                this Behaviour behaviour,
                ref T myObject,
                bool recursive) where T : Behaviour {
            if (myObject.IsNull()) {
                myObject = behaviour.GetComponent<T>();

                if (recursive && myObject.IsNull()) {
                    AssignComponentFromChildren(behaviour, ref myObject);
                }
            }
        }

        public static void AssignComponentFromChildren<T>(this Behaviour behaviour, ref T myObject)
                where T : Behaviour {
            if (myObject.IsNull()) {
                myObject = behaviour.GetComponentInChildren<T>();
            }
        }
    }
}