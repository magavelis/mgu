using UnityEngine;

namespace MGU {
	public class GeometricUtils {
		public static float Angle(Vector2 point1, Vector2 point2) {
			Vector2 fromAngle = point2 - point1;
			Vector2 toAngle = new Vector2(1, 0);
	 
			float result = Vector2.Angle(fromAngle, toAngle);
			Vector3 cross = Vector3.Cross(fromAngle, toAngle);
	 
			if (cross.z > 0.0f) {
				result = 360.0f - result;
			}
	 
			return result;
		}
	}
}
