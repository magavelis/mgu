﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace MGU {
    public static class Vector3Utils {
        public static Vector3 Lerp(Vector3 a, Vector3 b, float t) {
            Vector3 newVector = new Vector3(
                    MathUtils.Lerp(a.x, b.x, t),
                    MathUtils.Lerp(a.y, b.y, t),
                    MathUtils.Lerp(a.z, b.z, t));
            return newVector;
        }
        
        public static Vector3 NaN => new Vector3(float.NaN, float.NaN, float.NaN);
    }
    
    public static class Vector3Ex {
        public static Vector3 Abs(this Vector3 v3) {
            Vector3 newVector = new Vector3(
                    Mathf.Abs(v3.x),
                    Mathf.Abs(v3.y),
                    Mathf.Abs(v3.z));
            return newVector;
        }
        
        public static Vector3 RoundToInt(this Vector3 v3) {
            Vector3 newVector = new Vector3(
                    Mathf.RoundToInt(v3.x),
                    Mathf.RoundToInt(v3.y),
                    Mathf.RoundToInt(v3.z));
            return newVector;
        }

        public static Vector3 FloorToInt(this Vector3 v3) {
            Vector3 newVector = new Vector3(
                    Mathf.FloorToInt(v3.x),
                    Mathf.FloorToInt(v3.y),
                    Mathf.FloorToInt(v3.z));
            return newVector;
        }

        public static Vector3 CeilToInt(this Vector3 v3) {
            Vector3 newVector = new Vector3(
                    Mathf.CeilToInt(v3.x),
                    Mathf.CeilToInt(v3.y),
                    Mathf.CeilToInt(v3.z));
            return newVector;
        }
        
        public static Vector3 Divide(this Vector3 v3, Vector3 otherVector) {
            Vector3 newVector = new Vector3(
                    v3.x / otherVector.x,
                    v3.y / otherVector.y,
                    v3.z / otherVector.z);
            return newVector;
        }

        public static Vector2 ToVector2(this Vector3 v3) {
            return new Vector2(v3.x, v3.y);
        }
        
        public static Vector3 Average(this IEnumerable<Vector3> currentList) {
            Vector3 sum = Vector3.zero;
            List<Vector3> vectorList = currentList.ToList();
            vectorList.ForEach(item => sum += item);
            return sum / vectorList.Count;
        }
    }
}