﻿using UnityEngine;

namespace MGU {
    public static class ObjectEx {
        public static bool IsNotNull(this Object obj) {
            return !ReferenceEquals(obj, null);
        }

        public static bool IsNull(this Object obj) {
            return ReferenceEquals(obj, null);
        }
    }
}