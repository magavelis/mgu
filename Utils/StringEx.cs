namespace MGU {
    public static class StringEx {
        public static bool IsNullOrEmpty(this string myString) {
            return string.IsNullOrEmpty(myString);
        }
    }
}