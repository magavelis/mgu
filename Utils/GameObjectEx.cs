using UnityEngine;

namespace MGU {
    public static partial class GameObjectEx {
        public static void SetSortingLayer(this GameObject gameObject, string sortingLayerName, bool recursive = true) {
            Renderer renderer = gameObject.GetComponent<Renderer>();
            if (renderer) {
                renderer.sortingLayerName = sortingLayerName;
            }

            if (recursive) {
                Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer>();
                foreach (Renderer r in renderers) {
                    r.sortingLayerName = sortingLayerName;
                }
            }
        }

        public static void SetSortingOrder(this GameObject gameObject, int order, bool recursive = true) {
            Renderer renderer = gameObject.GetComponent<Renderer>();
            if (renderer) {
                renderer.sortingOrder = order;
            }

            if (recursive) {
                Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer>();
                foreach (Renderer r in renderers) {
                    r.sortingOrder = order;
                }
            }
        }
        
        public static void SetSortingOrderDelta(this GameObject gameObject, int orderDelta, bool recursive = true) {
            Renderer renderer = gameObject.GetComponent<Renderer>();
            if (renderer) {
                renderer.sortingOrder += orderDelta;
            }

            if (recursive) {
                Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer>();
                foreach (Renderer r in renderers) {
                    r.sortingOrder += orderDelta;
                }
            }
        }
    }
}