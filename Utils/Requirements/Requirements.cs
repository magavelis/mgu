using UnityEngine;

namespace MGU {
    public abstract class Requirements: ScriptableObject {
        public abstract bool IsFulfilled();
    }
}