using UnityEngine;

namespace MGU {
    [CreateAssetMenu(fileName = "NoneRequirements",  menuName = "MGU/Utils/Requirements/None")]
    public class NoneRequirements: Requirements {
        public override bool IsFulfilled() {
            return true;
        }
    }
}