using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using Vector3 = UnityEngine.Vector3;

namespace MGU {
    public interface ICurveValueAccessor {
        float Position(float time);
    }

    public class EasingFunctionWrapper: ICurveValueAccessor {
        private readonly Func<float, float> easingFunction;

        public EasingFunctionWrapper(Func<float, float> easingFunction) {
            this.easingFunction = easingFunction;
        }

        public float Position(float time) {
            return easingFunction(time);
        }
    }

    public class AnimationCurveValueWrapper: ICurveValueAccessor {
        private readonly AnimationCurve animationCurve;

        public AnimationCurveValueWrapper(AnimationCurve animationCurve) {
            this.animationCurve = animationCurve;
        }

        public float Position(float time) {
            return animationCurve.Evaluate(time);
        }
    }

    public static partial class TransformEx {
        public static IEnumerator MoveStraightLocally(
                this Transform t,
                Vector3 toVector,
                float delay,
                float duration,
                AnimationCurve animationCurve,
                UnityAction onComplete) {
            ICurveValueAccessor curve = new AnimationCurveValueWrapper(animationCurve);
            return MoveStraightLocally(t,
                    toVector,
                    delay,
                    duration,
                    curve,
                    onComplete);
        }

        public static IEnumerator MoveStraightLocally(
                this Transform t,
                Vector3 toVector,
                float delay,
                float duration,
                Func<float, float> easing,
                UnityAction onComplete) {
            ICurveValueAccessor curve = new EasingFunctionWrapper(easing);
            return MoveStraightLocally(t,
                    toVector,
                    delay,
                    duration,
                    curve,
                    onComplete);
        }

        private static IEnumerator MoveStraightLocally(
                this Transform t,
                Vector3 toVector,
                float delay,
                float duration,
                ICurveValueAccessor curve,
                UnityAction onComplete) {
            Vector3 fromVector = t.localPosition;
            return CoroutineEx.Execute(
                    delay,
                    duration,
                    (float time) => {
                        float position = curve.Position(time);
                        t.localPosition = Vector3Utils.Lerp(fromVector, toVector, position);
                    },
                    () => {
                        onComplete?.Invoke();
                    });
        }
        
        public static IEnumerator MoveStraight(
                this Transform t,
                Vector3 toVector,
                float delay,
                float duration,
                Func<float, float> easing,
                UnityAction onComplete) {
            ICurveValueAccessor curve = new EasingFunctionWrapper(easing);
            return MoveStraight(t,
                    toVector,
                    delay,
                    duration,
                    curve,
                    onComplete);
        }

        private static IEnumerator MoveStraight(
                this Transform t,
                Vector3 toVector,
                float delay,
                float duration,
                ICurveValueAccessor curve,
                UnityAction onComplete) {
            Vector3 fromVector = t.position;
            return CoroutineEx.Execute(
                    delay,
                    duration,
                    (float time) => {
                        float position = curve.Position(time);
                        t.position = Vector3Utils.Lerp(fromVector, toVector, position);
                    },
                    () => {
                        onComplete?.Invoke();
                    });
        }
        
        public static IEnumerator Scale(
                this Transform t,
                Vector3 toVector,
                float duration,
                Func<float, float> easing,
                UnityAction onComplete) {
            ICurveValueAccessor curve = new EasingFunctionWrapper(easing);
            return Scale(t, toVector, duration, curve, onComplete);
        }

        private static IEnumerator Scale(
                this Transform t,
                Vector3 toVector,
                float duration,
                ICurveValueAccessor curve,
                UnityAction onComplete) {
            Vector3 fromVector = t.localScale;
            return CoroutineEx.Execute(
                    duration,
                    (float time) => {
                        float position = curve.Position(time);
                        t.localScale = Vector3Utils.Lerp(fromVector, toVector, position);
                    },
                    () => {
                        onComplete?.Invoke();
                    });
        }
    }
}