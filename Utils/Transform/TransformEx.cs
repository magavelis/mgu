using UnityEngine;

namespace MGU {
    public static partial class TransformEx {
        public static void RepositionOnCameraRight(this Transform transform, Camera camera, float width) {
            Vector2 screenSize = camera.OrthographicFrameSize();
            float positionX = screenSize.x / 2f + width / 2f;
            transform.SetPositionX(positionX);
        }

        public static void RepositionOnCameraLeft(this Transform transform, Camera camera, float width) {
            Vector2 screenSize = camera.OrthographicFrameSize();
            float positionX = -screenSize.x / 2f - width / 2f;
            transform.SetPositionX(positionX);
        }

        public static void SetPositionX(this Transform transform, float x) {
            Vector3 position = transform.position;
            position.x = x;
            transform.position = position;
        }
        
        public static void SetPositionY(this Transform transform, float y) {
            Vector3 position = transform.position;
            position.y = y;
            transform.position = position;
        }
        
        public static void SetLocalPositionX(this Transform transform, float x) {
            Vector3 position = transform.localPosition;
            position.x = x;
            transform.localPosition = position;
        }
        
        public static void SetLocalPositionY(this Transform transform, float y) {
            Vector3 position = transform.localPosition;
            position.y = y;
            transform.localPosition = position;
        } 
    }

    public static partial class TransformEx {
        public static void LookAt2D(this Transform transform, Vector3 direction) {
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            float adjustedAngle = angle - 90.0f;
            Quaternion rotation = Quaternion.AngleAxis(adjustedAngle, Vector3.forward);
            transform.rotation = rotation;
        }
    }
}