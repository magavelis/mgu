using System.Collections.Generic;

namespace MGU {
    public static class StackEx {
        public static T PopOrDefault<T>(this Stack<T> stack) {
            return (stack.Count > 0) ? stack.Pop() : default;
        }
    }
}