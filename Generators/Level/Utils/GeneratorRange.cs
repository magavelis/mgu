using System;
using UnityEngine;

namespace MGU.Generators.Level {
    [Serializable]
    public class GeneratorRange {
        [SerializeField] private bool hasRange = false;
        [SerializeField] private float minimumValue = 0f;
        [SerializeField] private float maximumValue = 0f;

        public float Length() {
            return maximumValue - minimumValue;
        }

        public bool Contain(float value) {
            if (hasRange == false) {
                return true;
            }

            return value > minimumValue && value < maximumValue;
        }

        public float BoundaryValue(float value) {
            if (hasRange == false) {
                return value;
            }

            return Mathf.Clamp(value, minimumValue, maximumValue);
        }
    }
}