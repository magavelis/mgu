using System;
using UnityEngine;

namespace MGU.Generators.Level {
    [Serializable]
    public class GeneratorLength {
        [SerializeField] private float initialValue = 30f;
        [SerializeField] private float maximumValue = 150f;
        [SerializeField] private float valueBonusByStep = 30f;

        public float CurrentValue(int step = 0) {
            float value = initialValue + valueBonusByStep * step;
            return Mathf.Clamp(value, initialValue, maximumValue);
        }
    }
}