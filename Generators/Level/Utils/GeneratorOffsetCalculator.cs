using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MGU.Generators.Level {
    [Serializable]
    public class GeneratorOffsetCalculator {
        public GeneratorRange Range;
        
        [SerializeField] private Vector2 offsetChangeDelta = Vector2.zero;
        private float OffsetChangeDelta => Random.Range(offsetChangeDelta.x, offsetChangeDelta.y);
        
        public float OffsetAdjustedByValue(float currentOffset, float value) {
            float delta = OffsetChangeDelta;
            float offset = currentOffset + delta + value;
            if (Range.Contain(offset) == false) {
                offset = Range.BoundaryValue(offset);
                offset += -delta;
            }

            return offset;
        }
    }
}