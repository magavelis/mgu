using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MGU.Generators.Level {
    [Serializable]
    public class GeneratorSpacing {
        [SerializeField] private Vector2 spaceBefore = Vector2.zero;
        public float SpaceBefore => Random.Range(spaceBefore.x, spaceBefore.y);

        [SerializeField] private Vector2 spaceAfter = Vector2.zero;
        public float SpaceAfter => Random.Range(spaceAfter.x, spaceAfter.y);
    }
}