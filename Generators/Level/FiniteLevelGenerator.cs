using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MGU;

namespace MGU.Generators.Level {
    [CreateAssetMenu(fileName = "FiniteLevelGenerator", menuName = "Game/LevelGenerator/Finite")]
    public class FiniteLevelGenerator: LevelGenerator {
        [Tooltip("Used to generate end object, when generation offset exceeds this value.")] 
        [SerializeField] private GeneratorLength generatorLength = null;

        private float CurrentLevelLength => generatorLength.CurrentValue(Difficulty);

        [SerializeField] private List<Generatable> generatableList = null;
        [SerializeField] private List<Generatable> obstacleGeneratableList = null;
        [SerializeField] private GeneratableObject start = null;
        [SerializeField] private GeneratableObject end = null;

        public override IEnumerator IE_Generate() {
            Generate(start, true);

            while (true) {
                if (ShouldGenerateEndObject()) {
                    Generate(end, true);
                    yield break;
                }

                if (ShouldGenerateNextObject()) {
                    GeneratedObject generatedObject = GenerateRandomObject(
                            generatableList, 
                            true);
                    
                    if (generatedObject.IsNotNull()) {
                        GenerateRandomObject(obstacleGeneratableList, false);
                    }
                }

                yield return Yielders.EndOfFrame;
            }
        }

        private bool ShouldGenerateEndObject() {
            return (CurrentHorizontalOffset >= CurrentLevelLength || CurrentVerticalOffset >= CurrentLevelLength);
        }
    }
}