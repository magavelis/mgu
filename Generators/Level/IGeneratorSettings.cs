using System.Collections.Generic;

namespace MGU.Generators.Level {
    public interface IGeneratorSettings {
        List<GeneratedObject> GeneratedObjectList { get; }

        GeneratorRange HorizontalRange { get; }
        float CurrentHorizontalOffset { get; }
        
        GeneratorRange VerticalRange { get; }
        float CurrentVerticalOffset { get; }

        int Difficulty { get; }
    }
}