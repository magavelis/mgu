using UnityEngine;
using MGU;

namespace MGU.Generators.Level {
    public abstract class GeneratedObject: MonoBehaviour {
        public Transform T { get; private set; }

        protected virtual void Awake() {
            T = transform;
        }

        public abstract void Construct(IGeneratorSettings settings);
        public abstract void HandleCollision(Collision2DConfiguration configuration);
        public abstract void HandleTrigger(Trigger2DConfiguration configuration);
    }
}