﻿using System.Collections.Generic;
using UnityEngine;

namespace MGU.Generators.Level {
    [CreateAssetMenu(fileName = "GeneratableObject", menuName = "Game/LevelGenerator/Generatables/Object")]
    public class GeneratableObject: Generatable, IGeneratableObject {
        [SerializeField] protected GeneratedObject Prefab = null;

        #region - IGeneratableObject
        [SerializeField] private GeneratorRange verticalRangeRequirements = new GeneratorRange();
        public GeneratorRange VerticalRangeRequirements => verticalRangeRequirements;

        [SerializeField] private GeneratorRange horizontalRangeRequirements = new GeneratorRange();
        public GeneratorRange HorizontalRangeRequirements => horizontalRangeRequirements;

        [SerializeField] private GeneratorSpacing verticalSpacing = new GeneratorSpacing();
        public GeneratorSpacing VerticalSpacing => verticalSpacing;

        [SerializeField] private GeneratorSpacing horizontalSpacing = new GeneratorSpacing();
        public GeneratorSpacing HorizontalSpacing => horizontalSpacing;

        [SerializeField] private Vector2 offset = Vector2.zero;
        public Vector2 Offset => offset;

        [SerializeField] private List<GeneratableObject> prioritizedGeneratableObjectList = new List<GeneratableObject>();
        public List<GeneratableObject> PrioritizedGeneratableObjectList => prioritizedGeneratableObjectList;

        public virtual GeneratedObject Create(Transform parent, IGeneratorSettings settings) {
            GeneratedObject generatedObject = Instantiate(Prefab, parent);
            generatedObject.Construct(settings);

            Transform transform = generatedObject.T;
            transform.localPosition = new Vector3 {
                    x = settings.CurrentHorizontalOffset + Offset.x,
                    y = settings.CurrentVerticalOffset + Offset.y,
                    z = 0.0f
            };

            return generatedObject;
        }
        
        public bool CanGenerateRepeatedly() {
            return false;
        }
        #endregion

        #region - IGeneratable
        public override bool CanGenerate(IGeneratorSettings settings) {
            return VerticalRangeRequirements.Contain(settings.CurrentVerticalOffset) &&
                   HorizontalRangeRequirements.Contain(settings.CurrentHorizontalOffset);
        }

        public override Generatable Object(IGeneratorSettings settings) {
            return this;
        }
        #endregion
    }
}