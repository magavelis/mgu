﻿using System.Collections.Generic;
using UnityEngine;

namespace MGU.Generators.Level {
    [CreateAssetMenu(fileName = "NoneGeneratable", menuName = "Game/LevelGenerator/Generatables/None")]
    public class NoneGeneratableObject: Generatable, IGeneratableObject {
        [SerializeField] private bool canGenerateRepeatedly = false;
        
        #region - IGeneratableObject
        [SerializeField] private GeneratorRange verticalRangeRequirements = new GeneratorRange();
        public GeneratorRange VerticalRangeRequirements => verticalRangeRequirements;
        
        [SerializeField] private GeneratorRange horizontalRangeRequirements = new GeneratorRange();
        public GeneratorRange HorizontalRangeRequirements => horizontalRangeRequirements;

        [SerializeField] private GeneratorSpacing verticalSpacing = new GeneratorSpacing();
        public GeneratorSpacing VerticalSpacing => verticalSpacing;
        
        [SerializeField] private GeneratorSpacing horizontalSpacing = new GeneratorSpacing();
        public GeneratorSpacing HorizontalSpacing => horizontalSpacing;

        [SerializeField] private Vector2 offset = Vector2.zero;
        public Vector2 Offset => offset;

        [SerializeField] private List<GeneratableObject> prioritizedGeneratableObjectList = new List<GeneratableObject>();
        public List<GeneratableObject> PrioritizedGeneratableObjectList => prioritizedGeneratableObjectList;

        public GeneratedObject Create(Transform parent, IGeneratorSettings settings) {
            return null;
        }

        public bool CanGenerateRepeatedly() {
            return canGenerateRepeatedly;
        }
        #endregion

        #region - IGeneratable
        public override bool CanGenerate(IGeneratorSettings settings) {
            return true;
        }

        public override Generatable Object(IGeneratorSettings settings) {
            return this;
        }
        #endregion
    }
}