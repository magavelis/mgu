using System.Collections.Generic;
using UnityEngine;

namespace MGU.Generators.Level {
    public interface IGeneratableObject {
        GeneratorRange VerticalRangeRequirements { get; }
        GeneratorRange HorizontalRangeRequirements { get; }

        GeneratorSpacing VerticalSpacing { get; }
        GeneratorSpacing HorizontalSpacing { get; }

        Vector2 Offset { get; }

        List<GeneratableObject> PrioritizedGeneratableObjectList { get; }

        GeneratedObject Create(Transform parent, IGeneratorSettings settings);

        bool CanGenerateRepeatedly();
    }
}