using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MGU.Generators.Level {
    public abstract class Generatable: ScriptableObject {
        private Generatable lastGeneratableObject;

        public abstract bool CanGenerate(IGeneratorSettings settings);
        public abstract Generatable Object(IGeneratorSettings settings);

        protected Generatable Object(Generatable generatable, IGeneratorSettings settings) {
            if (lastGeneratableObject == generatable) {
                if (generatable is IGeneratableObject generatableObject &&
                    generatableObject.CanGenerateRepeatedly() == false) {
                    return Object(settings);
                }
            }

            lastGeneratableObject = generatable;
            return generatable;
        }
    }

    public static class GeneratableEx {
        public static Generatable RandomGeneratable(
                this List<Generatable> generatableList,
                IGeneratorSettings settings) {
            int generatableCount = generatableList.Count;
            if (generatableCount == 0) {
                return null;
            }

            int index = Random.Range(0, generatableCount);
            Generatable generatable = generatableList[index];
            return generatable.Object(settings);
        }
    }
}