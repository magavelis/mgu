using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MGU.Generators.Level {
    [CreateAssetMenu(fileName = "ChanceBasedGroupGeneratable",
            menuName = "Game/LevelGenerator/Generatables/ChanceBased")]
    public partial class ChanceBasedGroupGeneratable: Generatable {
        [SerializeField] private List<ChanceBasedGeneratable> generatableList = new List<ChanceBasedGeneratable>();
        private Generatable lastGeneratedObject;
        
        public override bool CanGenerate(IGeneratorSettings settings) {
            if (generatableList.Count == 0) {
                return false;
            }

            List<ChanceBasedGeneratable> filteredGeneratableObjectList = generatableList
                    .FindAll(obj => obj.CanGenerate(settings));
            return filteredGeneratableObjectList.Count > 0;
        }

        public override Generatable Object(IGeneratorSettings settings) {
            List<ChanceBasedGeneratable> filteredGeneratableObjectList = generatableList
                    .FindAll(obj => obj.CanGenerate(settings));
            
            Generatable generatable = filteredGeneratableObjectList.RandomGeneratable(settings);
            Generatable generatableCandidate = generatable.Object(settings);
            return Object(generatableCandidate, settings);
        }
    }

    public partial class ChanceBasedGroupGeneratable {
        [Serializable]
        public class ChanceBasedGeneratable {
            [SerializeField] private Generatable generatable = null;
            [SerializeField] private float generationChance = 10;
            [SerializeField] private float generationChanceBonusByDifficulty = 10;
            [SerializeField] private float maximumGenerationChance = -1;

            public float GenerationChange(int difficulty) {
                float chance = generationChance + generationChanceBonusByDifficulty * difficulty;
                return maximumGenerationChance < 0
                        ? Mathf.Max(0, chance)
                        : Mathf.Clamp(chance, 0, maximumGenerationChance);
            }

            public bool CanGenerate(IGeneratorSettings settings) {
                return generatable != null && generatable.CanGenerate(settings);
            }

            public Generatable Object(IGeneratorSettings settings) {
                return generatable.Object(settings);
            }
        }
    }

    public static class ChanceBasedGeneratableEx {
        public static Generatable RandomGeneratable(
                this List<ChanceBasedGroupGeneratable.ChanceBasedGeneratable> generatableList,
                IGeneratorSettings settings) {
            int generatableCount = generatableList.Count;
            if (generatableCount == 0) {
                return null;
            }

            float totalChange = generatableList.TotalChance(settings);
            float chance = Random.Range(0, totalChange);
            float chanceSum = -1f;
            foreach (ChanceBasedGroupGeneratable.ChanceBasedGeneratable generatable in generatableList) {
                chanceSum += generatable.GenerationChange(settings.Difficulty);
                if (chanceSum > chance) {
                    return generatable.Object(settings);
                }
            }

            int index = Random.Range(0, generatableCount);
            return generatableList[index].Object(settings);
        }

        private static float TotalChance(
                this IEnumerable<ChanceBasedGroupGeneratable.ChanceBasedGeneratable> generatableList,
                IGeneratorSettings settings) {
            return generatableList.Sum(group => group.GenerationChange(settings.Difficulty));
        }
    }
}