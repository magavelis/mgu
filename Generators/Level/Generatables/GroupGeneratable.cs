using System.Collections.Generic;
using UnityEngine;

namespace MGU.Generators.Level {
    [CreateAssetMenu(fileName = "GroupGeneratable", menuName = "Game/LevelGenerator/Generatables/Group")]
    public class GroupGeneratable: Generatable {
        [SerializeField] private List<Generatable> generatableList = null;

        #region - Generatable
        public override bool CanGenerate(IGeneratorSettings settings) {
            if (generatableList.Count == 0) {
                return false;
            }
            
            List<Generatable> filteredGeneratableObjectList = generatableList
                    .FindAll(obj => obj.CanGenerate(settings));
            return filteredGeneratableObjectList.Count > 0;
        }

        public override Generatable Object(IGeneratorSettings settings) {
            List<Generatable> filteredGeneratableObjectList = generatableList
                    .FindAll(obj => obj.CanGenerate(settings));

            Generatable generatable = filteredGeneratableObjectList.RandomGeneratable(settings);
            Generatable generatableCandidate = generatable.Object(settings);
            return Object(generatableCandidate, settings);
        }
        #endregion
    }
}