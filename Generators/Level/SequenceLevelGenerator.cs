using System.Collections;
using System.Collections.Generic;
using MGU;
using UnityEngine;

namespace MGU.Generators.Level {
    [CreateAssetMenu(fileName = "SequenceLevelGenerator", menuName = "Game/LevelGenerator/Sequence")]
    public class SequenceLevelGenerator: LevelGenerator {
        [SerializeField] private List<Generatable> generatableList = null;
        [SerializeField] private GeneratableObject start = null;
        [SerializeField] private GeneratableObject end = null;

        public override IEnumerator IE_Generate() {
            Generate(start, true);

            int generatedItemsCount = 0;
            while (generatedItemsCount < generatableList.Count) {
                if (ShouldGenerateNextObject()) {
                    GenerateRandomObject(generatableList[generatedItemsCount], true);
                    generatedItemsCount++;
                }

                yield return Yielders.EndOfFrame;
            }

            Generate(end, true);
        }
    }
}