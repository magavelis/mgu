﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MGU.Generators.Level {
    public abstract class LevelGenerator: ScriptableObject, IGeneratorSettings {
        public int LevelRequirement;

        protected Transform Parent;
        protected Transform Target;

        protected float RequiredGenerationDistance;

        [SerializeField] private Vector3 generationStartingPoint = Vector3.zero;
        [SerializeField] private GeneratorOffsetCalculator horizontalOffset = null;
        [SerializeField] private GeneratorOffsetCalculator verticalOffset = null;

        public float GeneratedLevelLength {
            get {
                GeneratedObject lastObject = GeneratedObjectList.LastOrDefault();
                if (lastObject == null) {
                    return 0;
                }

                Vector3 lastGeneratedObjectPosition = lastObject.T.localPosition;
                float horizontalLength = lastGeneratedObjectPosition.x - generationStartingPoint.x;
                float verticalLength = lastGeneratedObjectPosition.y - generationStartingPoint.y;
                return Mathf.Max(horizontalLength, verticalLength);
            }
        }

        private IGeneratableObject lastUsedGeneratableObject;

        #region - IGeneratorSettings
        public List<GeneratedObject> GeneratedObjectList { get; private set; }
        public GeneratorRange HorizontalRange { get; private set; }
        public float CurrentHorizontalOffset { get; private set; }
        public GeneratorRange VerticalRange { get; private set; }
        public float CurrentVerticalOffset { get; private set; }
        public int Difficulty { get; private set; }
        #endregion

        public virtual void Construct(Transform parent, Transform target, int levelNumber) {
            SetTarget(target);
            
            Parent = parent;
            GeneratedObjectList = new List<GeneratedObject>();
            RequiredGenerationDistance = 20f; // TODO: Calculate dependent on screen

            HorizontalRange = horizontalOffset.Range;
            CurrentHorizontalOffset = generationStartingPoint.x;

            VerticalRange = verticalOffset.Range;
            CurrentVerticalOffset = generationStartingPoint.y;

            Difficulty = levelNumber - LevelRequirement;
        }

        public virtual void SetTarget(Transform target) {
            Target = target;
        }

        public abstract IEnumerator IE_Generate();

        protected bool ShouldGenerateNextObject() {
            if (Target.IsNull()) {
                return false;
            }
            
            GeneratedObject lastGeneratedObject = GeneratedObjectList.LastOrDefault();
            if (lastGeneratedObject == null) {
                return true;
            }

            Vector3 lastGeneratedObjectPosition = lastGeneratedObject.T.localPosition;
            float distance = Vector2.Distance(Target.position, lastGeneratedObjectPosition);
            return (distance < RequiredGenerationDistance);
        }

        protected GeneratedObject GenerateRandomObject(List<Generatable> generatableList, bool shouldUpdateGeneratorState) {
            if (generatableList == null) {
                return null;
            }

            Generatable generatable = generatableList.RandomGeneratable(this);
            Debug.Log($"Generating object: {generatable.name}");
            return GenerateRandomObject(generatable, shouldUpdateGeneratorState);
        }

        protected GeneratedObject GenerateRandomObject(Generatable generatable, bool shouldUpdateGeneratorState) {
            if (generatable.Object(this) is IGeneratableObject generatableObject) {
                IGeneratableObject adjustedGeneratableObject = PrioritizedGeneratableObject(generatableObject);
                return Generate(adjustedGeneratableObject, shouldUpdateGeneratorState);
            }

            return null;
        }

        protected GeneratedObject Generate(IGeneratableObject generatableObject, bool shouldUpdateGeneratorState) {
            if (generatableObject == null) {
                return null;
            }

            if (shouldUpdateGeneratorState && GeneratedObjectList.Count > 0) {
                float horizontalSpaceBefore = generatableObject.HorizontalSpacing.SpaceBefore;
                CurrentHorizontalOffset = horizontalOffset.OffsetAdjustedByValue(
                        CurrentHorizontalOffset,
                        horizontalSpaceBefore);

                float verticalSpaceBefore = generatableObject.VerticalSpacing.SpaceBefore;
                CurrentVerticalOffset = verticalOffset.OffsetAdjustedByValue(
                        CurrentVerticalOffset,
                        verticalSpaceBefore);
            }

            GeneratedObject generatedObject = generatableObject.Create(Parent, this);
            if (shouldUpdateGeneratorState) {
                float horizontalSpaceAfter = generatableObject.HorizontalSpacing.SpaceAfter;
                CurrentHorizontalOffset = horizontalOffset.OffsetAdjustedByValue(
                        CurrentHorizontalOffset,
                        horizontalSpaceAfter);

                float verticalSpaceAfter = generatableObject.VerticalSpacing.SpaceAfter;
                CurrentVerticalOffset = verticalOffset.OffsetAdjustedByValue(
                        CurrentVerticalOffset,
                        verticalSpaceAfter);

                GeneratedObjectList.TryAdd(generatedObject);
                lastUsedGeneratableObject = generatableObject;
            }

            return generatedObject;
        }

        private IGeneratableObject PrioritizedGeneratableObject(IGeneratableObject generatableObject) {
            if (lastUsedGeneratableObject == null) {
                return generatableObject;
            }

            List<GeneratableObject> generatableObjectList = lastUsedGeneratableObject.PrioritizedGeneratableObjectList;
            int generatableObjectsCount = generatableObjectList.Count;
            if (generatableObjectsCount == 0) {
                return generatableObject;
            }

            return generatableObjectList[Random.Range(0, generatableObjectsCount)];
        }
    }
}