using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace MGU.Bonus {
    public abstract class Prize: MonoBehaviour {
        public string Identifier = "";
        public string Title = "";
        public string Description = "";

        protected bool isOpen = false;
        public bool IsOpen() {
            return !CanBeOpened() || isOpen;
        }
        
        public virtual bool CanBeOpened() {
            return false;
        }

        public virtual IEnumerator Open(Transform parent, UnityAction onCompletion) {
            yield break;
        }

        protected void SetAsOpen() {
            isOpen = true;
        }
        
        public virtual void Claim() {
            PrizeEvents.OnClaim.Invoke(this);
        }
    }
}