using UnityEngine.Events;

namespace MGU.Bonus {
    public static class PrizeEvents {
        public class OpenEvent: UnityEvent<string> { }
        public static readonly OpenEvent OnOpen = new OpenEvent();
        
        public class ClaimEvent: UnityEvent<Prize> { }
        public static readonly ClaimEvent OnClaim = new ClaimEvent();
    }
}