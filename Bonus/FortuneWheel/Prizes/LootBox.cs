using System.Collections;
using System.Collections.Generic;
using MGU;
using UnityEngine;
using UnityEngine.Events;

namespace MGU.Bonus {
    public class LootBox: Prize {
        [SerializeField] private ParticleEffect openEffect = null;
        [SerializeField] private float openDuration = 1f;
        [SerializeField] private List<string> itemList = new List<string>();

        [HideInInspector] public string OpenedItem = "";
        
        private string RandomItem {
            get {
                int itemsCount = itemList.Count;
                if (itemsCount == 0) {
                    return null;
                }

                int index = Random.Range(0, itemsCount);
                return itemList[index];
            }
        }

        public override bool CanBeOpened() {
            return !isOpen;
        }

        public override IEnumerator Open(Transform parent, UnityAction onCompletion) {
            OpenedItem = RandomItem;
            openEffect.Create(parent, false);

            yield return transform.Scale(Vector3.zero,
                    openDuration,
                    Easing.Linear,
                    null);
            
            SetAsOpen();
            PrizeEvents.OnOpen.Invoke(OpenedItem);
            onCompletion?.Invoke();
            
            Destroy(gameObject);
        }
    }
}