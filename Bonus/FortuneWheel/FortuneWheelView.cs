﻿using MGU.UI;
using MGU.Bonus;
using UnityEngine;

public interface IFortuneWheelStorage {
    int AvailableSpinCount();
}

public class FortuneWheelView: View {
    [SerializeField] private FortuneWheel fortuneWheel = null;
    [SerializeField] private ButtonView spinButton = null;

    private IFortuneWheelStorage storage = null;    
    
    protected override void Awake() {
        base.Awake();

        SetUpView();
    }

    public void Construct(IFortuneWheelStorage storage) {
        this.storage = storage;
    }
    
    public void PrepareToSpin() {
        fortuneWheel.Prepare();
    }

    private void SetUpView() {
        SetUpFortuneWheel();
        SetUpSpinButton();
    }

    private void SetUpFortuneWheel() {
        // Nothing to set up
    }

    private void SetUpSpinButton() {
        spinButton.OnClick.AddListener(SpinIfCan);
    }

    private void SpinIfCan() {
        int spinCount = storage.AvailableSpinCount();
        if (spinCount <= 0) {
            return;
        }
                
        fortuneWheel.Spin();
    }
}