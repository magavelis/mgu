﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MGU;

#if MASTER_AUDIO
using DarkTonic.MasterAudio;
#endif

using Random = UnityEngine.Random;

namespace MGU.Bonus {
    public class FortuneWheel: MonoBehaviour {
        private enum SpinDirection {
            Forward, 
            Backward
        }
        
        private const float fullAngle = 360f;
        private const float padding = 5f;
        
        [Serializable]
        public class WheelItem {
            public Prize Prize;
            [Range(0, 1000)] public float Chance;
        }
        
        [SerializeField] private Transform spinner = null;
        [SerializeField] private int spinDuration = 5;
        [SerializeField] private SpinDirection spinDirection = SpinDirection.Backward;

        [SerializeField] private AnimationCurve animationCurve = null;

        [SerializeField] private bool isAutoSpinEnabled = false;
        [SerializeField] private List<WheelItem> itemList = new List<WheelItem>();

        #if MASTER_AUDIO
        [SerializeField] private string masterAudioSoundGroupName = "fx.click.loop";
        #endif
        
        /// <summary>
        /// Use it only if your project does not have MasterAudio plugin.
        /// Otherwise variable will be assigned automatically if `masterAudioSoundGroupName` is assigned to correct value
        /// </summary>
        [SerializeField] private AudioSource audioSource = null;
        private bool isAudioSourceAvailable = false;

        private bool isSpinning = false;
        private bool didSpin = false;
        private float initialWheelOffset;

        private float velocity = 0.0f;
        
        private bool IsRotating => isSpinning || (isAutoSpinEnabled && !didSpin);

        private void Awake() {
            SetUpWheelParameters();
        }

        private void Update() {
            SpinByDefaultIfNeeded();
            PlayOrUpdateCurrentSoundIfNeeded();
        }

        public void Prepare() {
            if (isSpinning) {
                Debug.LogError("`FortuneWheel` can't be prepared for next spin while it is still spinning");
                return;
            }
            
            didSpin = false;
            isSpinning = false;
        }
        
        public void Spin() {
            if (isSpinning) {
                return;
            }

            isSpinning = true;
            didSpin = true;
            
            float chance = RandomChance();
            StartCoroutine(IE_Spin(spinDuration,chance));
        }

        private void SetUpWheelParameters() {
            SetUpInitialOffset();
            SetUpAudio();
        }

        private void SetUpInitialOffset() {
            initialWheelOffset = spinner.eulerAngles.z;
        }
        
        private void SetUpAudio() {
            if (audioSource == null) {
                #if MASTER_AUDIO
                // audioSource = MasterAudio.GetNextVariationForSoundGroup(masterAudioSoundGroupName);
                // if (audioSource == null) {
                //     Debug.LogError("Cannot find AudioSource using 'GetNextVariationForSoundGroup' make sure that Variation Sequence is Top to bottom");
                // }
                #endif
            }

            isAudioSourceAvailable = !(audioSource == null);
        }
        
        private void SpinByDefaultIfNeeded() {
            if (isAutoSpinEnabled == false) {
                return;
            }

            if (didSpin || isSpinning) {
                return;
            }

            RotateSpinner(SelectedSpinDirection(), 1);
        }

        private void PlayOrUpdateCurrentSoundIfNeeded() {
            if (!isAudioSourceAvailable) {
                return;
            }
            
            if (!IsRotating) {
                if (audioSource.isPlaying) {
                    audioSource.Stop();
                }
            } else {
                if (!audioSource.isPlaying) {
                    audioSource.Play();
                }
                
                const float maxVelocity = 18;
                const float maxPitch = 1.5f;
                float clampedVelocity = Mathf.Clamp(velocity, 0, maxVelocity);
                float pitchValue = MathUtils.Remap01(clampedVelocity, 0, maxVelocity) * maxPitch;
                audioSource.pitch = pitchValue;
            }
        }

        private void RotateSpinner(Vector3 axis, float angle) {
            spinner.Rotate(axis, angle);
            velocity = axis.magnitude * angle;
        }
        
        private float RandomChance() {
            float totalChance = TotalChance();
            float randomChance = Random.Range(0, totalChance);
            return randomChance;
        }
        
        private float TotalChance() {
            float totalChance = itemList.Sum(item => item.Chance);
            return totalChance;
        }
        
        private Vector3 SelectedSpinDirection() {
            switch (spinDirection) {
                case SpinDirection.Forward: return Vector3.forward;
                case SpinDirection.Backward: return Vector3.back;
                default: return Vector3.zero;
            }
        }

        private float AngleByChance(float chance) {
            int index = WheelItemIndexByChance(chance);

            // In case, when we have 4 items, angle can be: 0, 90, 180, 270
            float offset = fullAngle / itemList.Count / 2f;
            float angle = MathUtils.Remap01(index, 0, itemList.Count); 
            float itemCenterAngle = angle * fullAngle + (offset - initialWheelOffset);
            float minimumAllowedAngle = itemCenterAngle - offset + padding;
            float maximumAllowedAngle = itemCenterAngle + offset - padding;
            float calculatedAngle = Random.Range(minimumAllowedAngle, maximumAllowedAngle);
            float angleAdjustedBySpinDirection = calculatedAngle * SelectedSpinDirection().z;
            
            return angleAdjustedBySpinDirection;
        }

        private int WheelItemIndexByChance(float chance) {
            IEnumerable orderedItemList = itemList.OrderBy(item => item.Chance).ToList();

            float chancesSum = 0;
            foreach (WheelItem item in orderedItemList) {
                chancesSum += item.Chance;
                if (chancesSum > chance) {
                    return itemList.IndexOf(item);
                }
            }

            return -1;
        }

        private WheelItem WheelItemByChance(float chance) {
            int index = WheelItemIndexByChance(chance);
            return (index >= 0) ? itemList[index] : null;
        }

        private float WheelOffset() {
            return (spinner.eulerAngles.z - initialWheelOffset) * SelectedSpinDirection().z;
        }

        private IEnumerator IE_Spin(float duration, float chance) {
            FortuneWheelEvents.OnSpinStart.Invoke();

            float amount = duration * fullAngle;
            float itemAngle = AngleByChance(chance);
            float totalAmount = amount + itemAngle - WheelOffset();
            float startTime = Time.time;
            float previousAngle = 0f;
            Vector3 direction = SelectedSpinDirection();

            while (previousAngle < totalAmount) {
                float currentTime = Mathf.Clamp01((Time.time - startTime) / duration);
                float position = animationCurve.Evaluate(currentTime);
                float partialAmount = Mathf.Lerp(0, totalAmount, position);
                float angleDelta = partialAmount - previousAngle;

                RotateSpinner(direction, angleDelta);

                previousAngle = partialAmount;

                yield return Yielders.FixedUpdate;
            }

            WheelItem item = WheelItemByChance(chance);
            
            initialWheelOffset = spinner.eulerAngles.z;
            isSpinning = false;
            FortuneWheelEvents.OnSpinStop.Invoke(item.Prize);
        }
    }
}