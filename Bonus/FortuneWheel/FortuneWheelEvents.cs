using UnityEngine.Events;

namespace MGU.Bonus {
    public static class FortuneWheelEvents {
        public class SpinStartEvent: UnityEvent { }
        public static readonly SpinStartEvent OnSpinStart = new SpinStartEvent();

        public class SpinStopEvent: UnityEvent<Prize> { }
        public static readonly SpinStopEvent OnSpinStop = new SpinStopEvent();
    }
}