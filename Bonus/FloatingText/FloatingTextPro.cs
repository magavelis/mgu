﻿using System.Collections;
using UnityEngine;
using TMPro;
using MGU.Common;

namespace MGU.Bonus {
    public class FloatingTextPro: MonoBehaviour {
        private static readonly int dilate = Shader.PropertyToID("_FaceDilate");

        public enum Priority {
            Lowest = 0,
            Low = 1,
            Default = 2,
            High = 3,
            Highest = 4
        }
        
        [SerializeField] private TextMeshPro textComponent = null;
        [SerializeField] private MeshRenderer meshRenderer = null;
        [SerializeField] private Vector2 fontSize = Vector2.zero;
        [SerializeField] private Vector2 faceDilate = Vector2.zero;
        [SerializeField] private float lifetime = -1.0f;

        private Material textMaterial;
        private Transform t;

        private void Awake() {
            t = transform;
            textMaterial = meshRenderer.material;
        }

        public void Construct(string text) {
            t.localScale = Vector3.zero;
            textComponent.text = text;
        }

        public void Show(Priority priority) {
            float priorityValue = PriorityValue(priority);

            textComponent.fontSize = FontSize(priorityValue);
            textMaterial.SetFloat(dilate, FaceDilate(priorityValue));

            StartCoroutine(IE_LifetimeAnimation());
        }

        private float PriorityValue(Priority priority) {
            return MathUtils.Remap01((int) priority, (int) Priority.Lowest, (int) Priority.Highest);
        }

        private float FontSize(float priorityValue) {
            return Mathf.Lerp(fontSize.x, fontSize.y, priorityValue);
        }

        private float FaceDilate(float priorityValue) {
            return Mathf.Lerp(faceDilate.x, faceDilate.y, priorityValue);
        }

        private IEnumerator IE_LifetimeAnimation() {
            yield return t.Scale(
                    Vector3.one,
                    K.Animation.DefaultDuration,
                    new Easing.Back().EaseOut,
                    null);

            if (lifetime > 0.0f) {
                ICurveValueAccessor curve = new EasingFunctionWrapper(Easing.Exponential.EaseOut);
                float fromDilateValue = textMaterial.GetFloat(dilate);
                float toDilateValue = -1.0f;

                Vector3 fromScaleValue = t.localScale;
                Vector3 toScaleValue = new Vector3(0.9f, 0.9f, 0.9f);

                Vector3 fromPositionValue = t.position;
                Vector3 toPositionValue = fromPositionValue + new Vector3(0.0f, 0.5f, 0.0f);

                yield return CoroutineEx.Execute(
                        lifetime,
                        (float time) => {
                            float position = curve.Position(time);

                            float newDilateValue = MathUtils.Lerp(fromDilateValue, toDilateValue, position);
                            textMaterial.SetFloat(dilate, newDilateValue);
                            
                            Vector3 newScaleValue = Vector3Utils.Lerp(fromScaleValue, toScaleValue, position);
                            t.localScale = newScaleValue;

                            Vector3 newPositionValue = Vector3Utils.Lerp(fromPositionValue, toPositionValue, position);
                            t.position = newPositionValue;
                        },
                        () => {
                            Destroy(gameObject);
                        });
            }
        }
    }
}