﻿using UnityEngine;
using MGU;

namespace MGU.Bonus {
    public class ParticleSpawner: MonoBehaviour {
        [SerializeField] private ParticleEffect effect = null;
        [SerializeField] private Transform spawnPositionObject = null;

        public void Spawn() {
            if (effect.IsNull()) {
                return;
            }
            
            if (spawnPositionObject == null) {
                effect.Create(transform, false);
            } else {
                effect.Create(spawnPositionObject.position, transform);
            }
        }
    }
}
