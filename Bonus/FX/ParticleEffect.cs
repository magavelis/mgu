﻿using UnityEngine;
using MGU;

namespace MGU.Bonus {
    public class ParticleEffect: MonoBehaviour {
        [SerializeField] private GameObject prefab = null;

        private ParticleSystem particle;
        private ParticleSystem.ShapeModule shape;
        private ParticleSystemRenderer[] renderers;
        public float ShapeRadius {
            set => shape.radius = value;
        }

        public string SortingLayer {
            set {
                foreach (ParticleSystemRenderer r in renderers) {
                    r.sortingLayerName = value;
                }
            }
        }
        
        public void Awake() {
            Construct(gameObject);
        }

        public void Create(Vector3 position, Transform parent = null) {
            GameObject particleObject = Instantiate(
                    prefab,
                    position,
                    prefab.transform.rotation,
                    parent);
            
            Construct(particleObject);
        }

        public void Create(Transform parent, bool shouldKeepWorldPosition) {
            GameObject particleObject = Instantiate(
                    prefab,
                    parent,
                    shouldKeepWorldPosition);
            
            Construct(particleObject);
        }
        
        private void Construct(GameObject particleObject) {
            particle = particleObject.GetComponentInChildren<ParticleSystem>();
            shape = particle.shape;
            renderers = particle.GetComponentsInChildren<ParticleSystemRenderer>();
        }

        public void Play() {
            if (particle.isPlaying) {
                return;
            }
            
            particle.Play(true);
        }

        public void Stop() {
            particle.Stop(true, ParticleSystemStopBehavior.StopEmitting);
        }
    }
}