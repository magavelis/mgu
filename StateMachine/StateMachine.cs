﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace MGU.Logic {
    public class UnknownState: State {
        public UnknownState(): base("") { }
        public override void Act() { }
    }

    public class StateMachine {
        public class StateChangeEvent: UnityEvent<State> { }
        public readonly StateChangeEvent OnStateChange = new StateChangeEvent();

        private readonly List<State> stateList = new List<State>();
        public State CurrentState { get; private set; } = new UnknownState();

        public bool Condition() {
            bool isConditionValid = CurrentState.Condition();
            if (isConditionValid == false && CurrentState.NextStateConfiguration != null) {
                TryTransitToState(CurrentState.NextStateConfiguration);
            }

            return isConditionValid;
        }

        public void Act() {
            CurrentState.Act();
        }

        public void AddState(State state) {
            if (IsStateMachineContainState(state)) {
                Debug.LogError($"[StateMachine]: Impossible to add state {state} because state has already been added");
                return;
            }

            stateList.Add(state);
        }

        public void AddStates(State[] states) {
            foreach (State state in states) {
                AddState(state);
            }
        }

        public void SetNextState(IStateConfiguration configuration, bool shouldDoTransition) {
            CurrentState.NextStateConfiguration = configuration;
            if (shouldDoTransition) {
                TryTransitToState(configuration);
            }
        }
        
        public void TryTransitToState(IStateConfiguration configuration) {
            State state = GetState(configuration);
            if (state == null) {
                Debug.LogError($"[StateMachine]: Transit impossible to {configuration.Name} because state does not exist.");
                return;
            }

            if (CurrentState != null) {
                if (CurrentState.Name == configuration.Name) {
                    Debug.Log($"[StateMachine]: Cancelled, trying switch to current state: {CurrentState.Name}");
                    return;
                }

                if (CurrentState.IsTransitAvailable(configuration) == false) {
                    Debug.Log($"[StateMachine]: Transit impossible from {CurrentState.Name} to {configuration.Name}");
                    return;
                }

                CurrentState.DoBeforeLeaving();
            }

            CurrentState = state;

            CurrentState.DoBeforeEntering(configuration);

            OnStateChange.Invoke(CurrentState);
        }

        public void Clear() {
            OnStateChange.RemoveAllListeners();

            stateList.ForEach(state => state.Clear());
            stateList.Clear();
        }

        private State GetState(IStateConfiguration configuration) {
            return stateList.SingleOrDefault(state => state.Name == configuration.Name);
        }

        private bool IsStateMachineContainState(State state) {
            return stateList.Contains(state);
        }
    }
}