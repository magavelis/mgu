namespace MGU.Logic {
    public interface IStateConfiguration {
        string Name { get; }
    }
}