namespace MGU.Logic {
    public class DefaultStateConfiguration: IStateConfiguration {
        public string Name { get; }

        public DefaultStateConfiguration(string stateName) {
            Name = stateName;
        }
    }
}