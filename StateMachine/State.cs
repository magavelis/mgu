﻿using System.Collections.Generic;
using UnityEngine.Events;

namespace MGU.Logic {
    public abstract class State: IStateConfiguration {
        public class StateStartEvent: UnityEvent<State> { }
        public readonly StateStartEvent OnStateStart = new StateStartEvent();

        public class StateEndEvent: UnityEvent<State> { }
        public readonly StateEndEvent OnStateEnd = new StateEndEvent();

        public string Name { get; }
        private readonly List<string> dependentStateList;

        public IStateConfiguration NextStateConfiguration { get; set; } = null;

        protected State(string name, List<string> dependencyList = null) {
            Name = name;
            dependentStateList = dependencyList;
        }

        public virtual bool IsTransitAvailable(IStateConfiguration configuration) {
            if (dependentStateList == null) {
                return true;
            }

            foreach (string dependentStateName in dependentStateList) {
                if (dependentStateName == configuration.Name) {
                    return true;
                }
            }

            return false;
        }

        public virtual void Clear() {
            OnStateStart.RemoveAllListeners();
            OnStateEnd.RemoveAllListeners();
        }

        public virtual void DoBeforeEntering(IStateConfiguration configuration) {
            OnStateStart.Invoke(this);
        }

        public virtual void DoBeforeLeaving() {
            OnStateEnd.Invoke(this);
        }

        public virtual bool Condition() {
            return (NextStateConfiguration == null);
        }

        public abstract void Act();
    }
}