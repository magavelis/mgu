using UnityEngine;

namespace MGU.Services.RateUs {
    using Analytics;

    public interface IRateUsStorageManager {
        bool IsRatedOnVersion(string version);
        void SetRatedOnVersion(string version);
    }

    public class RateUsManager {
        private readonly IRateUsStorageManager storageManager;
        private readonly AnalyticsManager analyticsManager;

        private readonly Requirements requirements;

        public RateUsManager(
                IRateUsStorageManager storageManager,
                AnalyticsManager analyticsManager,
                Requirements requirements) {
            
            this.storageManager = storageManager;
            this.analyticsManager = analyticsManager;
            this.requirements = requirements;
        }

        public void RequestReviewIfCan() {
            if (!CanRequestReview()) {
                return;
            }

            RequestReview();
        }

        private void RequestReview() {
            #if UNITY_IOS && !UNITY_EDITOR
            if (RateUsNative.IsNativeReviewSupported()) {
                RateUsNative.RequestReview();
            } else {
                HandleReviewRequestInUnity();
            }
            #else
            HandleReviewRequestInUnity();
            #endif

            string currentAppVersion = CurrentAppVersion();
            
            analyticsManager?.LogEvent(new Analytics.RateUsWasRequested(currentAppVersion));
            storageManager.SetRatedOnVersion(currentAppVersion);
        }

        private void HandleReviewRequestInUnity() {
            Debug.LogError("Rate us in Unity not implemented yet!");
        }

        private bool CanRequestReview() {
            return isNotRatedOnCurrentAppVersion() && requirements.IsFulfilled();
        }

        private bool isNotRatedOnCurrentAppVersion() {
            return !storageManager.IsRatedOnVersion(version: CurrentAppVersion());
        }

        private string CurrentAppVersion() {
            return Application.version;
        }
    }
}