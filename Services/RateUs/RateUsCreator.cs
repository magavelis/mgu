using UnityEngine;
using MGU.Services.Analytics;

namespace MGU.Services.RateUs {
    public class RateUsCreator: MonoSingleton<RateUsCreator> {
        [SerializeField] private Requirements requirements = null;

        public RateUsManager RateUsManager(IRateUsStorageManager storageManager, AnalyticsManager analyticsManager) {
            if (requirements == null) {
                Debug.LogError("Unable to create `RateUsManager` without `Requirements`");
                return null;
            }
            
            return new RateUsManager(
                    storageManager,
                    analyticsManager,
                    requirements);
        }
    }
}