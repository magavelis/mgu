﻿public class RateUsNative {
    #if UNITY_IOS && !UNITY_EDITOR
    [System.Runtime.InteropServices.DllImport ("__Internal")] private static extern bool isNativeReviewSupported();
	[System.Runtime.InteropServices.DllImport ("__Internal")] private static extern void requestReview();
    #endif

    public static bool IsNativeReviewSupported() {
        #if UNITY_IOS && !UNITY_EDITOR
		return isNativeReviewSupported();
        #endif
        return false;
    }

    public static void RequestReview() {
        #if UNITY_IOS && !UNITY_EDITOR
		requestReview();
        #endif
    }
}