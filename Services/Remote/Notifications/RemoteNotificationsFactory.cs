using System.Collections.Generic;
using UnityEngine;
using MGU.Services.Remote.Notifications.Firebase;

namespace MGU.Services.Remote.Notifications {
    public static class RemoteNotificationsFactory {
        private static readonly Dictionary<RemoteNotificationsProviderType, IRemoteNotificationsFactory> factories =
                new Dictionary<RemoteNotificationsProviderType, IRemoteNotificationsFactory> {
                        { RemoteNotificationsProviderType.Firebase, new FirebaseRemoteNotificationsFactory() }
                };

        public static IRemoteNotifications Create(
                RemoteNotificationsProviderType type,
                RemoteNotificationsSettings settings) {
            
            if (factories.ContainsKey(type) == false) {
                Debug.LogError($"Unexpected Remote Notifications provider: {type}");
                return null;
            }

            return factories[type].Create(settings);
        }
    }
}