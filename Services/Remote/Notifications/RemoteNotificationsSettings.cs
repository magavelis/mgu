using UnityEngine;

namespace MGU.Services.Remote.Notifications {
    [CreateAssetMenu(fileName = "RemoteNotificationsSettings", menuName = "MGU/Services/Remote/Notifications/Settings")]
    public class RemoteNotificationsSettings: ScriptableObject {
    }
}