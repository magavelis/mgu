namespace MGU.Services.Remote.Notifications {
    public interface IRemoteNotificationsFactory {
        IRemoteNotifications Create(RemoteNotificationsSettings settings);
    }
}