namespace MGU.Services.Remote.Notifications.Firebase {
    public class FirebaseRemoteNotificationsFactory: IRemoteNotificationsFactory {
        public IRemoteNotifications Create(RemoteNotificationsSettings settings) {
            return new FirebaseRemoteNotifications(settings);
        }
    }
}