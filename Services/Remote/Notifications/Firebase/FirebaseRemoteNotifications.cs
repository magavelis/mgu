﻿using UnityEngine;
#if FIREBASE_REMOTE_MESSAGING
using Firebase.Messaging;
using MasterFirebaseMessaging = Firebase.Messaging.FirebaseMessaging;
#endif


namespace MGU.Services.Remote.Notifications.Firebase {
    public class FirebaseRemoteNotifications: IRemoteNotifications {

        private readonly RemoteNotificationsSettings settings;

        public FirebaseRemoteNotifications(RemoteNotificationsSettings settings) {
            this.settings = settings;
        }
        
        public void LoadUp() {
            #if FIREBASE_REMOTE_MESSAGING
            FirebaseEngine.FirebaseDidLoad += FirebaseDidLoad;
            #endif
        }

        public void CleanUp() {
            #if FIREBASE_REMOTE_MESSAGING
            FirebaseEngine.FirebaseDidLoad -= FirebaseDidLoad;
            MasterFirebaseMessaging.TokenReceived -= OnTokenReceived;
            MasterFirebaseMessaging.MessageReceived -= OnMessageReceived;
            #endif
        }
        
        #if FIREBASE_REMOTE_MESSAGING
        private void FirebaseDidLoad() {
            MasterFirebaseMessaging.TokenReceived += OnTokenReceived;
            MasterFirebaseMessaging.MessageReceived += OnMessageReceived;
        }

        private void OnTokenReceived(object sender, TokenReceivedEventArgs token) {
            Debug.Log("Received Registration Token: " + token.Token);
        }

        private void OnMessageReceived(object sender, MessageReceivedEventArgs e) {
            Debug.Log("Received a new message from: " + e.Message.From);
        }
        #endif
    }
}