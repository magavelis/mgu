using UnityEngine;

namespace MGU.Services.Remote.Notifications {
    public enum RemoteNotificationsProviderType {
        Firebase
    }
    
    public class RemoteNotificationsController: MonoBehaviour {
        [SerializeField] private RemoteNotificationsSettings settings = null;
        [SerializeField] private RemoteNotificationsProviderType providerType = RemoteNotificationsProviderType.Firebase;

        private IRemoteNotifications remoteNotifications = null;

        private void Awake() {
            remoteNotifications = RemoteNotificationsFactory.Create(providerType, settings);
            remoteNotifications.LoadUp();
        }

        private void OnDestroy() {
            remoteNotifications.CleanUp();
        }
    }
}