namespace MGU.Services.Remote.Notifications {
    public interface IRemoteNotifications {
        void LoadUp();
        void CleanUp();
    }
}