using System.Collections.Generic;
using UnityEngine;
using MGU.Services.Remote.Configuration.Firebase;

namespace MGU.Services.Remote.Configuration {
    public static class RemoteConfigFactory {
        private static readonly Dictionary<RemoteConfigProviderType, IRemoteConfigFactory> factories =
                new Dictionary<RemoteConfigProviderType, IRemoteConfigFactory> {
                        { RemoteConfigProviderType.Firebase, new FirebaseRemoteConfigFactory() }
                };

        public static IRemoteConfig Create(RemoteConfigProviderType type, RemoteConfigSettings settings) {
            if (factories.ContainsKey(type) == false) {
                Debug.LogError($"Unexpected Remote Config provider: {type}");
                return null;
            }

            return factories[type].Create(settings);
        }
    }
}