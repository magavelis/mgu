namespace MGU.Services.Remote.Configuration {
    public interface IRemoteConfig {
        void LoadUp();
        void CleanUp();
    }
}