using UnityEngine;

namespace MGU.Services.Remote.Configuration {
    public enum RemoteConfigProviderType {
        Firebase
    }
    
    public class RemoteConfigController: MonoBehaviour {
        [SerializeField] private RemoteConfigSettings settings = null;
        [SerializeField] private RemoteConfigProviderType providerType = RemoteConfigProviderType.Firebase;

        private IRemoteConfig remoteConfig = null;

        private void Awake() {
            remoteConfig = RemoteConfigFactory.Create(providerType, settings);
            remoteConfig.LoadUp();
        }

        private void OnDestroy() {
            remoteConfig.CleanUp();
        }
    }
}