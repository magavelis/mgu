namespace MGU.Services.Remote.Configuration.Firebase {
    public class FirebaseRemoteConfigFactory: IRemoteConfigFactory {
        public IRemoteConfig Create(RemoteConfigSettings settings) {
            return new FirebaseRemoteConfiguration(settings);
        }
    }
}