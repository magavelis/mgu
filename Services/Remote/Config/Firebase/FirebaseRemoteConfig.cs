﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#if FIREBASE_REMOTE_CONFIG
using Firebase.RemoteConfig;
#endif
namespace MGU.Services.Remote.Configuration.Firebase {
    public class FirebaseRemoteConfiguration: IRemoteConfig {
        private readonly RemoteConfigSettings settings = null;

        public FirebaseRemoteConfiguration(RemoteConfigSettings settings) {
            this.settings = settings;
        }

        public void LoadUp() {
            #if FIREBASE_REMOTE_CONFIG
            FirebaseEngine.FirebaseDidLoad += FirebaseDidLoad;
            #endif
        }

        public void CleanUp() {
            #if FIREBASE_REMOTE_CONFIG
            FirebaseEngine.FirebaseDidLoad -= FirebaseDidLoad;
            #endif
        }
        
        #if FIREBASE_REMOTE_CONFIG
        private void FirebaseDidLoad() {
            SetUpRemoteConfigDefaults(settings.RemoteConfigDefaults());
            FetchRemoteConfigData();
        }

        private void SetUpRemoteConfigDefaults(Dictionary<string, object> defaults) {
            FirebaseRemoteConfig.SetDefaults(defaults);
        }

        private void FetchRemoteConfigData() {
            Task task = FirebaseRemoteConfig.FetchAsync(TimeSpan.Zero);
            task.ContinueWith(FetchRemoteConfigDataDidComplete);
        }

        private void FetchRemoteConfigDataDidComplete(Task fetchTask) {
            if (fetchTask.IsCanceled) {
                Debug.Log("Fetch canceled.");
            } else if (fetchTask.IsFaulted) {
                Debug.Log("Fetch encountered an error.");
            } else if (fetchTask.IsCompleted) {
                Debug.Log("Fetch completed successfully!");
            }

            ConfigInfo info = FirebaseRemoteConfig.Info;
            switch (info.LastFetchStatus) {
                case LastFetchStatus.Success:
                    FirebaseRemoteConfig.ActivateFetched();
                    break;

                case LastFetchStatus.Failure:

                    switch (info.LastFetchFailureReason) {
                        case FetchFailureReason.Error:
                        case FetchFailureReason.Invalid:
                            Debug.Log("Fetch failed for unknown reason");
                            break;

                        case FetchFailureReason.Throttled:
                            Debug.Log("Fetch throttled until " + info.ThrottledEndTime);
                            break;

                        default:
                            throw new ArgumentOutOfRangeException();
                    }

                    break;

                case LastFetchStatus.Pending:
                    Debug.Log("Latest fetch call still pending.");
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private ConfigValue GetRemoteConfigValue(string key) {
            return FirebaseRemoteConfig.GetValue(key);
        }
        #endif
    }
}