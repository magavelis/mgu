namespace MGU.Services.Remote.Configuration {
    public interface IRemoteConfigFactory {
        IRemoteConfig Create(RemoteConfigSettings settings);
    }
}