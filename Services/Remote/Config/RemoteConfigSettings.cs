using System.Collections.Generic;
using UnityEngine;

namespace MGU.Services.Remote.Configuration {
    [CreateAssetMenu(fileName = "RemoteConfigSettings", menuName = "MGU/Services/Remote/Config/Settings")]
    public class RemoteConfigSettings: ScriptableObject {
        [SerializeField] private Dictionary<string, object> defaults = new Dictionary<string, object>();

        public Dictionary<string, object> RemoteConfigDefaults() {
            return defaults;
        }
    }
}