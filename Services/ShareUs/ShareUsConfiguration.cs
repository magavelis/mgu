using System.Collections.Generic;
using MGU;

public class ShareUsConfiguration {
    private readonly string title;
    private readonly string description;
    private readonly List<string> hashTagList;
    private readonly string androidUrl;
    private readonly string iOSUrl;

    public ShareUsConfiguration(
            string title,
            string description,
            List<string> hashTagList,
            string androidUrl,
            string iOSUrl) {
        
        this.title = title;
        this.description = description;
        this.hashTagList = hashTagList;
        this.androidUrl = androidUrl;
        this.iOSUrl = iOSUrl;
    }

    public string Title() {
        return title;
    }

    public string Description() {
        string fullDescription = "";
        AppendDescriptionIfNeeded(ref fullDescription);
        AppendHashTagsIfNeeded(ref fullDescription);
        AppendIOSUrlIfNeeded(ref fullDescription);
        AppendAndroidUrlIfNeeded(ref fullDescription);
        return fullDescription;
    }
    

    private void AppendDescriptionIfNeeded(ref string text) {
        if (description.IsNullOrEmpty()) {
            return;
        }

        text += description;
    }

    private void AppendHashTagsIfNeeded(ref string text) {
        if (hashTagList.IsNullOrEmpty()) {
            return;
        }
        
        foreach(string tag in hashTagList) {
            text += $" #{tag}";
        }
    }

    private void AppendIOSUrlIfNeeded(ref string text) {
        if (iOSUrl.IsNullOrEmpty()) {
            return;
        }

        text += "\n";
        text += $"iOS: {iOSUrl}";
    }

    private void AppendAndroidUrlIfNeeded(ref string text) {
        if (androidUrl.IsNullOrEmpty()) {
            return;
        }

        text += "\n";
        text += $"Android: {androidUrl}";
    }
}