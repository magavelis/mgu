﻿using System.Collections.Generic;
using UnityEngine;

namespace MGU.Services.ShareUs {
    public class ShareUsCreator: MonoSingleton<ShareUsCreator> {
        [SerializeField] private string title = null;
        [SerializeField] private string description = null;
        [SerializeField] private string androidUrl = null;
        [SerializeField] private string iOSUrl = null;

        public ShareUsManager ShareUsManager() {
            List<string> hashTagList = new List<string> {
                Application.productName.ToLower()
            };
            ShareUsConfiguration configuration =
                    new ShareUsConfiguration(
                            title,
                            description,
                            hashTagList,
                            androidUrl,
                            iOSUrl);
            return new ShareUsManager(configuration);
        }
    }
}