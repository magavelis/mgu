﻿public class ShareUsManager {
    private readonly ShareUsConfiguration configuration;

    public ShareUsManager(ShareUsConfiguration configuration) {
        this.configuration = configuration;
    }

    public void PresentSharingOptions() {
        #if NATIVE_SHARE
        string subject = configuration.Title();
        string text = configuration.Description();
        new NativeShare().SetSubject(subject).SetText(text).Share();
        #endif
    }
}