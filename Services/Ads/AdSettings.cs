using System.Collections.Generic;
using UnityEngine;

namespace MGU.Services.Ads {
    [CreateAssetMenu(fileName = "AdSettings",  menuName = "MGU/Services/Ads/Settings")]
    public class AdSettings: ScriptableObject {
        [SerializeField] private bool isAdsEnabled = true;
        public bool IsAdsEnabled {
            get => isAdsEnabled;
            set => isAdsEnabled = value;
        }
        
        [SerializeField] private int bannerRetryAttemptsCount = 100;
        public int BannerRetryAttemptsCount => bannerRetryAttemptsCount;

        [SerializeField] private float bannerDelayBetweenRetries = 1;
        public float BannerDelayBetweenRetries => bannerDelayBetweenRetries;

        [SerializeField] private int interstitialRetryAttemptsCount = 5;
        public int InterstitialRetryAttemptsCount => interstitialRetryAttemptsCount;

        [SerializeField] private float interstitialDelayBetweenRetries = 1;
        public float InterstitialDelayBetweenRetries => interstitialDelayBetweenRetries;

        [SerializeField] private int videoRetryAttemptsCount = 5;
        public int VideoRetryAttemptsCount => videoRetryAttemptsCount;

        [SerializeField] private float videoDelayBetweenRetries = 1;
        public float VideoDelayBetweenRetries => videoDelayBetweenRetries;
        
        [SerializeField] private int adShowRequestFilter = 3;
        public int AdShowRequestFilter => adShowRequestFilter;

        [SerializeField] private int dayShowLimit = 20;
        public int DayShowLimit => dayShowLimit;

        [SerializeField] private List<AdLoader> loaderList = new List<AdLoader>();
        public List<AdLoader> LoaderList => loaderList;

        [SerializeField] private Requirements additionalAdShowRequirements = null;
        public Requirements AdditionalAdShowRequirements => additionalAdShowRequirements;
    }
}