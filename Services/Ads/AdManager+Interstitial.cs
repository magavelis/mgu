using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace MGU.Services.Ads {
    public partial class AdManager {
        private List<AdInterstitial> interstitialAdList;
        private RetryHandler interstitialAdRetryHandler;

        public event UnityAction<AdRequestReason> DidShowAdInterstitial;

        public void ShowAdInterstitial(IAdRequestConfiguration configuration) {
            if (!IsAdsEnabled(configuration.Reason, AdType.Interstitial)) {
                return;
            }

            adShowRequestCount++;
            if (!IsAdShowRequestFulfillRequirements(configuration.Reason)) {
                return;
            }

            StopAdInterstitialRetrying();
            if (configuration.ShouldKeepRetrying) {
                interstitialAdRetryHandler = new RetryHandler(
                        launcher,
                        settings.InterstitialRetryAttemptsCount,
                        settings.InterstitialDelayBetweenRetries);
            }

            FindAndShowAdInterstitial(configuration);
        }

        private void FindAndShowAdInterstitial(IAdRequestConfiguration configuration) {
            AdInterstitial interstitial = FindLoadedAdInterstitial();
            if (interstitial == null) {
                Debug.Log("No displayable interstitial ads at the moment");
                interstitialAdRetryHandler.Retry(
                        () => { FindAndShowAdInterstitial(configuration); },
                        () => { });
                return;
            }

            interstitial.Show();
            DidShowAdInterstitial?.Invoke(configuration.Reason);
            adShowCount++;
        }

        private void StopAdInterstitialRetrying() {
            interstitialAdRetryHandler?.Cancel();
            interstitialAdRetryHandler = null;
        }
    }
}