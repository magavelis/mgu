﻿using System;

namespace MGU.Services.Ads {
    public abstract class AdVideo {
        public enum ShowResult {
            Disabled,
            Ignored,
            Failed,
            Skipped,
            Finished
        }
        
        // Method is responsible for loading a interstitial
        public abstract void Load();

        // Method is responsible to inform if interstitial is loaded
        public abstract bool IsLoaded();

        // Method is responsible to inform if video is skippable
        public abstract bool IsSkippable();
        
        // Method is responsible for showing a interstitial
        public abstract void Show(Action<ShowResult> onComplete);

        // Method is responsible for interstitial destroying
        public abstract void Destroy();
    }
}