﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace MGU.Services.Ads {
    
    public enum AdType {
        Banner,
        Interstitial,
        Video
    }
    
    public enum AdRequestReason {
        Manual,
        Automatic
    }
    
    public partial class AdManager {
        private readonly AdSettings settings;
        private readonly MonoBehaviour launcher;
        private readonly Dictionary<AdType, Func<bool>> adProvidersEnabledStatusDict;

        private int adShowRequestCount;
        private int adShowCount;

        public event UnityAction<bool> DidChangeAdsEnabledStatus;

        public AdManager(AdSettings settings, MonoBehaviour launcher) {
            this.settings = settings;
            this.launcher = launcher;
            this.adProvidersEnabledStatusDict = new Dictionary<AdType, Func<bool>> {
                    { AdType.Banner, () => !bannerAdList.IsNullOrEmpty() },
                    { AdType.Interstitial, () => !interstitialAdList.IsNullOrEmpty() },
                    { AdType.Video, () => !videoAdList.IsNullOrEmpty() },
            };

            ConstructAdLoaders();
            PreLoadAds();
        }

        ~AdManager() {
            DidShowAdInterstitial = null;
            DidShowAdVideo = null;
        }

        public bool IsAdsEnabled() {
            return settings.IsAdsEnabled;
        }

        public void SetAdsEnabled(bool isAdsEnabled) {
            settings.IsAdsEnabled = isAdsEnabled;
            DidChangeAdsEnabledStatus?.Invoke(isAdsEnabled);
        }

        private bool IsAdsEnabled(AdRequestReason reason, AdType type) {
            bool hasAnyAdProvider = adProvidersEnabledStatusDict[type]?.Invoke() ?? false;
            return (IsAdsEnabled() || reason == AdRequestReason.Manual) 
                    && hasAnyAdProvider;
        }

        public bool DidReachAdsDayLimit() {
            int limit = settings.DayShowLimit;
            return (limit > 0) && (adShowCount > limit);
        }

        private bool DidFulfillAdSettingsRequirements(AdRequestReason reason) {
            if (reason == AdRequestReason.Manual) {
                return true;
            }

            return adShowRequestCount % settings.AdShowRequestFilter == 0 
                    && settings.AdditionalAdShowRequirements.IsFulfilled();
        }

        public void PreLoadAds() {
            bannerAdList = new List<AdBanner>();
            interstitialAdList = new List<AdInterstitial>();
            videoAdList = new List<AdVideo>();

            settings.LoaderList.ForEach(loader => {
                bannerAdList.TryAdd(loader.CreateAndLoadAdBanner());
                interstitialAdList.Add(loader.CreateAndLoadAdInterstitial());
                videoAdList.AddRange(loader.CreateAndLoadAdVideos());
            });
        }

        private void ConstructAdLoaders() {
            settings.LoaderList.ForEach(loader => loader.Construct());
        }

        private AdBanner FindLoadedAdBanner() {
            return bannerAdList.FirstOrDefault(banner => banner.IsLoaded());
        }

        private AdInterstitial FindLoadedAdInterstitial() {
            return interstitialAdList.FirstOrDefault(interstitial => interstitial.IsLoaded());
        }

        private AdVideo FindLoadedAdVideo(bool shouldAllowSkipping) {
            return videoAdList.FirstOrDefault(video => video.IsLoaded() && video.IsSkippable() == shouldAllowSkipping);
        }

        private bool IsAdShowRequestFulfillRequirements(AdRequestReason reason) {
            return DidFulfillAdSettingsRequirements(reason) 
                   && !DidReachAdsDayLimit();
        }
    }
}