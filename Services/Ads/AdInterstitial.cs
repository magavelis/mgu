﻿namespace MGU.Services.Ads {
    public abstract class AdInterstitial {
        // Method is responsible for loading a interstitial
        public abstract void Load();

        // Method is responsible to inform if interstitial is loaded
        public abstract bool IsLoaded();

        // Method is responsible for showing a interstitial
        public abstract void Show();

        // Method is responsible for interstitial destroying
        public abstract void Destroy();
    }
}