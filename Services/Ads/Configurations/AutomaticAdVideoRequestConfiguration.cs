namespace MGU.Services.Ads {
    public class AutomaticAdVideoRequestConfiguration: IAdVideoRequestConfiguration {
        public AdRequestReason Reason => AdRequestReason.Automatic;
        public bool ShouldKeepRetrying => true;
        public bool ShouldAllowSkipping => true;
    }
}