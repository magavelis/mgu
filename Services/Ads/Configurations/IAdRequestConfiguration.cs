namespace MGU.Services.Ads {
    public interface IAdRequestConfiguration {
        AdRequestReason Reason { get; }
        bool ShouldKeepRetrying { get; }
    }
}