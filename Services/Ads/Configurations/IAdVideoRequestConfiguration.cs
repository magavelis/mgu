namespace MGU.Services.Ads {
    public interface IAdVideoRequestConfiguration: IAdRequestConfiguration {
        bool ShouldAllowSkipping { get; }
    }
}