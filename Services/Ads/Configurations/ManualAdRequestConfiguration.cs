namespace MGU.Services.Ads {
    public class ManualAdRequestConfiguration: IAdRequestConfiguration {
        public AdRequestReason Reason => AdRequestReason.Manual;
        public bool ShouldKeepRetrying => true;
    }
}