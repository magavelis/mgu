namespace MGU.Services.Ads {
    public class AutomaticAdRequestConfiguration: IAdRequestConfiguration {
        public AdRequestReason Reason => AdRequestReason.Automatic;
        public bool ShouldKeepRetrying => true;
    }
}