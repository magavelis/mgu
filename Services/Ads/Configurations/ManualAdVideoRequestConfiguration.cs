namespace MGU.Services.Ads {
    public class ManualAdVideoRequestConfiguration: IAdVideoRequestConfiguration {
        public AdRequestReason Reason => AdRequestReason.Manual;
        public bool ShouldKeepRetrying => true;
        public bool ShouldAllowSkipping => false;
    }
}