﻿#if UNITY_ADS
using UnityEngine.Advertisements;

namespace MGU.Services.Ads {
    public class UnityAdBanner: AdBanner {
        private readonly string id;
        private readonly BannerPosition position;

        public UnityAdBanner(string id, BannerPosition position) {
            this.id = id;
            this.position = position;
        }
        
        public override void Load() {
            Advertisement.Banner.Load(id);
        }

        public override bool IsLoaded() {
            return Advertisement.Banner.isLoaded;
        }

        public override void Show() {
            Advertisement.Banner.Show(id);
            Advertisement.Banner.SetPosition(position);
        }

        public override void Hide() {
            Advertisement.Banner.Hide();
        }

        public override void Destroy() {
            Advertisement.Banner.Hide(true);
        }
    }
}
#endif