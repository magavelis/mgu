#if UNITY_ADS
using System;
using MGU.Services.Ads;
using UnityEngine;
using UnityEngine.Advertisements;

namespace MGU.Services.Ads {
    public class UnityAdVideo: AdVideo, IUnityAdsListener {
        private readonly string id;
        private readonly bool isSkippable;
        private Action<AdShowResult> completionCallback;

        public UnityAdVideo(string id, bool isSkippable) {
            this.id = id;
            this.isSkippable = isSkippable;
        }

        public override void Load() {
            Advertisement.Load(id);
            Advertisement.AddListener(this);
        }

        public override bool IsLoaded() {
            return Advertisement.IsReady(id);
        }

        public override bool IsSkippable() {
            return isSkippable;
        }

        public override void Show(Action<AdShowResult> onComplete) {
            completionCallback = onComplete;
            Advertisement.Show(id);
        }

        public override void Destroy() {
            // Destroy not supported?
            Advertisement.RemoveListener(this);
        }

        #region - IUnityAdsListener
        public void OnUnityAdsReady(string placementId) {
        }

        public void OnUnityAdsDidError(string message) {
            Debug.LogError(message);
            completionCallback?.Invoke(AdShowResult.Failed);
        }

        public void OnUnityAdsDidStart(string placementId) {
        }

        public void OnUnityAdsDidFinish(string placementId, ShowResult showResult) {
            if (id == placementId) {
                completionCallback?.Invoke(showResult.ToAdShowResult());
            }
        }
        #endregion
    }
}

internal static class ShowResultEx {
    public static AdShowResult ToAdShowResult(this ShowResult showResult) {
        switch (showResult) {
            case ShowResult.Failed: return AdShowResult.Failed;
            case ShowResult.Skipped: return AdShowResult.Skipped;
            case ShowResult.Finished: return AdShowResult.Finished;
            default: throw new ArgumentOutOfRangeException(nameof(showResult), showResult, null);
        }
    }
}
#endif