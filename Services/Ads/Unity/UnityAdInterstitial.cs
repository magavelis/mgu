#if UNITY_ADS
using UnityEngine.Advertisements;

namespace MGU.Services.Ads {
    public class UnityAdInterstitial: AdInterstitial {
        private readonly string id;

        public UnityAdInterstitial(string id) {
            this.id = id;
        }

        public override void Load() {
            Advertisement.Load(id);
        }

        public override bool IsLoaded() {
            return Advertisement.IsReady(id);
        }

        public override void Show() {
            Advertisement.Show(id);
        }

        public override void Destroy() {
            // Destroy not supported?
        }
    }
}
#endif