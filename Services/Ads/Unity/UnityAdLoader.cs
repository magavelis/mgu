#if UNITY_ADS
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using MGU;

namespace MGU.Services.Ads {
    [CreateAssetMenu(fileName = "UnityAdLoader",  menuName = "MGU/Services/Ads/UnityLoader")]
    public class UnityAdLoader: AdLoader {
        [Serializable]
        public struct AdVideoSettings {
            public string videoId;
            public bool isSkippable;
        }
        
        [SerializeField] private string iOSGameId = null;
        [SerializeField] private string androidGameId = null;
        [SerializeField] private string bannerId = null;
        [SerializeField] private BannerPosition bannerPosition = BannerPosition.BOTTOM_CENTER;
        [SerializeField] private string interstitialId = null;
        [SerializeField] private List<AdVideoSettings> videoAdSettingList = new List<AdVideoSettings>();
        
        public override void Construct() {
            string gameId = GameId();
            if (gameId.IsNullOrEmpty()) {
                return;
            }
            
            Advertisement.Initialize(gameId, Advertisement.debugMode);
        }

        public override AdBanner CreateAndLoadAdBanner() {
            if (bannerId.IsNullOrEmpty()) {
                return null;
            }
            
            AdBanner banner = new UnityAdBanner(bannerId, bannerPosition);
            banner.Load();

            return banner;
        }

        public override AdInterstitial CreateAndLoadAdInterstitial() {
            if (interstitialId.IsNullOrEmpty()) {
                return null;
            }
            
            AdInterstitial interstitial = new UnityAdInterstitial(interstitialId);
            interstitial.Load();

            return interstitial;
        }

        public override List<AdVideo> CreateAndLoadAdVideos() {
            List<AdVideo> adList = new List<AdVideo>();
            foreach (AdVideoSettings settings in videoAdSettingList) {
                string videoId = settings.videoId;
                if (videoId.IsNullOrEmpty()) {
                    return null;
                }
                
                AdVideo video = new UnityAdVideo(videoId, settings.isSkippable);
                video.Load();
                adList.Add(video);
            }

            return adList;
        }

        private string GameId() {
            #if UNITY_ANDROID
		        return androidGameId;
            #elif UNITY_IPHONE
                return iOSGameId;
            #else
                return null;
            #endif
        }
    }
}
#endif