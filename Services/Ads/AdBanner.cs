﻿using UnityEngine;

namespace MGU.Services.Ads {
    public abstract class AdBanner {
        // Method is responsible for loading a banner
        public abstract void Load();

        // Method is responsible to inform if banner is loaded
        public abstract bool IsLoaded();

        // Method is responsible for showing a banner
        public abstract void Show();

        // Method is responsible for banner hiding
        public abstract void Hide();

        // Method is responsible for banner destroying
        public abstract void Destroy();
    }
}