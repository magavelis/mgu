using System.Collections.Generic;
using UnityEngine;

namespace MGU.Services.Ads {
    public abstract class AdLoader: ScriptableObject {
        public abstract void Construct();
        public abstract AdBanner CreateAndLoadAdBanner();
        public abstract AdInterstitial CreateAndLoadAdInterstitial();
        public abstract List<AdVideo> CreateAndLoadAdVideos();
    }
}