using UnityEngine;

namespace MGU.Services.Ads {
    public class AdCreator: MonoSingleton<AdCreator> {
        public AdSettings settings;

        public AdManager AdManager() {
            if (settings == null) {
                Debug.LogError("Unable to create `AdManager` without `AdSettings`");
                return null;
            }
            
            return new AdManager(settings, this);
        }
    }
}