using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace MGU.Services.Ads {
    public partial class AdManager {
        private List<AdVideo> videoAdList;
        private RetryHandler videoAdRetryHandler;

        public event UnityAction<AdRequestReason> DidShowAdVideo;

        public void ShowAdVideo(
                IAdVideoRequestConfiguration configuration,
                Action<AdVideo.ShowResult> onComplete) {
            if (!IsAdsEnabled(configuration.Reason, AdType.Video)) {
                onComplete.Invoke(AdVideo.ShowResult.Disabled);
                return;
            }

            adShowRequestCount++;
            if (!IsAdShowRequestFulfillRequirements(configuration.Reason)) {
                onComplete.Invoke(AdVideo.ShowResult.Ignored);
                return;
            }

            StopAdVideoRetrying();
            if (configuration.ShouldKeepRetrying) {
                videoAdRetryHandler = new RetryHandler(
                        launcher,
                        settings.VideoRetryAttemptsCount,
                        settings.VideoDelayBetweenRetries);
            }

            FindAndShowAdVideo(configuration, onComplete);
        }

        private void FindAndShowAdVideo(
                IAdVideoRequestConfiguration configuration,
                Action<AdVideo.ShowResult> onComplete) {
            AdVideo video = FindLoadedAdVideo(configuration.ShouldAllowSkipping);
            if (video == null) {
                videoAdRetryHandler?.Retry(
                        () => { ShowAdVideo(configuration, onComplete); },
                        () => { onComplete.Invoke(AdVideo.ShowResult.Failed); });
                Debug.Log("No displayable video ads at the moment.");
                return;
            }

            video.Show(result => {
                DidShowAdVideo?.Invoke(configuration.Reason);
                onComplete.Invoke(result);
            });
            
            adShowCount++;
        }

        private void StopAdVideoRetrying() {
            videoAdRetryHandler?.Cancel();
            videoAdRetryHandler = null;
        }
    }
}