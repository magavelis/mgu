using System.Collections.Generic;
using UnityEngine;

namespace MGU.Services.Ads {
    public partial class AdManager {
        private List<AdBanner> bannerAdList;
        private RetryHandler bannerAdRetryHandler;
        
        public void ShowAdBanner(IAdRequestConfiguration configuration) {
            if (!IsAdsEnabled(configuration.Reason,AdType.Banner)) {
                return;
            }
            
            StopAdBannerRetrying();
            if (configuration.ShouldKeepRetrying) {
                bannerAdRetryHandler = new RetryHandler(
                        launcher,
                        settings.BannerRetryAttemptsCount,
                        settings.BannerDelayBetweenRetries);
            }

            ShowAdBanner();
        }

        private void ShowAdBanner() {
            AdBanner banner = FindLoadedAdBanner();
            if (banner == null) {
                Debug.Log("No displayable banners at the moment");
                return;
            }

            banner.Show();
        }

        private void StopAdBannerRetrying() {
            bannerAdRetryHandler?.Cancel();
            bannerAdRetryHandler = null;
        }
    }
}