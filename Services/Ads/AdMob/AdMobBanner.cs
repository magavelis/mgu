﻿#if GOOGLE_ADS
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

namespace MGU.Services.Ads {
    public class AdMobBanner: AdBanner {
        private BannerView bannerView = null;
        private bool isLoaded = false;

        public AdMobBanner(MonoBehaviour listener) { }

        #region - Overriding Banner

        public override void Load() {

            #if UNITY_EDITOR
            string adUnitId = "unused";
            #elif UNITY_ANDROID
		    string adUnitId = "ca-app-pub-1559800581990388/1152630352";
            #elif UNITY_IPHONE
		    string adUnitId = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
            #else
		    string adUnitId = "unexpected_platform";
            #endif

            // Create a 320x50 banner at the top of the screen.
            bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Top);
            bannerView.OnAdLoaded += OnAdLoaded;
            bannerView.OnAdFailedToLoad += OnAdFailedToLoad;
            bannerView.OnAdOpening += OnAdOpening;
            bannerView.OnAdClosed += OnAdClosed;
            bannerView.OnAdLeavingApplication += OnAdLeavingApplication;

            // Create an empty ad request.
            AdRequest request = AdMobUtils.CreateAdRequest();

            // Load the banner with the request.
            bannerView.LoadAd(request);
        }

        public override bool IsLoaded() {
            if (bannerView == null) {
                return false;
            }

            return isLoaded;
        }

        public override void Show() {
            bannerView.Show();
        }

        public override void Hide() {
            bannerView.Hide();
        }

        public override void Destroy() {
            bannerView.OnAdLoaded -= OnAdLoaded;
            bannerView.OnAdFailedToLoad -= OnAdFailedToLoad;
            bannerView.OnAdOpening -= OnAdOpening;
            bannerView.OnAdClosed -= OnAdClosed;
            bannerView.OnAdLeavingApplication -= OnAdLeavingApplication;

            bannerView.Destroy();
            bannerView = null;
        }

        #endregion
        #region - BannerView Events
        private void OnAdLoaded(object sender, System.EventArgs e) {
            isLoaded = true;
        }

        private void OnAdFailedToLoad(object sender, AdFailedToLoadEventArgs e) {
            isLoaded = false;
        }

        private void OnAdOpening(object sender, System.EventArgs e) { }

        private void OnAdClosed(object sender, System.EventArgs e) { }

        private void OnAdLeavingApplication(object sender, System.EventArgs e) { }
        #endregion
    }
}
#endif