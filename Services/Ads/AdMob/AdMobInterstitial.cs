﻿#if GOOGLE_ADS
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

namespace MGU.Services.Ads {
    public class AdMobInterstitial: AdInterstitial {

        private readonly string id;
        private InterstitialAd interstitial = null;
        
        public AdMobInterstitial(string id) {
            this.id = id;
        }
        
        #region - Overriding Interstitial
        public override void Load() {
            #if UNITY_ANDROID
		    string adUnitId = "ca-app-pub-1559800581990388/2629363556";
            #elif UNITY_IPHONE
            string adUnitId = "INSERT_IOS_INTERSTITIAL_AD_UNIT_ID_HERE";
            #else
		    string adUnitId = "unexpected_platform";
            #endif

            // Create an InterstitialAd object.
            interstitial = new InterstitialAd(adUnitId);

            interstitial.OnAdLoaded += OnAdLoaded;
            interstitial.OnAdFailedToLoad += OnAdFailedToLoad;
            interstitial.OnAdOpening += OnAdOpening;
            interstitial.OnAdClosed += OnAdClosed;
            interstitial.OnAdLeavingApplication += OnAdLeavingApplication;

            // Create an empty ad request.
            AdRequest request = AdMobUtils.CreateAdRequest();

            // Load the interstitial with the request.
            interstitial.LoadAd(request);
        }

        public override bool IsLoaded() {
            if (interstitial == null) {
                return false;
            }

            return interstitial.IsLoaded();
        }

        public override void Show() {
            interstitial.Show();
        }

        public override void Destroy() {
            interstitial.OnAdLoaded -= OnAdLoaded;
            interstitial.OnAdFailedToLoad -= OnAdFailedToLoad;
            interstitial.OnAdOpening -= OnAdOpening;
            interstitial.OnAdClosed -= OnAdClosed;
            interstitial.OnAdLeavingApplication -= OnAdLeavingApplication;

            interstitial.Destroy();
            interstitial = null;
        }

        #endregion
        #region - Interstitial Events
        private void OnAdLoaded(object sender, System.EventArgs e) { }

        private void OnAdFailedToLoad(object sender, AdFailedToLoadEventArgs e) { }

        private void OnAdOpening(object sender, System.EventArgs e) { }

        private void OnAdClosed(object sender, System.EventArgs e) { }

        private void OnAdLeavingApplication(object sender, System.EventArgs e) { }
        #endregion
    }
}
#endif