﻿#if GOOGLE_ADS
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

namespace MGU.Services.Ads {
    public class AdMobVideoAd: AdVideo {
        private RewardBasedVideoAd videoAd = null;

        public override void Load() {
            #if UNITY_EDITOR
            string adUnitId = "unused";
            #elif UNITY_ANDROID
		    string adUnitId = "ca-app-pub-1559800581990388/9795069956";
            #elif UNITY_IPHONE
		    string adUnitId = "INSERT_IOS_REWARD_BASED_VIDEO_AD_UNIT_ID_HERE";
            #else
		    string adUnitId = "unexpected_platform";
            #endif

            // Create a reward based video ad object.
            videoAd = RewardBasedVideoAd.Instance;

            // Registrating ad events
            videoAd.OnAdLoaded += OnAdLoaded;
            videoAd.OnAdFailedToLoad += OnAdFailedToLoad;
            videoAd.OnAdOpening += OnAdOpening;
            videoAd.OnAdStarted += OnAdStarted;
            videoAd.OnAdClosed += OnAdClosed;
            videoAd.OnAdRewarded += OnAdRewarded;
            videoAd.OnAdLeavingApplication += OnAdLeavingApplication;

            // Create an empty ad request.
            AdRequest request = AdMobUtils.CreateAdRequest();

            // Load the reward based video ad with the request and unit id.
            videoAd.LoadAd(request, adUnitId);
        }

        public override bool IsLoaded() {

            if (videoAd == null) {
                return false;
            }

            return videoAd.IsLoaded();
        }

        public override void Show() {
            videoAd.Show();
        }

        public override void Destroy() {
            videoAd.OnAdLoaded -= OnAdLoaded;
            videoAd.OnAdFailedToLoad -= OnAdFailedToLoad;
            videoAd.OnAdOpening -= OnAdOpening;
            videoAd.OnAdStarted -= OnAdStarted;
            videoAd.OnAdClosed -= OnAdClosed;
            videoAd.OnAdRewarded -= OnAdRewarded;
            videoAd.OnAdLeavingApplication -= OnAdLeavingApplication;

            videoAd = null;
        }

        #region - AdVideo Events
        private void OnAdLoaded(object sender, System.EventArgs e) {
            Debug.Log("Sender: " + sender + " e: " + e);
        }

        private void OnAdFailedToLoad(object sender, AdFailedToLoadEventArgs e) {
            Debug.Log("Sender: " + sender + " e: " + e);
        }

        private void OnAdOpening(object sender, System.EventArgs e) {
            Debug.Log("Sender: " + sender + " e: " + e);
        }

        private void OnAdStarted(object sender, System.EventArgs e) {
            Debug.Log("Sender: " + sender + " e: " + e);
        }

        private void OnAdClosed(object sender, System.EventArgs e) {
            Debug.Log("Sender: " + sender + " e: " + e);
        }

        private void OnAdRewarded(object sender, Reward e) {
            Debug.Log("Sender: " + sender + " e: " + e);
        }

        private void OnAdLeavingApplication(object sender, System.EventArgs e) {
            Debug.Log("Sender: " + sender + " e: " + e);
        }
        #endregion
    }
}
#endif