﻿#if GOOGLE_ADS
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

namespace MGU.Services.Ads {
    public class AdMobUtils {
        public static AdRequest CreateAdRequest() {
            AdRequest request = new AdRequest
                            .Builder()
                    .AddTestDevice(AdRequest.TestDeviceSimulator)
                    .AddTestDevice("8FDCD85A6BB1E9935EBE4AB8F5768635")	// Nexus 5 android_id
                    // TODO: Add our devices as test devices, not exactly sure how to get that identifier...
                    .Build();

            return request;
        }

        #if UNITY_ANDROID
	public static string GetAndroidAdMobID() {
		UnityEngine.AndroidJavaClass up = new UnityEngine.AndroidJavaClass("com.unity3d.player.UnityPlayer");
		UnityEngine.AndroidJavaObject currentActivity = up.GetStatic<UnityEngine.AndroidJavaObject>("currentActivity");
		UnityEngine.AndroidJavaObject contentResolver = currentActivity.Call<UnityEngine.AndroidJavaObject>("getContentResolver");
		UnityEngine.AndroidJavaObject secure = new UnityEngine.AndroidJavaObject("android.provider.Settings$Secure");

		string deviceID = secure.CallStatic<string>("getString", contentResolver, "android_id");
		return Md5Sum(deviceID).ToUpper();
	}
        #endif

        public static string Md5Sum(string strToEncrypt) {

            System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
            byte[] bytes = ue.GetBytes(strToEncrypt);

            System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] hashBytes = md5.ComputeHash(bytes);

            string hashString = "";
            int hashBytesCount = hashBytes.Length;
            for (int i = 0; i < hashBytesCount; i++) {
                hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
            }

            return hashString.PadLeft(32, '0');
        }
    }
}
#endif