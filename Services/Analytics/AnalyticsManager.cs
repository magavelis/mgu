﻿using System.Collections.Generic;

namespace MGU.Services.Analytics {
    using UI;
    
    public class AnalyticsManager {
        private readonly AnalyticsSettings settings;

        private List<AnalyticsTracker> TrackerList => settings.AnalyticTrackerList;
        
        protected AnalyticsManager(AnalyticsSettings settings) {
            this.settings = settings;

            LoadUpAnalyticsTrackers();
        }

        ~AnalyticsManager() {
            CleanUpAnalyticsTrackers();
        }

        private void LoadUpAnalyticsTrackers() {
            TrackerList.ForEach(tracker => tracker.LoadUp());
        }

        private void CleanUpAnalyticsTrackers() {
            TrackerList.ForEach(tracker => tracker.CleanUp());
        }

        public void LogEvent(AnalyticsEvent @event) {
            if (!settings.IsAnalyticsEnabled) {
                return;
            }
            
            TrackerList.ForEach(tracker => tracker.LogEvent(@event));
        }
        
        public void LogScreenView(AnalyticsScreenEvent @event) {
            if (!settings.IsAnalyticsEnabled) {
                return;
            }
            
            TrackerList.ForEach(tracker => tracker.LogScreenView(@event));
        }

        public void SetUserProperty(AnalyticsUserProperty property) {
            if (!settings.IsAnalyticsEnabled) {
                return;
            }
            
            TrackerList.ForEach(tracker => tracker.SetUserProperty(property));
        }
    }
    
    public static partial class AnalyticsManagerEx {
        public static void LogScreenView(
                this AnalyticsManager analyticsManager, 
                ViewController viewController, 
                string screenName) {
            AnalyticsScreenEvent @event = new AnalyticsScreenEvent(
                    screenName, 
                    viewController.GetType().Name);
            analyticsManager.LogScreenView(@event);
        }
        
        public static void LogScreenView(
                this AnalyticsManager analyticsManager, 
                View view,
                string screenName) {
            AnalyticsScreenEvent @event = new AnalyticsScreenEvent(
                    screenName, 
                    view.GetType().Name);
            analyticsManager.LogScreenView(@event);
        }
    }
}