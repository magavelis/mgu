namespace MGU.Services.Analytics {
    public class AnalyticsScreenEvent {
        public readonly string ScreenName;
        public readonly string ClassName;
        
        public AnalyticsScreenEvent(string screenName, string className) {
            ScreenName = screenName;
            ClassName = className;
        }
    }
}

