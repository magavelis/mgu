using System.Collections.Generic;

namespace MGU.Services.Analytics {
    public abstract class AnalyticsEvent {
        public abstract string Name();
        public abstract Dictionary<string, object> Parameters();
    }
}