using System.Collections.Generic;

namespace MGU.Services.Analytics {
    public abstract partial class Analytics {
        public class RateUsWasRequested: AnalyticsEvent {
            private readonly string gameVersion;
            
            public RateUsWasRequested(string gameVersion) {
                this.gameVersion = gameVersion;
            }
            
            public override string Name() {
                return "rate_us_was_requested";
            }

            public override Dictionary<string, object> Parameters() {
                return new Dictionary<string, object> { { "game_version", gameVersion } };
            }
        }
    }
}