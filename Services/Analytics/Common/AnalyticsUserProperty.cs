namespace MGU.Services.Analytics {
    public abstract class AnalyticsUserProperty {
        public abstract string Name();
        public abstract string Value();
    }
}