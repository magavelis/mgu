using System.Collections.Generic;
using UnityEngine;

namespace MGU.Services.Analytics {
    public abstract class AnalyticsTracker: ScriptableObject {
        protected bool IsLoaded = false;

        private readonly List<AnalyticsEvent> cachedAnalyticsEventList = new List<AnalyticsEvent>();

        private readonly List<AnalyticsUserProperty>
                cachedAnalyticsUserPropertyList = new List<AnalyticsUserProperty>();

        public virtual void LoadUp() {
            IsLoaded = true;
            SendCachedEvents();
            SetCachedUserProperties();
        }

        public virtual void CleanUp() {
            IsLoaded = false;
            cachedAnalyticsEventList.Clear();
        }

        public virtual void LogEvent(AnalyticsEvent @event) {
            if (!IsLoaded) {
                cachedAnalyticsEventList.Add(@event);
            }
        }

        public virtual void LogScreenView(AnalyticsScreenEvent @event) { }

        public virtual void SetUserProperty(AnalyticsUserProperty property) {
            if (!IsLoaded) {
                cachedAnalyticsUserPropertyList.Add(property);
            }
        }

        private void SendCachedEvents() {
            foreach (AnalyticsEvent @event in cachedAnalyticsEventList) {
                LogEvent(@event);
            }

            cachedAnalyticsEventList.Clear();
        }

        private void SetCachedUserProperties() {
            foreach (AnalyticsUserProperty property in cachedAnalyticsUserPropertyList) {
                SetUserProperty(property);
            }

            cachedAnalyticsUserPropertyList.Clear();
        }
    }
}