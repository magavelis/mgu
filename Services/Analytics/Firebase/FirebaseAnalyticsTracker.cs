using System.Collections.Generic;
using UnityEngine;
using MGU.Services.Firebase;
#if FIREBASE_ANALYTICS
using Firebase.Analytics;
#endif

namespace MGU.Services.Analytics.Firebase {
    [CreateAssetMenu(fileName = "FirebaseAnalytics", menuName = "MGU/Services/Analytics/Trackers/Firebase")]
    public class FirebaseAnalyticsTracker: AnalyticsTracker {
        public override void LoadUp() {
            #if FIREBASE_ANALYTICS
            FirebaseEngine.OnReady(() => {
                FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
                base.LoadUp();
            });
            #endif
        }

        public override void CleanUp() {
            base.CleanUp();
            #if FIREBASE_ANALYTICS
            FirebaseAnalytics.GetAnalyticsInstanceIdAsync().Dispose();
            #endif
        }

        public override void LogEvent(AnalyticsEvent @event) {
            #if FIREBASE_ANALYTICS
            base.LogEvent(@event);
            if (IsLoaded) {
                FirebaseAnalytics.LogEvent(
                        @event.Name(),
                        ParametersFromEvent(@event));
            }
            #endif
        }

        public override void LogScreenView(AnalyticsScreenEvent @event) {
            #if FIREBASE_ANALYTICS
            base.LogScreenView(@event);
            if (IsLoaded) {
                FirebaseAnalytics.SetCurrentScreen(
                        @event.ScreenName,
                        @event.ClassName);
            }
            #endif
        }

        public override void SetUserProperty(AnalyticsUserProperty property) {
            #if FIREBASE_ANALYTICS
            base.SetUserProperty(property);
            if (IsLoaded) {
                FirebaseAnalytics.SetUserProperty(
                        property.Name(), 
                        property.Value());
            }
            #endif
        }

        #if FIREBASE_ANALYTICS
        private Parameter[] ParametersFromEvent(AnalyticsEvent @event) {
            if (@event.Parameters() == null) {
                return new Parameter[] { };
            }
            
            List<Parameter> parameterList = new List<Parameter>();
            foreach (KeyValuePair<string, object> keyValuePair in @event.Parameters()) {
                Parameter param;
                if (keyValuePair.Value is string stringValue) {
                    param = new Parameter(keyValuePair.Key, stringValue);
                } else if (keyValuePair.Value is long longValue) {
                    param = new Parameter(keyValuePair.Key, longValue);
                } else if (keyValuePair.Value is double doubleValue) {
                    param = new Parameter(keyValuePair.Key, doubleValue);
                } else if (keyValuePair.Value is bool boolValue) {
                    param = new Parameter(keyValuePair.Key, boolValue ? 1 : 0);
                } else {
                    param = null;
                }

                if (param != null) {
                    parameterList.Add(param);
                }
            }

            return parameterList.ToArray();
        }
        #endif
    }
}