using System.Collections.Generic;
using UnityEngine;

namespace MGU.Services.Analytics {
    [CreateAssetMenu(fileName = "AnalyticSettings", menuName = "MGU/Services/Analytics/Settings")]
    public class AnalyticsSettings: ScriptableObject {
        [SerializeField] private bool isAnalyticsEnabled = true;
        public bool IsAnalyticsEnabled => isAnalyticsEnabled;

        [SerializeField] private List<AnalyticsTracker> analyticTrackerList = new List<AnalyticsTracker>();
        public List<AnalyticsTracker> AnalyticTrackerList => analyticTrackerList;
    }
}