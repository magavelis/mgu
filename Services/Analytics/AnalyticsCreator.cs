using System;
using UnityEngine;

namespace MGU.Services.Analytics {
    public class AnalyticsCreator: MonoSingleton<AnalyticsCreator> {
        [SerializeField] private AnalyticsSettings settings = null;
        
        public T AnalyticsManager<T>() where T : AnalyticsManager {
            if (settings == null) {
                Debug.LogError("Unable to create `AnalyticsManager` without `AnalyticsSettings`");
                return null;
            }

            return Activator.CreateInstance(typeof(T), new object[] { settings }) as T;
        }
    }
}