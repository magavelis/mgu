﻿using UnityEngine;
using UnityEngine.Events;
#if FIREBASE
using Firebase;
#endif

namespace MGU.Services.Firebase {
    public class FirebaseEngine: MonoBehaviour {
        #if FIREBASE
        private class FirebaseDidLoadEvent: UnityEvent { }
        private static readonly FirebaseDidLoadEvent firebaseDidLoad = new FirebaseDidLoadEvent();
        
        private static DependencyStatus dependencyStatus = DependencyStatus.UnavailableDisabled;
        
        private void Awake() {
            FirebaseApp.CheckAndFixDependenciesAsync()
                    .ContinueWith(task => {
                        dependencyStatus = task.Result;
                        if (dependencyStatus == DependencyStatus.Available) {
                            firebaseDidLoad.Invoke();
                            firebaseDidLoad.RemoveAllListeners();
                            Debug.Log("[Firebase Engine]: Did load successfully!");
                        } else {
                            Debug.LogError("[Firebase Engine]: Could not resolve all Firebase dependencies: " +
                                           dependencyStatus);
                        }
                    });
        }

        private void OnApplicationQuit() {
            firebaseDidLoad.RemoveAllListeners();
            FirebaseApp.DefaultInstance.Dispose();
        }

        public static void OnReady(UnityAction callback) {
            if (dependencyStatus == DependencyStatus.Available) {
                callback.Invoke();
            } else {
                firebaseDidLoad.AddListener(callback);
            }
        }
        #endif
    }
}