using UnityEngine;

namespace MGU.Repository {
	public static class Prefs {
	    #region - String
	    public static void Set(string key , string value) {
			PlayerPrefs.SetString(key, value);
		}

		public static string Get(string key, string defaultValue) {
			return PlayerPrefs.GetString(key, defaultValue);
		}

		#endregion
		#region - Int
		public static void Set(string key, int value) {
			PlayerPrefs.SetInt(key, value);
		}

		public static int Get(string key, int defaultValue) {
			return PlayerPrefs.GetInt(key, defaultValue);
		}

		#endregion
		#region - Bool
		public static void Set(string key, bool value) {
			int intValue = (value) ? 1 : 0;
			Set(key, intValue);
		}

		public static bool Get(string key, bool defaultValue) {
			int intValue = Get(key, (defaultValue) ? 1 : 0);
			return (intValue >= 1);
		}
		#endregion
	}
}
