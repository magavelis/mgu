using System;
using System.Collections.Generic;
using UnityEngine;

namespace MGU.FileSystem {
    public class StorageCreator: MonoSingleton<StorageCreator> {
        [SerializeField] private List<Storage> storageList = new List<Storage>();

        public T StorageManager<T>() where T : StorageManager {
            if (storageList.IsNullOrEmpty()) {
                Debug.LogError("Unable to create `StorageManager` without `storageList`");
                return null;
            }

            return Activator.CreateInstance(typeof(T), storageList) as T;
        }
    }
}