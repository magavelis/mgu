﻿using System;
using System.IO;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;

namespace MGU.FileSystem {
    public static class FileManager {
        public static T Load<T>(string filename) where T: class {
            if (File.Exists(filename) == false) {
                // File is not exist, we should try to load it
                return null;
            }

            try {
                using (Stream stream = File.OpenRead(filename)) {
                    BinaryFormatter formatter = new BinaryFormatter();
                    object deserializedObject = formatter.Deserialize(stream);
                    stream.Close();

                    return deserializedObject as T;
                }
            } catch (Exception e) {
                Debug.LogError(e.Message);
            }

            return default;
        }

        public static void Save<T>(string filename, T data) where T: class {
            if (data == null) {
                return;
            }

            using (Stream stream = File.OpenWrite(filename)) {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, data);
                stream.Close();
            }
        }
    }
}