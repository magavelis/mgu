using System.Collections.Generic;

namespace MGU.FileSystem {
    public class StorageManager {
        private readonly List<Storage> storageList;
        protected StorageManager(List<Storage> storageItems) {
            this.storageList = storageItems;
        
            Load();
        }

        protected List<Storage> StorageItems() {
            return storageList;
        }
    
        public void Save() {
            StorageItems().ForEach(storage => storage.Save());
        }

        private void Load() {
            StorageItems().ForEach(storage => storage.Load());
        }
    }
}