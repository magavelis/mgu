using System.IO;
using UnityEngine;

namespace MGU.FileSystem {
    public abstract class Storage: ScriptableObject {
        private const string extension = "mgu";

        private Storage originalStorage;
        
        // MARK: - Helpers
        private string FilePath() {
            string filename = $"{GetType().Name}.{extension}";
            string path = Path.Combine(Application.persistentDataPath, filename);
            return path;
        }
        
        public void Save(bool isForceSave = false) {
            if (isForceSave == false && IsChanged() == false) {
                return;
            }
            
            string path = FilePath();
            string json = JsonUtility.ToJson(this);
            Debug.Log($"[{GetType()}]: Saving {json} at path: {path}");
            
            FileManager.Save(path, json);
            SetStorageOriginPoint(this);
        }

        public virtual void Load() {
            string path = FilePath();
            string json = FileManager.Load<string>(path);
            if (string.IsNullOrEmpty(json)) {
                Debug.Log($"[{GetType()}]: No storage at path: {path}");
                Save(true);
                return;
            }

            Debug.Log($"[{GetType()}]: Did load {json} from path: {path}");
            JsonUtility.FromJsonOverwrite(json, this);
            SetStorageOriginPoint(this);
        }
        
        private bool IsChanged() {
            // TODO: Make sure if we can check like this
            return originalStorage != this;
        }

        private void SetStorageOriginPoint(Storage storage) {
            originalStorage = storage;
        }
    }
}