﻿using System.Linq;
using System.Collections.Generic;
using Sirenix.Utilities;
using UnityEngine;

namespace MGU.Input {
    public enum GestureRecognizerState {
        // Have not started yet and we are still listening
        Possible,

        // Have started and used at least one finger
        Began,

        // Failed to recognize or we are done recognizing
        FailedOrEnded,

        // Will fire the state changed event and allow a gesture to continue to recognize. useful for continuous gestures
        RecognizedAndStillRecognizing,

        // Successfully recognized and we are not a continuous recognizer
        Recognized
    }

    public abstract class GestureRecognizer {
        public bool IsEnabled = true;

        // 0 by default. If a `Priority` is greater than 0 uses a touch in touchesBegan it will not be passed to any other recognizers.
        // Not quite sure how it works...
        // TODO: Find out...
        public int Priority = 0;

        private GestureRecognizerState state = GestureRecognizerState.Possible;

        protected GestureRecognizerState State {
            get => state;
            set {
                state = value;
                if (state == GestureRecognizerState.Recognized || state == GestureRecognizerState.RecognizedAndStillRecognizing) {
                    NotifyRecognizedEvent();
                }

                if (state == GestureRecognizerState.Recognized || state == GestureRecognizerState.FailedOrEnded) {
                    state = GestureRecognizerState.Possible;
                    TrackedTouchList.Clear();
                }
            }
        }

        protected bool ShouldAlwaysSendTouchMovedEvent = false;
        protected List<Touch> TrackedTouchList = new List<Touch>();

        private bool ShouldStartRecognizing => (
                IsEnabled &&
                state != GestureRecognizerState.FailedOrEnded &&
                state != GestureRecognizerState.Recognized
        );

        internal void Recognize(List<Touch> touchList) {
            if (ShouldStartRecognizing == false) {
                return;
            }

            bool shouldSendTouchBegan = true;
            bool shouldSendTouchMoved = true;
            bool shouldSendTouchEnded = true;

            for (int i = touchList.Count - 1; i >= 0; i--) {
                Touch touch = touchList[i];
                if (touch.ShouldIgnore) {
                    continue;
                }

                switch (touch.Phase) {
                    case TouchPhase.Began: {
                        if (shouldSendTouchBegan) {
                            if (TouchBegan(touchList) && Priority > 0) {
                                int removedTouchesCount = 0;
                                for (int j = touchList.Count - 1; j >= 0; j--) {
                                    if (touchList[j].Phase == TouchPhase.Began) {
                                        touchList.RemoveAt(j);
                                        removedTouchesCount++;
                                    }
                                }

                                if (removedTouchesCount > 0) {
                                    i -= (removedTouchesCount - 1);
                                }
                            }
                        }

                        shouldSendTouchBegan = false;
                        break;
                    }

                    case TouchPhase.Moved:
                    case TouchPhase.Stationary: {
                        List<Touch> applicableTouchList = ApplicableTouchList(touchList);
                        if (shouldSendTouchMoved && applicableTouchList.Contains(touch)) {
                            TouchStayedStationaryOrMoved(applicableTouchList);
                            shouldSendTouchMoved = false;
                        }

                        break;
                    }

                    case TouchPhase.Ended:
                    case TouchPhase.Canceled: {
                        List<Touch> applicableTouchList = ApplicableTouchList(touchList);
                        if (shouldSendTouchEnded && applicableTouchList.Contains(touch)) {
                            TouchEnded(applicableTouchList);
                            shouldSendTouchEnded = false;
                        }

                        break;
                    }

                    default:
                        Debug.LogError($"Unexpected case: {touch.Phase} while recognizing gestures");
                        break;
                }
            }
        }

        private List<Touch> ApplicableTouchList(List<Touch> touchList) {
            List<Touch> list = new List<Touch>();
            touchList.ForEach(touch => {
                if (ShouldAlwaysSendTouchMovedEvent || IsTrackingTouch(touch)) {
                    list.Add(touch);
                }
            });
            return list;
        }

        protected bool IsTrackingTouch(Touch touch) {
            return TrackedTouchList.Contains(touch);
        }

        protected bool IsTrackingTouches(List<Touch> touchList) {
            bool isTracking = false;
            touchList.ForEach(touch => {
                if (IsTrackingTouch(touch)) {
                    isTracking = true;
                }
            });

            return isTracking;
        }

        /// <summary>
        /// Returns the start location of the touches. 
        /// If there are multiple touches this will return the centroid of the location.
        /// </summary>
        public Vector2 StartPosition() {
            Vector2 startPosition = (TrackedTouchList.IsNullOrEmpty())
                    ? Vector2.zero
                    : TrackedTouchList.Select(touch => touch.StartPosition).Average();

            return startPosition;
        }

        /// <summary>
        /// Returns the location of the touches. 
        /// If there are multiple touches this will return the centroid of the location.
        /// </summary>
        public Vector2 CurrentPosition() {
            Vector2 startPosition = (TrackedTouchList.IsNullOrEmpty())
                    ? Vector2.zero
                    : TrackedTouchList.Select(touch => touch.Position).Average();

            return startPosition;
        }

        public Vector2 Delta() {
            Vector2 delta = (TrackedTouchList.IsNullOrEmpty())
                    ? Vector2.zero
                    : TrackedTouchList.Select(touch => touch.DeltaPosition).Average();
            
            return delta;
        }

        public abstract void Clear();

        protected abstract bool TouchBegan(List<Touch> touchList);

        protected abstract void TouchStayedStationaryOrMoved(List<Touch> touchList);

        protected abstract void TouchEnded(List<Touch> touchList);

        protected virtual void NotifyRecognizedEvent() { }
    }
}