﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace MGU.Input {

    public interface ITouchGesture {
        
        bool IsDirty { get; }

        Vector2 StartPosition();
        Vector2 CurrentPosition();

        Vector2 Delta();
    }
    
    public class TouchGestureRecognizer: GestureRecognizer, ITouchGesture {
        public class TouchStartEvent: UnityEvent<ITouchGesture> { }
        public readonly TouchStartEvent OnTouchStart = new TouchStartEvent();

        public class TouchProgressEvent: UnityEvent<ITouchGesture> { }
        public readonly TouchProgressEvent OnTouchProgress = new TouchProgressEvent();

        public class TouchEndEvent: UnityEvent<ITouchGesture> { }
        public readonly TouchEndEvent OnTouchEnd = new TouchEndEvent();

        public bool IsDirty => StartPosition() != CurrentPosition();

        public TouchGestureRecognizer() {
            ShouldAlwaysSendTouchMovedEvent = true;
        }

        public TouchGestureRecognizer(TouchGestureRecognizer recognizer) : this() {
            TrackedTouchList = recognizer.TrackedTouchList;
            State = recognizer.State;
        }
        
        public override void Clear() {
            OnTouchStart.RemoveAllListeners();
            OnTouchProgress.RemoveAllListeners();
            OnTouchEnd.RemoveAllListeners();
        }

        protected override bool TouchBegan(List<Touch> touchList) {
            if (State == GestureRecognizerState.Possible) {
                foreach (Touch touch in touchList) {
                    if (touch.Phase == TouchPhase.Began) {
                        TrackedTouchList.Add(touch);
                        State = GestureRecognizerState.RecognizedAndStillRecognizing;
                        OnTouchStart.Invoke(this);
                        return true;
                    }
                }
            }

            return false;
        }

        protected override void TouchStayedStationaryOrMoved(List<Touch> touchList) {
            foreach (Touch touch in touchList) {
                if (IsTrackingTouch(touch) == false) {
                    continue;
                }
                
                OnTouchProgress.Invoke(this);
            }
        }

        protected override void TouchEnded(List<Touch> touchList) {
            foreach (Touch touch in touchList) {
                if (touch.Phase == TouchPhase.Ended && IsTrackingTouch(touch)) {
                    TrackedTouchList.Remove(touch);
                    State = GestureRecognizerState.FailedOrEnded;
                    OnTouchEnd.Invoke(this);
                }
            }
        }
    }
}