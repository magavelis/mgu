﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using MGU;

namespace MGU.Input {
    public enum SwipeDirection {
        Left = (1 << 0),
        Right = (1 << 1),
        Up = (1 << 2),
        Down = (1 << 3),

        UpLeft = (1 << 4),
        DownLeft = (1 << 5),
        UpRight = (1 << 6),
        DownRight = (1 << 7),

        Horizontal = (Left | Right),
        Vertical = (Up | Down),
        Cardinal = (Horizontal | Vertical),

        DiagonalUp = (UpLeft | UpRight),
        DiagonalDown = (DownLeft | DownRight),
        DiagonalLeft = (UpLeft | DownLeft),
        DiagonalRight = (UpRight | DownRight),
        Diagonal = (DiagonalUp | DiagonalDown),

        RightSide = (Right | DiagonalRight),
        LeftSide = (Left | DiagonalLeft),
        TopSide = (Up | DiagonalUp),
        BottomSide = (Down | DiagonalDown),

        All = (Horizontal | Vertical | Diagonal)
    }

    public interface ISwipeGesture {
        float SwipeVelocity { get; }
        SwipeDirection CompletedSwipeDirection { get; }
        Vector2 StartPoint { get; }
        Vector2 EndPoint { get; }
    }

    public class SwipeGestureRecognizer: GestureRecognizer, ISwipeGesture {
        public class SwipeEvent: UnityEvent<ISwipeGesture> { }
        public readonly SwipeEvent OnSwipe = new SwipeEvent();

        /// <summary>
        /// The minimum number of simultaneous touches (fingers) on the screen to trigger
        /// this swipe recognizer. Default is 1.
        /// </summary>
        private const int minimumNumberOfTouches = 1;

        /// <summary>
        /// The maximum number of simultaneous touches (fingers) on the screen to trigger
        /// this swipe recognizer. Default is 2.
        /// </summary>
        private const int maximumNumberOfTouches = 2;

        /// <summary>
        /// The maximum amount of time for the motion to be considered a swipe.
        /// Setting to 0f will disable the time restriction completely.
        /// </summary>
        private readonly float swipeRestrictionTime = 0.5f;

        /// <summary>
        /// The minimum distance in centimeters that the gesture has to make to be considered
        /// a proper swipe, based on resolution and pixel density. Default is 2cm.
        /// </summary>
        private readonly float minimumDistance = 2f;

        /// <summary>
        /// The maximum distance in centimeters that the gesture has to make to be considered 
        /// a proper swipe.
        /// </summary>
        private readonly float maximumDistance = 10f;

        /// <summary>
        /// The individual points that make up the gesture, recorded every frame from when a
        /// finger is first pressed to the screen until it's lifted. Only tracks the first touch
        /// on the screen, in the case of multiple touches.
        /// </summary>
        private readonly List<Vector2> pointList = new List<Vector2>();

        /// <summary>
        /// The time that the gesture started. Is used to determine if the time limit has been
        /// passed, and whether to ignore further checks.
        /// </summary>
        private float startTime;

        /// <summary>
        /// If true, will trigger on the frame that the criteria for a swipe are first met.
        /// If false, will only trigger on completion of the motion, when the touch is lifted.
        /// </summary>
        public bool TriggerWhenCriteriaMet = true;

        /// <summary>
        /// The velocity of the swipe, in centimeters based on the screen resolution
        /// and pixel density, if available.
        /// </summary>
        public float SwipeVelocity { get; private set; }

        /// <summary>
        /// The direction that the swipe was made in. Possibilities include the four
        /// cardinal directions and the four diagonal directions.
        /// </summary>
        public SwipeDirection CompletedSwipeDirection { get; private set; }

        /// <summary>
        /// The first touch point in the gesture.
        /// </summary>
        public Vector2 StartPoint => pointList.FirstOrDefault();

        /// <summary>
        /// The last touch point in the gesture.
        /// </summary>
        public Vector2 EndPoint => pointList.LastOrDefault();

        public SwipeGestureRecognizer() { }

        public SwipeGestureRecognizer(
                float swipeRestrictionTime,
                float minimumDistanceInCentimeters,
                float maximumDistanceInCentimeters) {

            this.swipeRestrictionTime = swipeRestrictionTime;
            this.minimumDistance = minimumDistanceInCentimeters;
            this.maximumDistance = maximumDistanceInCentimeters;
        }

        public override void Clear() {
            OnSwipe.RemoveAllListeners();
        }

        protected override bool TouchBegan(List<Touch> touchList) {
            if (State == GestureRecognizerState.Possible) {
                // add any touches on screen
                foreach (Touch touch in touchList) {
                    TrackedTouchList.Add(touch);
                }

                // if the number of touches is within our constraints, begin tracking
                if (TrackedTouchList.Count >= minimumNumberOfTouches
                    && TrackedTouchList.Count <= maximumNumberOfTouches) {
                    pointList.Clear();
                    pointList.Add(touchList[0].Position);

                    startTime = Time.time;
                    State = GestureRecognizerState.Began;
                }
            }

            return false;
        }

        protected override void TouchStayedStationaryOrMoved(List<Touch> touchList) {
            // only bother doing anything if we haven't recognized or failed yet
            if (State == GestureRecognizerState.Began) {
                pointList.Add(touchList[0].Position);

                // if we're triggering when the criteria is met, then check for completion every frame
                if (TriggerWhenCriteriaMet && CheckForSwipeCompletion(touchList[0])) {
                    State = GestureRecognizerState.Recognized;
                }
            }
        }

        protected override void TouchEnded(List<Touch> touchList) {
            // if we haven't recognized or failed yet
            if (State == GestureRecognizerState.Began) {
                pointList.Add(touchList[0].Position);

                // last frame, one last check- recognized or fail
                State = CheckForSwipeCompletion(touchList[0])
                        ? GestureRecognizerState.Recognized
                        : GestureRecognizerState.FailedOrEnded;
            }
        }

        protected override void NotifyRecognizedEvent() {
            OnSwipe.Invoke(this);
        }

        private bool CheckForSwipeCompletion(Touch touch) {
            // if we have a time stipulation and we exceeded it stop listening for swipes, fail
            if (swipeRestrictionTime > 0.0f && (Time.time - startTime) > swipeRestrictionTime) {
                return false;
            }

            // if we don't have at least two points to test yet, then fail
            if (pointList.Count <= 1) {
                return false;
            }

            // the ideal distance in pixels from the start to the finish
            float idealDistance = Vector2.Distance(StartPoint, EndPoint);

            // the ideal distance in centimeters, based on the screen pixel density
            float idealDistanceInCentimeters = idealDistance / ScreenUtils.PixelsPerCentimeter;

            // if the distance moved in cm was less than the minimum,
            if (idealDistanceInCentimeters < minimumDistance || idealDistanceInCentimeters > maximumDistance) {
                return false;
            }

            // add up distances between all points sampled during the gesture to get the real distance
            float realDistance = 0f;
            for (int i = 1; i < pointList.Count; i++) {
                realDistance += Vector2.Distance(pointList[i], pointList[i - 1]);
            }

            // if the real distance is 10% greater than the ideal distance, then fail
            // this weeds out really irregular "lines" and curves from being considered swipes
            if (realDistance > idealDistance * 1.1f) {
                return false;
            }

            // the speed in cm/s of the swipe
            SwipeVelocity = idealDistanceInCentimeters / (Time.time - startTime);

            // turn the slope of the ideal swipe line into an angle in degrees
            Vector2 normalizedDirection = (EndPoint - StartPoint).normalized;
            float swipeAngle = Mathf.Atan2(normalizedDirection.y, normalizedDirection.x) * Mathf.Rad2Deg;
            if (swipeAngle < 0) {
                swipeAngle = 360 + swipeAngle;
            }

            swipeAngle = 360 - swipeAngle;

            // depending on the angle of the line, give a logical swipe direction
            if (swipeAngle >= 292.5f && swipeAngle <= 337.5f) {
                CompletedSwipeDirection = SwipeDirection.UpRight;
            } else if (swipeAngle >= 247.5f && swipeAngle <= 292.5f) {
                CompletedSwipeDirection = SwipeDirection.Up;
            } else if (swipeAngle >= 202.5f && swipeAngle <= 247.5f) {
                CompletedSwipeDirection = SwipeDirection.UpLeft;
            } else if (swipeAngle >= 157.5f && swipeAngle <= 202.5f) {
                CompletedSwipeDirection = SwipeDirection.Left;
            } else if (swipeAngle >= 112.5f && swipeAngle <= 157.5f) {
                CompletedSwipeDirection = SwipeDirection.DownLeft;
            } else if (swipeAngle >= 67.5f && swipeAngle <= 112.5f) {
                CompletedSwipeDirection = SwipeDirection.Down;
            } else if (swipeAngle >= 22.5f && swipeAngle <= 67.5f) {
                CompletedSwipeDirection = SwipeDirection.DownRight;
            } else {
                CompletedSwipeDirection = SwipeDirection.Right;
            }

            return true;
        }
    }
}