﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace MGU.Input {

    public interface IRaycastTouchGesture {
        
    }
    
    public class RaycastTouchGestureRecognizer: GestureRecognizer, IRaycastTouchGesture {
        public class TouchStartEvent: UnityEvent<IRaycastTouchGesture> { }
        public readonly TouchStartEvent OnTouchStart = new TouchStartEvent();

        public class TouchEndEvent: UnityEvent<IRaycastTouchGesture> { }
        public readonly TouchEndEvent OnTouchEnd = new TouchEndEvent();

        private readonly Camera camera;
        private readonly Transform target;

        public RaycastTouchGestureRecognizer(Camera camera, Transform target) {
            this.target = target;
            this.camera = camera;
        }

        public override void Clear() {
            OnTouchStart.RemoveAllListeners();
            OnTouchEnd.RemoveAllListeners();
        }

        protected override bool TouchBegan(List<Touch> touchList) {
            if (State == GestureRecognizerState.Possible) {
                foreach (Touch touch in touchList) {
                    if (touch.Phase != TouchPhase.Began) {
                        continue;
                    }

                    Vector2 positionInWorldCoordinatesSystem = camera.ScreenToWorldPoint(touch.StartPosition);
                    RaycastHit2D hit = Physics2D.Raycast(positionInWorldCoordinatesSystem, Vector2.zero, 0f);
                    if (hit.transform == null) {
                        continue;
                    }

                    int targetCode = target.GetHashCode();
                    int hitCode = hit.transform.GetHashCode();
                    if (targetCode != hitCode) {
                        continue;
                    }

                    TrackedTouchList.Add(touch);
                    State = GestureRecognizerState.RecognizedAndStillRecognizing;
                    OnTouchStart.Invoke(this);
                    return true;
                }
            }

            return false;
        }

        protected override void TouchStayedStationaryOrMoved(List<Touch> touchList) { }

        protected override void TouchEnded(List<Touch> touchList) {
            foreach (Touch touch in touchList) {
                if (touch.Phase == TouchPhase.Ended && IsTrackingTouch(touch)) {
                    TrackedTouchList.Remove(touch);
                    State = GestureRecognizerState.FailedOrEnded;
                    OnTouchEnd.Invoke(this);
                }
            }
        }
    }
}