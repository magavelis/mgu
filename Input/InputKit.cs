﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MGU.Input {
    public class InputKit: MonoBehaviour {
        private static InputKit shared;

        public static InputKit Shared {
            get {
                if (shared == null) {
                    shared = FindObjectOfType(typeof(InputKit)) as InputKit;
                    if (shared == null) {
                        GameObject inputKitObject = new GameObject(nameof(InputKit));
                        shared = inputKitObject.AddComponent<InputKit>();
                        DontDestroyOnLoad(inputKitObject);
                    }
                }

                return shared;
            }
        }

        [SerializeField] private bool respectTouchesOnLegacyGui = true;
        [SerializeField] private bool respectTouchesOnUnityGui = true;
        [SerializeField] private int touchesLimit = 2;
        private List<IDetector> detectorList;
        private List<Touch> touchCacheList;

        private readonly Dictionary<int, List<GestureRecognizer>> recognizerDict =
                new Dictionary<int, List<GestureRecognizer>>();

        private List<GestureRecognizer> prioritizedRecognizerList = new List<GestureRecognizer>();

        private void Awake() {
            SetUpInputDetectors();
            SetUpTouchesCache();
        }

        private void Update() {
            DetectTouches();
        }

        public void AddGestureRecognizer(GestureRecognizer recognizer) {
            AddGestureRecognizer(recognizer, this);
        }

        public void AddGestureRecognizer(GestureRecognizer recognizer, object groupOwner) {
            int groupId = groupOwner.GetHashCode();
            List<GestureRecognizer> recognizerList = (recognizerDict.ContainsKey(groupId))
                    ? recognizerDict[groupId]
                    : new List<GestureRecognizer>();

            recognizerList.Add(recognizer);
            recognizerDict[groupId] = recognizerList;
            PrioritizeGestureRecognizers();
        }

        public void RemoveGestureRecognizer(GestureRecognizer recognizer) {
            recognizer.Clear();

            foreach (KeyValuePair<int, List<GestureRecognizer>> item in recognizerDict) {
                List<GestureRecognizer> recognizerList = item.Value;
                if (recognizerList.Contains(recognizer)) {
                    recognizerList.Remove(recognizer);
                }
            }

            PrioritizeGestureRecognizers();
        }

        public void RemoveGestureRecognizers(object groupOwner) {
            int groupId = groupOwner.GetHashCode();
            if (recognizerDict.ContainsKey(groupId)) {
                List<GestureRecognizer> recognizerList = recognizerDict[groupId];
                recognizerList.ForEach(recognizer => { recognizer.Clear(); });

                recognizerDict.Remove(groupId);
            }

            PrioritizeGestureRecognizers();
        }

        public void RemoveAllGestureRecognizers() {
            foreach (KeyValuePair<int, List<GestureRecognizer>> item in recognizerDict) {
                List<GestureRecognizer> recognizerList = item.Value;
                recognizerList.ForEach(recognizer => { recognizer.Clear(); });
            }

            recognizerDict.Clear();
            PrioritizeGestureRecognizers();
        }

        private void PrioritizeGestureRecognizers() {
            List<GestureRecognizer> allRecognizerList = new List<GestureRecognizer>();
            foreach (KeyValuePair<int, List<GestureRecognizer>> item in recognizerDict) {
                allRecognizerList.AddRange(item.Value);
            }

            prioritizedRecognizerList = allRecognizerList
                    .OrderBy(recognizer => recognizer.Priority)
                    .ToList();
        }

        private void SetUpInputDetectors() {
            detectorList = new List<IDetector> {
                    #if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER || UNITY_WEBGL
                    new MouseInputDetector(),
                    #endif

                    new TouchInputDetector(touchesLimit)
            };
        }

        private void SetUpTouchesCache() {
            IDetector detector = detectorList.FirstOrDefault();
            if (detector == null) {
                Debug.LogError("Unable to setup InputKit touches cache without IDetector");
                return;
            }

            touchCacheList = detector.CachedTouches(touchesLimit);
        }

        private void DetectTouches() {
            List<Touch> touchList = new List<Touch>();
            detectorList.ForEach(detector => {
                List<Touch> detectedTouchList = detector.ActiveTouches(touchCacheList);
                touchList.AddRange(detectedTouchList);
            });
            touchList.ForEach(touch => touch.ShouldIgnore = !ShouldDetectTouch(touch));

            if (touchList.Count > 0) {
                prioritizedRecognizerList.ForEach(recognizer => { recognizer.Recognize(touchList); });
            }
        }

        private bool ShouldDetectTouch(Touch touch) {
            if (respectTouchesOnLegacyGui && GUIUtility.hotControl > 0) {
                return false;
            }

            if (respectTouchesOnUnityGui) {
                EventSystem eventSystem = EventSystem.current;
                if (eventSystem &&
                    eventSystem.IsPointerOverGameObject(touch.FingerId) &&
                    eventSystem.currentSelectedGameObject != null) {
                    return false;
                }
            }

            return true;
        }
    }
}