﻿using System.Collections.Generic;

namespace MGU.Input {
    public interface IDetector {
        List<Touch> ActiveTouches(List<Touch> touchCacheList);
        List<Touch> CachedTouches(int touchesLimit);
    }
}