﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace MGU.Input {
    using Input = UnityEngine.Input;

    public class MouseInputDetector: IDetector {
        public List<Touch> ActiveTouches(List<Touch> touchCacheList) {
            List<Touch> touchList = new List<Touch>();
            #if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER || UNITY_WEBGL
            if (Input.GetMouseButtonUp(0) || Input.GetMouseButton(0)) {
                Touch touch = touchCacheList.FirstOrDefault();
                if (touch != null) {
                    TouchPhase phase = TouchPhase.Moved;

                    // Guard against down and up being called in the same frame
                    if (Input.GetMouseButtonDown(0) && Input.GetMouseButtonUp(0)) {
                        phase = TouchPhase.Canceled;
                    }
                    else if (Input.GetMouseButtonUp(0)) {
                        phase = TouchPhase.Ended;
                    }
                    else if (Input.GetMouseButtonDown(0)) {
                        phase = TouchPhase.Began;
                    }

                    Vector2 position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                    touchList.Add(touch.Construct(position, phase));
                }
            }
            #endif
            return touchList;
        }
        
        public List<Touch> CachedTouches(int touchesLimit) {
            List<Touch> touchCacheList = new List<Touch>(touchesLimit);
            for (int i = -1; i < touchesLimit - 1; i++) {
                touchCacheList.Add(new Touch(i));
            }

            return touchCacheList;
        }
    }
}