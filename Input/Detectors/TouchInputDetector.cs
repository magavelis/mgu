﻿using System.Collections.Generic;
using UnityEngine;

namespace MGU.Input {
    using Input = UnityEngine.Input;
    using UnityTouch = UnityEngine.Touch;
    
    public class TouchInputDetector: IDetector {
        private readonly int touchesLimit;
        private bool shouldLookUpForgottenTouches;

        public TouchInputDetector(int touchesLimit) {
            this.touchesLimit = touchesLimit;
        }

        public List<Touch> ActiveTouches(List<Touch> touchCacheList) {
            List<Touch> touchList = new List<Touch>();
            if (Input.touchCount > 0) {
                shouldLookUpForgottenTouches = true;
                int maxTouchIndexToExamine = Mathf.Min(Input.touches.Length, touchesLimit);
                for (int i = 0; i < maxTouchIndexToExamine; i++) {
                    UnityTouch touch = Input.GetTouch(i);
                    if (touch.fingerId < touchesLimit) {
                        touchList.Add(touchCacheList[touch.fingerId].Construct(touch));
                    }
                }
            }
            else {
                if (shouldLookUpForgottenTouches) {
                    List<Touch> forgottenTouchList = ForgottenTouches(touchCacheList);
                    touchList.AddRange(forgottenTouchList);
                    shouldLookUpForgottenTouches = false;
                }
            }

            return touchList;
        }

        public List<Touch> CachedTouches(int touchesLimit) {
            List<Touch> touchCacheList = new List<Touch>(touchesLimit);
            for (int i = 0; i < touchesLimit; i++) {
                touchCacheList.Add(new Touch(i));
            }

            return touchCacheList;
        }
        
        // Unity sometimes misses the Ended phase of touches so this method will look out for that
        private List<Touch> ForgottenTouches(List<Touch> touchCacheList) {
            List<Touch> touchList = new List<Touch>();
            touchCacheList.ForEach(touch => {
                if (touch.Phase != TouchPhase.Ended) {
                    touch.Phase = TouchPhase.Ended;
                    touchList.Add(touch);
                }
            });
            return touchList;
        }
    }
}