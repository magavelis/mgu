﻿using System;
using UnityEngine;

namespace MGU.Input {
    using UnityTouch = UnityEngine.Touch;

    public class Touch {
        public readonly int FingerId;

        public Vector2 Position;
        public Vector2 StartPosition;
        public Vector2 DeltaPosition;
        public float DeltaTime;
        public int TapCount;

        public bool ShouldIgnore = false;

        public TouchPhase Phase = TouchPhase.Ended;

        public Touch(int fingerId) {
            FingerId = fingerId;
        }

        public Touch Construct(UnityTouch touch) {
            Position = touch.position;
            DeltaPosition = touch.deltaPosition;
            DeltaTime = touch.deltaTime;
            TapCount = touch.tapCount;

            if (touch.phase == TouchPhase.Began) {
                StartPosition = touch.position;
            }

            Phase = (touch.phase == TouchPhase.Canceled) ? TouchPhase.Ended : touch.phase;
            return this;
        }

        #if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER || UNITY_WEBGL
        private Vector2? lastPosition;
        private double lastClickTime;
        private const double multipleClickInterval = 0.2;
        private const float tolerance = 0.001f;

        public Touch Construct(Vector2 currentPosition, TouchPhase touchPhase) {
            DeltaPosition = currentPosition - (lastPosition ?? currentPosition);
            switch (touchPhase) {
            case TouchPhase.Began:
                Phase = TouchPhase.Began;
                TapCount = (Time.time < lastClickTime + multipleClickInterval) ? TapCount + 1 : 1;
                lastPosition = currentPosition;
                StartPosition = currentPosition;
                lastClickTime = Time.time;
                break;

            case TouchPhase.Stationary:
            case TouchPhase.Moved:
                Phase = (Math.Abs(DeltaPosition.sqrMagnitude) < tolerance) ? TouchPhase.Stationary : TouchPhase.Moved;
                lastPosition = currentPosition;
                break;

            case TouchPhase.Canceled:
            case TouchPhase.Ended:
                Phase = TouchPhase.Ended;
                lastPosition = null;
                break;

            default:
                throw new ArgumentOutOfRangeException(nameof(touchPhase), touchPhase, null);
            }

            Position = currentPosition;
            return this;
        }
        #endif
    }
}