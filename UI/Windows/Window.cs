﻿using System;
using UnityEngine;
using MGU.Common;

namespace MGU.UI {
    public class Window: MonoBehaviour {
        [SerializeField] private Canvas canvas;

        public static Window Main;
        [SerializeField] private string sortingLayerName = K.SortingLayer.Ui;
        public string SortingLayerName => canvas.sortingLayerName;

        private ViewController rootViewController;

        public ViewController RootViewController {
            get => rootViewController;
            set {
                if (rootViewController.IsNotNull()) {
                    Destroy(rootViewController.gameObject);
                }

                rootViewController = value;
            }
        }

        private RectTransform rect;

        public RectTransform Rect {
            get {
                if (rect.IsNull()) {
                    rect = transform as RectTransform;
                }

                return rect;
            }
        }

        public Camera Camera => canvas.worldCamera;

        private void Awake() {
            Main = this;

            if (canvas == null) {
                canvas = GetComponent<Canvas>();
            }
            
            UpdateCameraIfNeeded();
            DontDestroyOnLoad(gameObject);
        }

        private void Update() {
            UpdateCameraIfNeeded();
        }

        public void MakeVisible() {
            MakeVisible(RootViewController);
        }

        public void MakeVisible(ViewController viewController) {
            AddViewController(viewController);
            viewController.MakeVisible();
        }

        // MARK: - Helpers
        private void UpdateCameraIfNeeded() {
            if (Camera.IsNull()) {
                canvas.worldCamera = FindObjectOfType<Camera>();
                canvas.renderMode = RenderMode.ScreenSpaceCamera;
                canvas.sortingLayerName = sortingLayerName;
            }
        }
        
        private void AddViewController(ViewController viewController) {
            viewController.transform.SetParent(transform, false);
            viewController.View.Layout();
        }
    }
}