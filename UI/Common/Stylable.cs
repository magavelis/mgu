﻿using UnityEngine;

namespace MGU.UI {
    public interface IStylable { }
    public abstract class Stylable: ScriptableObject, IStylable { }
}
