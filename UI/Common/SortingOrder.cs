﻿using System;

namespace MGU.UI {
    [Flags]
    public enum RenderOrder {
        AboveToolBar = 1,
        AboveNavigationBar = 2,
        AboveEverything = 4,

        AboveBars = AboveToolBar | AboveNavigationBar
    }

    public class SortingOrder {
        // MARK: - Declarations
        private readonly RenderOrder renderOrder;

        public static int PresentedViewController => 500;

        public int FaderViewController {
            get {
                int sortingOrder = 0;
                switch (renderOrder) {
                case RenderOrder.AboveEverything:
                    sortingOrder = 1000;
                    break;
                case RenderOrder.AboveBars:
                case RenderOrder.AboveNavigationBar:
                case RenderOrder.AboveToolBar:
                    sortingOrder = 100;
                    break;
                }

                return sortingOrder;
            }
        }

        public int NavigationBar {
            get {
                int sortingOrder = 0;
                switch (renderOrder) {
                case RenderOrder.AboveEverything:
                case RenderOrder.AboveBars:
                case RenderOrder.AboveNavigationBar:
                    sortingOrder = 99;
                    break;
                case RenderOrder.AboveToolBar:
                    sortingOrder = 199;
                    break;
                }

                return sortingOrder;
            }
        }

        public int ToolBar {
            get {
                int sortingOrder = 0;
                switch (renderOrder) {
                case RenderOrder.AboveEverything:
                case RenderOrder.AboveBars:
                case RenderOrder.AboveToolBar:
                    sortingOrder = 99;
                    break;
                case RenderOrder.AboveNavigationBar:
                    sortingOrder = 199;
                    break;
                }

                return sortingOrder;
            }
        }

        // MARK: - Methods
        public SortingOrder(RenderOrder renderOrder) {
            this.renderOrder = renderOrder;
        }
    }
}