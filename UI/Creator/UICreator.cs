﻿using UnityEngine;

namespace MGU.UI {
    [CreateAssetMenu(fileName = "UICreator", menuName = "UI/Creator")]
    public partial class UICreator: ScriptableObjectSingleton<UICreator> {
        private ViewController CreateViewController(ViewController prefab) {
            ViewController newViewController = Instantiate(prefab);
            newViewController.name = prefab.name;

            return newViewController;
        }
    }
}