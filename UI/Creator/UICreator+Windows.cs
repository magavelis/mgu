using UnityEngine;

namespace MGU.UI {
    public partial class UICreator {
        [SerializeField] private Window mainWindow = null;

        public Window CreateWindow() {
            Window window = Instantiate(mainWindow);
            window.name = mainWindow.name;

            return window;
        }
    }
}