using UnityEngine;

namespace MGU.UI {
    public partial class UICreator {
        [SerializeField] private ButtonView buttonView = null;
        [SerializeField] private ToggleView toggleView = null;

        public ButtonView CreateButtonView(string buttonName = null) {
            ButtonView button = Instantiate(buttonView);
            button.name = buttonName ?? buttonView.name;

            return button;
        }

        public ToggleView CreateToggleView(ToggleType type, string toggleName = null) {
            ToggleView toggle = Instantiate(toggleView);
            toggle.name = toggleName ?? toggleView.name;
            toggle.SetToggleType(type);

            return toggle;
        }
    }
}