using System;
using UnityEngine;

namespace MGU.UI {
    public partial class UICreator {
        [SerializeField] private DrawerController drawerController = null;
        [SerializeField] private NavigationController navigationController = null;
        [SerializeField] private ViewController viewController = null;
        [SerializeField] private AlertViewController alertViewController = null;
        [SerializeField] private FaderViewController faderViewController = null;

        [SerializeField] private NavigationBar[] navigationBars = null;
        [SerializeField] private ToolBar[] toolBars = null;
        
        public DrawerController CreateDrawerController(
                Window window,
                ViewController centerViewController,
                ViewController leftViewController = null,
                ViewController rightViewController = null) {
            DrawerController drawer = CreateViewController(drawerController) as DrawerController;
            if (drawer == null) {
                Debug.LogError("Unable to create `DrawerController`");
                return null;
            }

            drawer.SetWindow(Window.Main);
            drawer.SetViewControllers(centerViewController, leftViewController, rightViewController);
            return drawer;
        }

        public NavigationController CreateNavigationController(
                ViewController rootViewController,
                Type navigationBarType = null,
                Type toolBarType = null) {
            NavigationController navigator = CreateViewController(navigationController) as NavigationController;
            if (navigator == null) {
                Debug.LogError("Unable to create `NavigationController`");
                return null;
            }

            if (navigationBarType != null) {
                NavigationBar navigationBar = CreateNavigationBar(navigationBarType);
                if (navigationBar != null) {
                    navigator.SetNavigationBar(navigationBar);
                }
            }

            if (toolBarType != null) {
                ToolBar toolBar = CreateToolBar(toolBarType);
                if (toolBar != null) {
                    navigator.SetToolBar(toolBar);
                }
            }

            navigator.RootViewController = rootViewController;
            return navigator;
        }

        public ViewController CreateViewController() {
            ViewController newViewController = CreateViewController(viewController);
            if (newViewController == null) {
                Debug.LogError("Unable to create `ViewController`");
                return null;
            }

            return newViewController;
        }

        public AlertViewController CreateAlertViewController() {
            AlertViewController alert = CreateViewController(alertViewController) as AlertViewController;
            if (alert == null) {
                Debug.LogError("Unable to create `AlertViewController`");
                return null;
            }
            
            return alert;
        }

        public FaderViewController CreateFaderViewController() {
            FaderViewController fader = CreateViewController(faderViewController) as FaderViewController;
            if (fader == null) {
                Debug.LogError("Unable to create `FaderViewController`");
                return null;
            }

            // TODO: Use construct method?
            fader.SortingOrder = new SortingOrder(RenderOrder.AboveEverything).FaderViewController;
            fader.Hide(false, null);
            return fader;
        }

        private NavigationBar CreateNavigationBar(Type type) {
            View prefab = navigationBars.FirstOrDefaultOfType(type);
            if (prefab == null) {
                Debug.LogError("Couldn't find a navigation bar prefab of type: " + type);
                return null;
            }

            NavigationBar navigationBar = Instantiate(prefab) as NavigationBar;
            if (navigationBar == null) {
                Debug.LogError("Unable to create a navigation bar of type: " + type);
                return null;
            }

            navigationBar.name = prefab.name;
            return navigationBar;
        }

        private ToolBar CreateToolBar(Type type) {
            View prefab = toolBars.FirstOrDefaultOfType(type);
            if (prefab == null) {
                Debug.LogError("Couldn't find a tab bar prefab of type: " + type);
                return null;
            }

            ToolBar toolBar = Instantiate(prefab) as ToolBar;
            if (toolBar == null) {
                Debug.LogError("Unable to create a tab bar of type: " + type);
                return null;
            }

            toolBar.name = prefab.name;
            return toolBar;
        }
    }
}