﻿using System;
using UnityEngine;

namespace MGU.UI {
    [Serializable]
    public class LabelViewStyle: IStylable {
        public Font Font;
        public FontStyle FontStyle;
        public int FontSize;
        public Color TextColor;
    }
}