using UnityEngine;
using TMPro;

namespace MGU.UI {
    [ExecuteInEditMode]
    public class LabelProView: View {
        [SerializeField] private TextMeshProUGUI textComponent = null;

        public string Text {
            get => textComponent.text;
            set => textComponent.text = value;
        }

        public Color TextColor {
            get => textComponent.color;
            set => textComponent.color = value;
        }

        public TMP_FontAsset Font {
            get => textComponent.font;
            set => textComponent.font = value;
        }

        public FontStyles FontStyle {
            get => textComponent.fontStyle;
            set => textComponent.fontStyle = value;
        }

        public float FontSize {
            get => textComponent.fontSize;
            set => textComponent.fontSize = value;
        }

        // MARK: - Methods
        protected override void Awake() {
            base.Awake();
            this.AssignComponent(ref textComponent, true);
        }

        // MARK: - Overriding ASDView
        public override void ApplyStyle(IStylable newStyle) {
            if (newStyle is LabelProViewStyle labelViewStyle) {
                Font = labelViewStyle.Font;
                FontStyle = labelViewStyle.FontStyle;
                FontSize = labelViewStyle.FontSize;
                TextColor = labelViewStyle.TextColor;
            }
        }
    }
}