﻿using System;
using TMPro;
using UnityEngine;

namespace MGU.UI {
    [Serializable]
    public class LabelProViewStyle: IStylable {
        public TMP_FontAsset Font;
        public FontStyles FontStyle;
        public float FontSize = 36;
        public Color TextColor = Color.white;
    }
}