﻿using UnityEngine;
using UnityEngine.UI;

namespace MGU.UI {

    [ExecuteInEditMode]
    public class LabelView: View {
        // Declarations
        [SerializeField] private Text textComponent = null;

        public string Text {
            get => textComponent.text;
            set => textComponent.text = value;
        }

        public Color TextColor {
            get => textComponent.color;
            set => textComponent.color = value;
        }

        public Font Font {
            get => textComponent.font;
            set => textComponent.font = value;
        }

        public FontStyle FontStyle {
            get => textComponent.fontStyle;
            set => textComponent.fontStyle = value;
        }

        public int FontSize {
            get => textComponent.fontSize;
            set => textComponent.fontSize = value;
        }

        // MARK: - Methods
        protected override void Awake() {
            base.Awake();
            this.AssignComponent(ref textComponent, true);
        }

        // MARK: - Overriding ASDView
        public override void ApplyStyle(IStylable newStyle) {
            if (newStyle is LabelViewStyle labelViewStyle) {
                Font = labelViewStyle.Font;
                FontStyle = labelViewStyle.FontStyle;
                FontSize = labelViewStyle.FontSize;
                TextColor = labelViewStyle.TextColor;
            }
        }
    }
}