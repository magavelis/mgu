﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace MGU.UI {
    public static class ViewEx {
        // MARK: - Moving
        public static IEnumerator MoveVertically(
                this View view,
                float value,
                float duration,
                Func<float, float> ease,
                UnityAction onComplete) {
            Vector2 moveValue = new Vector2(view.Rect.X, value);
            return Move(view, moveValue, duration, ease, onComplete);
        }

        public static IEnumerator MoveHorizontally(
                this View view,
                float value,
                float duration,
                Func<float, float> ease,
                UnityAction onComplete) {
            Vector2 moveValue = new Vector2(value, view.Rect.Y);
            return Move(view, moveValue, duration, ease, onComplete);
        }

        public static IEnumerator Move(
                this View view,
                Vector2 toValue,
                float duration,
                Func<float, float> ease,
                UnityAction onComplete) {
            Vector2 startingPosition = view.Rect.Origin;
            return CoroutineEx.Execute(
                    duration,
                    (float time) => {
                        view.Rect.Origin = Vector2Utils.Lerp(startingPosition, toValue, ease(time));
                    },
                    () => {
                        view.Rect.Origin = toValue;
                        onComplete?.Invoke();
                    });
        }

        // MARK: - Sizing/Scaling
        public static IEnumerator Resize(
                this View view,
                Vector2 toValue,
                float delay,
                float duration,
                Func<float, float> ease,
                UnityAction onComplete) {
            Vector2 startingSize = view.Rect.Size;
            return CoroutineEx.Execute(
                    delay,
                    duration,
                    (float time) => {
                        view.Rect.Size = Vector2Utils.Lerp(startingSize, toValue, ease(time));
                    },
                    () => {
                        view.Rect.Size = toValue;
                        onComplete?.Invoke();
                    });
        }
        
        public static IEnumerator Scale(
                this View view,
                Vector3 toValue,
                float delay,
                float duration,
                Func<float, float> ease,
                UnityAction onComplete) {
            Vector3 startingScale = view.Rect.LocalScale;
            return CoroutineEx.Execute(
                    delay,
                    duration,
                    (float time) => {
                        view.Rect.LocalScale = Vector3Utils.Lerp(startingScale, toValue, ease(time));
                    },
                    () => {
                        view.Rect.LocalScale = toValue;
                        onComplete?.Invoke();
                    });
        }

        // MARK: - Fading
        public static IEnumerator FadeOut(
                this View view,
                float duration,
                Func<float, float> ease,
                UnityAction onComplete) {
            return FadeOut(view, 0.0f, duration, ease, onComplete);
        }

        public static IEnumerator FadeOut(
                this View view,
                float delay,
                float duration,
                Func<float, float> ease,
                UnityAction onComplete) {
            return FadeTo(view, 0, delay,duration, ease, onComplete);
        }

        public static IEnumerator FadeIn(
                this View view,
                float duration,
                Func<float, float> ease,
                UnityAction onComplete) {
            return FadeIn(view, 0.0f, duration, ease, onComplete);
        }
        
        public static IEnumerator FadeIn(
                this View view,
                float delay,
                float duration,
                Func<float, float> ease,
                UnityAction onComplete) {
            return FadeTo(view, 1, delay,duration, ease, onComplete);
        }

        public static IEnumerator FadeTo(
                this View view,
                float toValue,
                float delay,
                float duration,
                Func<float, float> ease,
                UnityAction onComplete) {
            float startingAlpha = view.Alpha;
            return CoroutineEx.Execute(
                    delay,
                    duration,
                    (float time) => {
                        view.Alpha = Mathf.Lerp(startingAlpha, toValue, ease(time));
                    },
                    () => {
                        view.Alpha = toValue;
                        onComplete?.Invoke();
                    });
        }

        // MARK: - Changing Color
        public static IEnumerator ChangeColor(
                this View view,
                Color newColor,
                float duration,
                Func<float, float> ease,
                UnityAction onComplete) {
            Color startingColor = view.BackgroundColor;
            return CoroutineEx.Execute(
                    duration,
                    (float time) => {
                        view.BackgroundColor = Color.Lerp(startingColor, newColor, ease(time));
                    },
                    () => {
                        view.BackgroundColor = newColor;
                        onComplete?.Invoke();
                    });
        }
    }
}