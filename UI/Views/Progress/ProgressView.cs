﻿using UnityEngine;
using UnityEngine.UI;

namespace MGU.UI {
    public class ProgressView : View {
        // Declarations
        [SerializeField] private Image progressImage = null;
        [SerializeField] private View indicatorBackgroundView = null;
        [SerializeField] private LabelView leftLabelView = null;
        [SerializeField] private LabelView rightLabelView = null;
        
        public float Progress {
            get => progressImage.fillAmount;
            set => progressImage.fillAmount = value;
        }

        public string LeftText {
            get => leftLabelView.Text;
            set => leftLabelView.Text = value;
        }

        public string RightText {
            get => rightLabelView.Text;
            set => rightLabelView.Text = value;
        }
        
        // MARK: - Overriding ASDView
        public override void ApplyStyle(IStylable newStyle) {
            if (newStyle is ProgressViewStyle progressViewStyle) {
                base.ApplyStyle(progressViewStyle.BackgroundView);
                indicatorBackgroundView.ApplyStyle(progressViewStyle.IndicatorView);
            }
        }
    }
}