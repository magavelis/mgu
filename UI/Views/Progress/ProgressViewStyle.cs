﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MGU.UI {
    [CreateAssetMenu(fileName = "ProgressViewStyle", menuName = "UI/Style/ProgressView")]
    public class ProgressViewStyle: Stylable {
        public ViewStyle BackgroundView;
        public ViewStyle IndicatorView;
    }
}