﻿using System;
using UnityEngine;

namespace MGU.UI {
    [Serializable]
    public class ViewStyle: IStylable {
        public Sprite Sprite;
        public Color Color;
        public EdgeInsets EdgeInsets;
    }
}