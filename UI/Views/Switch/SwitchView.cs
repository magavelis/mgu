﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace MGU.UI {
    public class SwitchView: View {
        // MARK: - Constants
        private const float switchAnimationDuration = 0.1f;

        // MARK: - Declarations
        [SerializeField] private View thumbView = null;
        [SerializeField] private View borderView = null;
        [SerializeField] private View backgroundView = null;
        [SerializeField] private Button switchButton = null;

        public class SwitchChangeEvent: UnityEvent<bool> { }
        public SwitchChangeEvent OnSwitchValueChange = new SwitchChangeEvent();

        public bool IsOn;

        private float SwitchOnThumbPositionX => Rect.Width - thumbView.Rect.Width;
        private float SwitchOffThumbPositionX => 0.0f;

        private Color SwitchOnBackgroundColor =>
                ((SwitchViewStyle) style)?.ActiveBackgroundViewStyle.Color
                ?? backgroundView.BackgroundColor;

        private Color SwitchOffBackgroundColor =>
                ((SwitchViewStyle) style)?.InactiveBackgroundViewStyle.Color
                ?? backgroundView.BackgroundColor;

        private DurationCalculator durationCalculator;
        private CoroutineQueue queue;

        // MARK: - Methods
        protected override void Awake() {
            base.Awake();
            switchButton.onClick.AddListener(SwitchButtonClick);
            durationCalculator = new DurationCalculator(
                    switchAnimationDuration,
                    SwitchOffThumbPositionX,
                    SwitchOnThumbPositionX);
            queue = CoroutineQueue.Create(gameObject);
        }

        protected override void OnDestroy() {
            base.OnDestroy();
            OnSwitchValueChange.RemoveAllListeners();
        }

        // MARK: - Overriding View
        public override void ApplyStyle(IStylable newStyle) {
            if (newStyle is SwitchViewStyle switchViewStyle) {
                ViewStyle backgroundViewStyle = IsOn
                        ? switchViewStyle.ActiveBackgroundViewStyle
                        : switchViewStyle.InactiveBackgroundViewStyle;
                backgroundView.ApplyStyle(backgroundViewStyle);
                thumbView.ApplyStyle(switchViewStyle.ThumbViewStyle);
                borderView.ApplyStyle(switchViewStyle.BorderViewStyle);
            }
        }

        // MARK: - UI Actions
        public void SwitchButtonClick() {
            SetSwitchOn(!IsOn, true, true);
        }

        public void SetSwitchOn(bool value, bool shouldSendCallback, bool animated) {
            IsOn = value;
            UpdateSwitch(animated);
            if (shouldSendCallback) {
                OnSwitchValueChange.Invoke(value);
            }
        }

        // MARK: - Helpers
        private void UpdateSwitch(bool animated) {
            if (animated) {
                queue.SkipAllCoroutines();
                queue.AddCoroutine(SwitchAnimationCoroutine(IsOn));
            } else {
                thumbView.Rect.X = SwitchThumbPositionX(IsOn);
                backgroundView.BackgroundColor = SwitchBackgroundColor(IsOn);
            }
        }
        

        private MyCoroutine SwitchAnimationCoroutine(bool isOn) {
            float duration = SwitchAnimationDuration(isOn);
            return new MyCoroutine(new[] {
                    thumbView.MoveHorizontally(
                            SwitchThumbPositionX(isOn),
                            duration,
                            Easing.Exponential.EaseInOut,
                            null),
                    backgroundView.ChangeColor(
                            SwitchBackgroundColor(isOn),
                            duration,
                            Easing.Exponential.EaseInOut,
                            null)
            });
        }
        
        private float SwitchThumbPositionX(bool isOn) {
            return isOn ? SwitchOnThumbPositionX : SwitchOffThumbPositionX;
        }
        
        private Color SwitchBackgroundColor(bool isOn) {
            return isOn ? SwitchOnBackgroundColor : SwitchOffBackgroundColor;
        }
        
        private float SwitchAnimationDuration(bool isOn) {
            return isOn 
                    ? durationCalculator.EnterDuration(thumbView.Rect.X) 
                    : durationCalculator.ExitDuration(thumbView.Rect.X);
        }
    }
}