﻿using System;
using UnityEngine;

namespace MGU.UI {
    [CreateAssetMenu(fileName = "SwitchViewStyle", menuName = "UI/Style/SwitchView")]
    [Serializable] public class SwitchViewStyle: Stylable {
        public ViewStyle ThumbViewStyle;
        public ViewStyle BorderViewStyle;
        public ViewStyle ActiveBackgroundViewStyle;
        public ViewStyle InactiveBackgroundViewStyle;
    }
}