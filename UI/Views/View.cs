﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace MGU.UI {
    public class View: MonoBehaviour {
        private Transform owner;

        public Transform Transform {
            get => owner ? owner : transform;
            set => owner = value;
        }

        private ViewTransform rect;

        public ViewTransform Rect {
            get {
                if (rect == null && transform is RectTransform t) {
                    rect = new ViewTransform(t);
                }

                return rect;
            }
        }

        public float Alpha {
            get => CanvasGroup.alpha;
            set => CanvasGroup.alpha = value;
        }

        public Color BackgroundColor {
            get => ImageView.color;
            set => ImageView.color = value;
        }

        public Sprite Image {
            get => ImageView.sprite;
            set {
                ImageView.sprite = value;
                if (value != null && BackgroundColor.a < 0.01f) {
                    BackgroundColor = Color.white;
                }
            }
        }

        public bool IsHidden => !gameObject.activeSelf;

        public bool IsInteractable {
            get => CanvasGroup.interactable;
            set {
                CanvasGroup.interactable = value;
                CanvasGroup.blocksRaycasts = value;
            }
        }
        
        [SerializeField] protected Stylable style;
        protected CanvasGroup CanvasGroup;
        protected Image ImageView;

        public event UnityAction OnViewLayout;
        
        // MARK - Lifecycle
        protected virtual void Awake() {
            this.AssignComponent(ref CanvasGroup);
            this.AssignComponent(ref ImageView, true);
        }

        protected virtual void Start() {
            ApplyStyle(style);
            StartCoroutine(IE_DelayedLayout());
        }

        protected virtual void OnDestroy() { }

        // MARK: - Overridable
        public virtual void Layout() {
            OnViewLayout?.Invoke();
        }

        public virtual void ApplyStyle(IStylable newStyle) {
            if (newStyle is ViewStyle viewStyle) {
                Image = viewStyle.Sprite;
                BackgroundColor = viewStyle.Color;
                Rect.EdgeInsets = viewStyle.EdgeInsets;
            }
        }

        public virtual void SetHidden(bool isHidden) {
            gameObject.SetActive(!isHidden);
        }

        // MARK: - Subview Helpers
        public void AddSubview(View view) {
            view.Transform.SetParent(transform, false);
            view.Layout();
        }

        // MARK: - Helpers
        private IEnumerator IE_DelayedLayout() {
            yield return null;
            Layout();
        }
    }
}