﻿using UnityEngine;

namespace MGU.UI {
    public enum AnchorPosition {
        BottomLeft,
        BottomCenter,
        BottomRight,
        MiddleLeft,
        MiddleCenter,
        MiddleRight,
        TopLeft,
        TopCenter,
        TopRight,
        StretchBottom,
        StretchMiddle,
        StretchTop,
        StretchLeft,
        StretchCenter,
        StretchRight,
        Stretch,
        Custom
    }

    public class ViewTransformCache {
        public readonly float X;
        public readonly float Y;
        public readonly float Width;
        public readonly float Height;
        public readonly Vector3 LocalPosition;
        public readonly Vector3 LocalScale;

        public ViewTransformCache(ViewTransform transform) {
            X = transform.X;
            Y = transform.Y;
            Width = transform.Width;
            Height = transform.Height;
            LocalPosition = transform.LocalPosition;
            LocalScale = transform.LocalScale;
        }

        public Vector2 Origin() {
            return new Vector2(X, Y);
        }
        
        public Vector2 Size() {
            return new Vector2(Width, Height);
        }

        public string Description() {
            return $"X: {X}, "
                    + $"Y: {Y}, "
                    + $"Width: {Width}, "
                    + $"Height: {Height}, "
                    + $"LocalPosition: {LocalPosition}, "
                    + $"LocalScale: {LocalScale}";
        }
    }
    
    public class ViewTransform {
        public readonly RectTransform Transform;

        private Vector2 offsetMax;
        private Vector2 offsetMin;

        public Vector3 LocalPosition => Transform.localPosition;

        public Vector3 LocalScale {
            get => Transform.localScale;
            set => Transform.localScale = value;
        }

        public float X {
            get => Transform.anchoredPosition.x;
            set => Origin = new Vector2(value, Origin.y);
        }

        public float Y {
            get => Transform.anchoredPosition.y;
            set => Origin = new Vector2(Origin.x, value);
        }

        public Vector2 Origin {
            get => Transform.anchoredPosition;
            set => Transform.anchoredPosition = value;
        }

        public float Width {
            get => Transform.rect.width;
            set => Size = new Vector2(value, Size.y);
        }

        public float Height {
            get => Transform.rect.height;
            set => Size = new Vector2(Size.x, value);
        }

        public Vector2 Size {
            get => Transform.sizeDelta;
            set {
                Transform.sizeDelta = value;
                UpdateOffsets();
                UpdateViewInsets();
            }
        }

        public Vector2 Pivot {
            get => Transform.pivot;
            set => Transform.pivot = value;
        }

        private EdgeInsets edgeInsets;

        public EdgeInsets EdgeInsets {
            get => edgeInsets ?? EdgeInsets.Zero;
            set {
                edgeInsets = value;
                UpdateViewInsets();
            }
        }

        private ViewTransformCache cache;
        
        public ViewTransform(RectTransform transform) {
            Transform = transform;
            offsetMax = transform.offsetMax;
            offsetMin = transform.offsetMin;
        }

        public void SetAnchor(AnchorPosition position, bool adjustPivot = true) {
            Vector2 newPivot = Pivot;
            switch (position) {
            case AnchorPosition.BottomLeft:
                Transform.anchorMin = Transform.anchorMax = newPivot = Vector2.zero;
                break;
            case AnchorPosition.BottomCenter:
                Transform.anchorMin = Transform.anchorMax = newPivot = new Vector2(0.5f, 0.0f);
                break;
            case AnchorPosition.BottomRight:
                Transform.anchorMin = Transform.anchorMax = newPivot = Vector2.right;
                break;
            case AnchorPosition.MiddleLeft:
                Transform.anchorMin = Transform.anchorMax = newPivot = new Vector2(0.0f, 0.5f);
                break;
            case AnchorPosition.MiddleCenter:
                Transform.anchorMin = Transform.anchorMax = newPivot = new Vector2(0.5f, 0.5f);
                break;
            case AnchorPosition.MiddleRight:
                Transform.anchorMin = Transform.anchorMax = newPivot = new Vector2(1.0f, 0.5f);
                break;
            case AnchorPosition.TopLeft:
                Transform.anchorMin = Transform.anchorMax = newPivot = Vector2.up;
                break;
            case AnchorPosition.TopCenter:
                Transform.anchorMin = Transform.anchorMax = newPivot = new Vector2(0.5f, 1.0f);
                break;
            case AnchorPosition.TopRight:
                Transform.anchorMin = Transform.anchorMax = newPivot = Vector2.one;
                break;
            case AnchorPosition.StretchBottom:
                Transform.anchorMin = Vector2.zero;
                Transform.anchorMax = Vector2.right;
                newPivot = new Vector2(0.5f, 0.0f);
                break;
            case AnchorPosition.StretchMiddle:
                Transform.anchorMin = new Vector2(0.0f, 0.5f);
                Transform.anchorMax = new Vector2(1.0f, 0.5f);
                newPivot = new Vector2(0.5f, 0.5f);
                break;
            case AnchorPosition.StretchTop:
                Transform.anchorMin = Vector2.up;
                Transform.anchorMax = Vector2.one;
                newPivot = new Vector2(0.5f, 1.0f);
                break;
            case AnchorPosition.StretchLeft:
                Transform.anchorMin = Vector2.zero;
                Transform.anchorMax = Vector2.up;
                newPivot = new Vector2(0.0f, 0.5f);
                break;
            case AnchorPosition.StretchCenter:
                Transform.anchorMin = new Vector2(0.5f, 0.0f);
                Transform.anchorMax = new Vector2(0.5f, 1.0f);
                newPivot = new Vector2(0.5f, 0.5f);
                break;
            case AnchorPosition.StretchRight:
                Transform.anchorMin = Vector2.right;
                Transform.anchorMax = Vector2.one;
                newPivot = new Vector2(1.0f, 0.5f);
                break;
            case AnchorPosition.Stretch:
                Transform.anchorMin = Vector2.zero;
                Transform.anchorMax = Vector2.one;
                newPivot = new Vector2(0.5f, 0.5f);
                break;
            case AnchorPosition.Custom: break;
            }

            if (adjustPivot) {
                Pivot = newPivot;
            }
        }

        public void MakeCache() {
            cache = new ViewTransformCache(this);
        }

        public ViewTransformCache Cache() {
            if (cache == null) {
                Debug.LogError("Cache must be create via `MakeCache` method before accessing");
            }
            return cache;
        }

        // MARK: - Helpers
        private void UpdateOffsets() {
            Vector2 max = Transform.offsetMax;
            offsetMax = new Vector2(max.x + EdgeInsets.Trailing, max.y + EdgeInsets.Top);
            Vector2 min = Transform.offsetMin;
            offsetMin = new Vector2(min.x - EdgeInsets.Leading, min.y - EdgeInsets.Bottom);
        }

        private void UpdateViewInsets() {
            Transform.offsetMax = new Vector2(offsetMax.x - EdgeInsets.Trailing, offsetMax.y - EdgeInsets.Top);
            Transform.offsetMin = new Vector2(offsetMin.x + EdgeInsets.Leading, offsetMin.y + EdgeInsets.Bottom);
        }
    }
}