﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace MGU.UI {
    public enum ToggleType {
        Normal,
        GraphicSwap
    }

    [ExecuteInEditMode]
    [RequireComponent(typeof(Toggle))]
    public class ToggleView: View {
        [SerializeField] private Toggle toggle = null;
        [SerializeField] private Image toggleBackgroundImageView = null;
        [SerializeField] private Image toggleForegroundImageView = null;
        [SerializeField] private Text titleLabel = null;

        public ToggleType toggleType;

        public UnityAction<bool> onValueChangedAction;

        public string Title {
            get => titleLabel.text;
            set => titleLabel.text = value;
        }

        public bool IsEnabled {
            get => toggle.interactable;
            set => toggle.interactable = value;
        }

        public bool IsOn {
            get => toggle.isOn;
            set => SetOn(value, true);
        }

        protected override void Awake() {
            base.Awake();
            this.AssignComponent(ref toggle);
            this.AssignComponent(ref titleLabel, true);
            toggle.onValueChanged.AddListener(OnToggleValueChange);

            UpdateView();
        }

        protected override void OnDestroy() {
            base.OnDestroy();
            toggle.onValueChanged.RemoveListener(OnToggleValueChange);
            onValueChangedAction = null;
        }

        // MARK: - View
        public override void SetHidden(bool isHidden) {
            base.SetHidden(isHidden);
            IsEnabled = !isHidden;
        }
        
        // MARK: - Toggle Helpers
        public void SetOn(bool isOn, bool shouldSendCallback) {
            if (shouldSendCallback) {
                toggle.isOn = isOn;
            } else {
                toggle.SetIsOnWithoutNotify(isOn);
            }
        }

        public void SetToggleType(ToggleType type) {
            toggleType = type;
            UpdateView();
        }

        // MARK: - Helpers
        private void UpdateView() {
            switch (toggleType) {
            case ToggleType.Normal:
                UpdateToggleAsNormal();
                break;

            case ToggleType.GraphicSwap:
                UpdateToggleAsGraphicSwap();
                break;
            }
        }

        private void UpdateToggleAsNormal() {
            toggle.graphic = toggleForegroundImageView;
            toggleForegroundImageView.gameObject.SetActive(true);
        }

        private void UpdateToggleAsGraphicSwap() {
            bool isOn = IsOn;
            toggle.SetIsOnWithoutNotify(false);
            toggle.graphic = null;
            toggle.SetIsOnWithoutNotify(isOn);
            toggleForegroundImageView.gameObject.SetActive(false);
        }

        private void OnToggleValueChange(bool value) {
            toggleBackgroundImageView.overrideSprite = value ? toggleForegroundImageView.sprite : null;
            onValueChangedAction?.Invoke(value);
        }
    }
}