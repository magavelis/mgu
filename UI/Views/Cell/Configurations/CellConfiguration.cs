﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace MGU.UI {
    public interface ICellConfiguration {
        EdgeInsets BackgroundEdgeInsets { get; set; }

        string Title { get; set; }
        string Subtitle { get; set; }

        bool ShowSeparatorView { get; set; }

        Func<CellConfiguration> OnSelect { get; set; }
    }

    public interface ICellValueConfiguration {
        object Value { get; set; }
    }

    public class CellConfiguration: ICellConfiguration {
        public EdgeInsets BackgroundEdgeInsets { get; set; }
        public Sprite Icon { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }

        public bool ShowSeparatorView { get; set; }

        public Func<CellConfiguration> OnSelect { get; set; }

        public CellConfiguration() { }
    }

    public class ValueCellConfiguration: CellConfiguration, ICellValueConfiguration {
        public object Value { get; set; }
    }
}