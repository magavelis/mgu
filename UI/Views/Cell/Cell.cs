﻿using UnityEngine;
using UnityEngine.UI;

namespace MGU.UI {
    public class Cell: View {
        // MARK: - Declarations
        [SerializeField] protected View BackgroundView;
        [SerializeField] protected LabelProView TitleLabelView;
        [SerializeField] protected LabelProView SubtitleLabelView;
        [SerializeField] protected View IconView;
        [SerializeField] protected ButtonView ButtonView;
        [SerializeField] protected View SeparatorView;

        protected CellConfiguration Configuration;

        // MARK: - Methods
        protected override void Awake() {
            base.Awake();
            
            ButtonView.OnClick.AddListener(OnSelect);
        }

        // MARK: - Overriding View
        public override void ApplyStyle(IStylable newStyle) {
            base.ApplyStyle(newStyle);

            if (newStyle is CellStyle cellStyle) {
                BackgroundView.ApplyStyle(cellStyle.BackgroundViewStyle);
                TitleLabelView.ApplyStyle(cellStyle.TitleLabelStyle);
                SubtitleLabelView.ApplyStyle(cellStyle.SubtitleLabelStyle);
                SeparatorView.ApplyStyle(cellStyle.SeparatorViewStyle);
            }
        }

        // MARK: - Public
        public virtual void Construct(CellConfiguration configuration, bool animated = false) {
            Reset();

            Configuration = configuration;
            BackgroundView.Rect.EdgeInsets = configuration.BackgroundEdgeInsets;

            IconView.Image = configuration.Icon;
            TitleLabelView.Text = configuration.Title;

            if (SubtitleLabelView != null) {
                SubtitleLabelView.Text = configuration.Subtitle;
            }

            SeparatorView.SetHidden(!configuration.ShowSeparatorView);
        }

        // MARK: - Helpers
        protected virtual void Reset() {
            Configuration = null;
        }

        protected virtual void OnSelect() {
            Configuration = Configuration.OnSelect?.Invoke();
            Construct(Configuration, true);
        }
    }
}