﻿using UnityEngine;

namespace MGU.UI {
    [CreateAssetMenu(fileName = "NavigationCellStyle", menuName = "UI/Style/Cell/NavigationCell")]
    public class NavigationCellStyle: Stylable {
        public CellStyle CellStyle;
        public ViewStyle ValueIconViewStyle;
        public LabelProViewStyle ValueLabelStyle;
    }
}