﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MGU.UI {
    public class NavigationCell: Cell {
        // MARK: - Declarations
        [SerializeField] protected LabelProView ValueLabelView = null;
        [SerializeField] protected View ValueIconView = null;

        // MARK: - Overriding View
        public override void ApplyStyle(IStylable newStyle) {
            if (newStyle is NavigationCellStyle cellStyle) {
                base.ApplyStyle(cellStyle.CellStyle);
                ValueLabelView.ApplyStyle(cellStyle.ValueLabelStyle);
                ValueIconView.ApplyStyle(cellStyle.ValueIconViewStyle);
            }
        }
        
        // MARK: - Overriding Cell
        public override void Construct(CellConfiguration configuration, bool animated = false) {
            base.Construct(configuration, animated);

            if (configuration is ICellValueConfiguration valueConfiguration) {
                UpdateValueLabelView(valueConfiguration, animated);
            }
        }

        // MARK: - Helpers
        private void UpdateValueLabelView(ICellValueConfiguration configuration, bool animated) {
            ValueLabelView.Text = (string) configuration.Value;
        }
    }
}
