﻿using UnityEngine;

namespace MGU.UI {
    [CreateAssetMenu(fileName = "CellStyle", menuName = "UI/Style/Cell/Cell")]
    public class CellStyle: Stylable {
        public ViewStyle BackgroundViewStyle;
        public LabelProViewStyle TitleLabelStyle;
        public LabelProViewStyle SubtitleLabelStyle;
        public ViewStyle SeparatorViewStyle;
    }
}