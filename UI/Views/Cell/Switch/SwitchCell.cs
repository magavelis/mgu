﻿using UnityEngine;

namespace MGU.UI {
    public class SwitchCell: Cell {
        // MARK: - Declarations
        [SerializeField] protected SwitchView SwitchView;

        protected override void Awake() {
            base.Awake();

            SwitchView.OnSwitchValueChange.AddListener(OnSwitchValueChange);
        }
        
        // MARK: - UI Actions
        private void OnSwitchValueChange(bool value) {
            base.OnSelect();
        }

        // MARK: - Overriding View
        public override void ApplyStyle(IStylable newStyle) {
            if (newStyle is SwitchCellStyle cellStyle) {
                base.ApplyStyle(cellStyle.CellStyle);
                SwitchView.ApplyStyle(cellStyle.SwitchViewStyle);
            }
        }
        
        // MARK: - Overriding Cell
        public override void Construct(CellConfiguration configuration, bool animated = false) {
            base.Construct(configuration, animated);
            UpdateSwitchView(configuration, animated);
        }

        protected override void OnSelect() {
            SwitchView.SwitchButtonClick();
        }

        // MARK: - Helpers
        private void UpdateSwitchView(CellConfiguration configuration, bool animated) {
            if (configuration is ICellValueConfiguration valueConfiguration) {
                SwitchView.SetSwitchOn(
                        (bool) valueConfiguration.Value, 
                        false, 
                        animated);
            }
        }
    }
}