﻿using UnityEngine;

namespace MGU.UI {
    [CreateAssetMenu(fileName = "SwitchCellStyle", menuName = "UI/Style/Cell/SwitchCell")]
    public class SwitchCellStyle: Stylable {
        public CellStyle CellStyle;
        public SwitchViewStyle SwitchViewStyle;
    }
}