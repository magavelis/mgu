﻿using UnityEngine;

namespace MGU.UI {
    [CreateAssetMenu(fileName = "ButtonViewStyle", menuName = "UI/Style/ButtonView")]
    public class ButtonViewStyle: Stylable {
        public ViewStyle BackgroundViewStyle;
        public LabelProViewStyle TitleLabelViewStyle;
    }
}
