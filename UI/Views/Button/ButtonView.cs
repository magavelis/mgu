﻿using UnityEngine;
using UnityEngine.UI;

namespace MGU.UI {
    [ExecuteInEditMode]
    public class ButtonView: View {
        // Declarations
        public Button Button;
        public LabelProView TitleLabelView;

        public Button.ButtonClickedEvent OnClick => Button.onClick;

        public string Title {
            get => TitleLabelView.Text;
            set => TitleLabelView.Text = value;
        }

        public bool IsEnabled {
            get => Button.interactable;
            set => Button.interactable = value;
        }

        // MARK: - Methods
        protected override void Awake() {
            base.Awake();
            this.AssignComponent(ref Button);
            this.AssignComponent(ref TitleLabelView, true);
        }

        protected override void OnDestroy() {
            base.OnDestroy();
            Button.onClick.RemoveAllListeners();
        }

        // MARK: - Overriding ASDView
        public override void ApplyStyle(IStylable newStyle) {
            if (newStyle is ButtonViewStyle buttonViewStyle) {
                base.ApplyStyle(buttonViewStyle.BackgroundViewStyle);
                TitleLabelView.ApplyStyle(buttonViewStyle.TitleLabelViewStyle);
            }
        }
    }
}