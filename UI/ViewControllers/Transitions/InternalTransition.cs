﻿using System.Collections;
using UnityEngine.Events;

namespace MGU.UI {
    public class InternalTransition: AnimatedTransition {
        private readonly IEnumerator [] pushActions;
        private readonly IEnumerator [] popActions;

        protected InternalTransition(ViewController viewController): base(viewController) { }
        
        public InternalTransition(
            ViewController viewController,
            IEnumerator [] pushActions,
            IEnumerator [] popActions): base(viewController) {
            this.pushActions = pushActions;
            this.popActions = popActions;
        }

        public override void DoPush(UnityAction onComplete) {
            if (DependencyController.IsNotNull() && DependencyController.GetTransition() is InternalTransition transition) {
                MyCoroutine popCoroutine = new MyCoroutine(transition.popActions);
                Queue.AddCoroutine(popCoroutine);
            }

            MyCoroutine pushCoroutine = new MyCoroutine(pushActions);
            pushCoroutine.OnCoroutineComplete.AddListenerSafe(onComplete);
            Queue.AddCoroutine(pushCoroutine);
        }

        public override void DoPop(UnityAction onComplete) {
            MyCoroutine popCoroutine = new MyCoroutine(popActions);
            Queue.AddCoroutine(popCoroutine);

            if (DependencyController.IsNotNull() && DependencyController.GetTransition() is InternalTransition transition) {
                MyCoroutine pushCoroutine = new MyCoroutine(transition.pushActions);
                pushCoroutine.OnCoroutineComplete.AddListenerSafe(onComplete);
                Queue.AddCoroutine(pushCoroutine);
            } else {
                popCoroutine.OnCoroutineComplete.AddListenerSafe(onComplete);
            }
        }
    }
}