﻿using UnityEngine;
using UnityEngine.Events;

namespace MGU.UI {
    public class AlertTransition: AnimatedTransition {
        // MARK: - Declarations
        private readonly View backgroundView;
        private readonly View contentView;

        protected AlertTransition(ViewController viewController): base(viewController) { }

        public AlertTransition(
                ViewController viewController,
                View backgroundView,
                View contentView): base(viewController) {
            this.backgroundView = backgroundView;
            this.contentView = contentView;
        }

        public override void DoPush(UnityAction onComplete) {
            backgroundView.Alpha = 0.0f;
            contentView.Alpha = 0.0f;
            contentView.Rect.LocalScale = Vector3.zero;

            MyCoroutine coroutine = new MyCoroutine(new[] {
                    backgroundView.FadeIn(0.5f, Easing.Linear, null),
                    contentView.FadeIn(0.2f, Easing.Linear, null),
                    contentView.Scale(
                            Vector3.one,
                            0.0f,
                            0.3f,
                            new Easing.Back().EaseOut,
                            null)
            });

            Queue.OnQueueComplete.AddListenerSafe(onComplete);
            Queue.AddCoroutine(coroutine);
        }

        public override void DoPop(UnityAction onComplete) {
            MyCoroutine coroutine = new MyCoroutine(new[] {
                    backgroundView.FadeOut(0.1f, Easing.Linear, null),
                    backgroundView.FadeOut(0.1f, Easing.Linear, null),
                    contentView.Scale(Vector3.zero,
                            0.0f,
                            0.1f,
                            Easing.Exponential.EaseOut,
                            null)
            });

            Queue.OnQueueComplete.AddListenerSafe(onComplete);
            Queue.AddCoroutine(coroutine);
        }
    }
}