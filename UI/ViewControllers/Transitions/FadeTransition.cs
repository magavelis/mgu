﻿using UnityEngine.Events;

namespace MGU.UI {
    public class FadeTransition: AnimatedTransition {
        public FadeTransition(ViewController viewController): base(viewController) { }

        public override void DoPush(UnityAction onComplete) {
            OwnerController.View.Alpha = 0;

            MyCoroutine coroutine = new MyCoroutine(
                    OwnerController.View.FadeIn(
                            TransitionDuration(),
                            Easing.Linear,
                            null)
            );

            if (DependencyController.IsNotNull()) {
                coroutine.AddCoroutine(
                        DependencyController.View.FadeOut(
                                TransitionDuration(),
                                Easing.Linear,
                                null)
                );
            }

            coroutine.OnCoroutineComplete.AddListenerSafe(onComplete);
            Queue.AddCoroutine(coroutine);
        }

        public override void DoPop(UnityAction onComplete) {
            MyCoroutine coroutine = new MyCoroutine(
                    OwnerController.View.FadeOut(
                            TransitionDuration(),
                            Easing.Linear,
                            null)
            );

            if (DependencyController.IsNotNull()) {
                coroutine.AddCoroutine(
                        DependencyController.View.FadeIn(
                                TransitionDuration(),
                                Easing.Linear,
                                null)
                );
            }

            coroutine.OnCoroutineComplete.AddListenerSafe(onComplete);
            Queue.AddCoroutine(coroutine);
        }
    }
}