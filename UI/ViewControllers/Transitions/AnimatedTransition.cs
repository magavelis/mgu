﻿using UnityEngine.Events;
using MGU.Common;

namespace MGU.UI {
    public abstract class AnimatedTransition {
        protected readonly ViewController OwnerController;
        protected readonly float AnimationDuration;
        protected readonly CoroutineQueue Queue;

        /// <summary>
        /// Optional. May be used to simulate animation between this and `OwnerController`
        /// </summary>
        protected ViewController DependencyController;

        protected AnimatedTransition(ViewController viewController, float animationDuration = -1.0f) {
            OwnerController = viewController;
            AnimationDuration = animationDuration;
            Queue = CoroutineQueue.Create(viewController.gameObject);
        }

        public void SetDependencyController(ViewController viewController) {
            DependencyController = viewController;
        }

        protected virtual float TransitionDuration() {
            return (AnimationDuration < 0.0f) ? K.Animation.DefaultDuration : AnimationDuration;
        }
        public abstract void DoPush(UnityAction onComplete);
        public abstract void DoPop(UnityAction onComplete);
    }
}