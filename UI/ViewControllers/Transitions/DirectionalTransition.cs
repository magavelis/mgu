﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace MGU.UI {
    public class DirectionalTransition: AnimatedTransition {
        public enum Direction {
            Up,
            Down,
            Left,
            Right
        }

        private readonly Direction transitionDirection;
        private readonly Func<float, float> ease;
        
        public DirectionalTransition(
                ViewController viewController,
                Direction transitionDirection,
                float animationDuration,
                Func<float, float> ease): base(viewController, animationDuration) {
            this.transitionDirection = transitionDirection;
            this.ease = ease;
        }

        public override void DoPush(UnityAction onComplete) {
            OwnerController.View.Rect.Origin = PushStartingOrPopEndingPositionByDirection(transitionDirection);
            Vector2 destination = (DependencyController.IsNotNull())
                    ? DependencyController.View.Rect.Origin
                    : Vector2.zero;
            MyCoroutine coroutine = new MyCoroutine(
                    OwnerController.View.Move(
                            destination,
                            TransitionDuration(),
                            ease,
                            null)
            );

            if (DependencyController.IsNotNull()) {
                coroutine.AddCoroutine(
                        DependencyController.View.Move(
                                GetDependencyControllerPushPositionByDirection(transitionDirection),
                                TransitionDuration(),
                                ease,
                                null
                        )
                );
            }

            Queue.OnQueueComplete.AddListenerSafe(onComplete);
            Queue.AddCoroutine(coroutine);
        }

        public override void DoPop(UnityAction onComplete) {
            MyCoroutine coroutine = new MyCoroutine(
                    OwnerController.View.Move(
                            PushStartingOrPopEndingPositionByDirection(transitionDirection),
                            TransitionDuration(),
                            ease,
                            null)
            );

            if (DependencyController.IsNotNull()) {
                coroutine.AddCoroutine(
                        DependencyController.View.Move(
                                OwnerController.View.Rect.Origin,
                                TransitionDuration(),
                                ease,
                                null
                        )
                );
            }

            Queue.OnQueueComplete.AddListenerSafe(onComplete);
            Queue.AddCoroutine(coroutine);
        }

        // MARK: - Helpers
        private Vector2 PushStartingOrPopEndingPositionByDirection(Direction direction) {
            Vector2 position;
            switch (direction) {
            case Direction.Left:
                position = new Vector2(OwnerController.View.Rect.Width, OwnerController.View.Rect.Y);
                break;

            case Direction.Right:
                position = new Vector2(-OwnerController.View.Rect.Width, OwnerController.View.Rect.Y);
                break;

            case Direction.Up:
                position = new Vector2(OwnerController.View.Rect.X, -OwnerController.View.Rect.Height);
                break;

            case Direction.Down:
                position = new Vector2(OwnerController.View.Rect.X, OwnerController.View.Rect.Height);
                break;

            default:
                position = new Vector2(OwnerController.View.Rect.X, OwnerController.View.Rect.Y);
                break;
            }

            return position;
        }

        private Vector2 GetDependencyControllerPushPositionByDirection(Direction direction) {
            Vector2 position;
            switch (direction) {
            case Direction.Left:
                position = new Vector2(-OwnerController.View.Rect.Width, OwnerController.View.Rect.Y);
                break;

            case Direction.Right:
                position = new Vector2(OwnerController.View.Rect.Width, OwnerController.View.Rect.Y);
                break;

            case Direction.Up:
                position = new Vector2(OwnerController.View.Rect.X, OwnerController.View.Rect.Height);
                break;

            case Direction.Down:
                position = new Vector2(OwnerController.View.Rect.X, -OwnerController.View.Rect.Height);
                break;

            default:
                position = new Vector2(OwnerController.View.Rect.X, OwnerController.View.Rect.Y);
                break;
            }

            return position;
        }
    }
}