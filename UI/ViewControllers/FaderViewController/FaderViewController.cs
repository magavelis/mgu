﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace MGU.UI {
    public class FaderViewController: ViewController, IPointerClickHandler, IDragHandler {
        private enum FadeTransition {
            TransitionIn,
            TransitionOut
        }

        public class FaderClickEvent: UnityEvent { }
        public class FaderDragEvent: UnityEvent { }

        // MARK: - Constants
        public const float defaultFadeAnimationDuration = 1f;

        // MARK: - Declarations
        [SerializeField] public Canvas canvas;

        public int SortingOrder {
            set => canvas.sortingOrder = value;
        }

        public Color Color {
            get => View.BackgroundColor;
            set => View.BackgroundColor = value;
        }

        public float AnimationDuration {
            set => durationCalculator = new DurationCalculator(value, 0.0f, 1.0f);
        }

        private readonly FaderClickEvent onClick = new FaderClickEvent();
        private readonly FaderDragEvent onDrag = new FaderDragEvent();

        private GraphicRaycaster graphicRaycaster;
        private DurationCalculator durationCalculator;
        private CoroutineQueue queue;

        protected override void Awake() {
            base.Awake();
            graphicRaycaster = GetComponent<GraphicRaycaster>();
        }

        // MARK: - Override ViewController
        protected override void ViewDidLoad() {
            base.ViewDidLoad();
            durationCalculator = new DurationCalculator(defaultFadeAnimationDuration, 0.0f, 1.0f);
            queue = CoroutineQueue.Create(gameObject);

            Hide(false, null);
        }

        protected override void ViewDidLayout() {
            base.ViewDidLayout();

            canvas.overrideSorting = true;
            canvas.sortingLayerName = Window.Main.SortingLayerName;
        }

        protected override void Deinit() {
            base.Deinit();

            RemoveEventListeners();
        }

        // MARK: - EventsSystem
        public void OnPointerClick(PointerEventData eventData) {
            onClick?.Invoke();
        }

        public void OnDrag(PointerEventData eventData) {
            if (Mathf.Abs(eventData.delta.x) < 5.0) {
                return;
            }

            onDrag?.Invoke();
        }

        // MARK: - Visibility Helpers
        public void Show(
                bool animated,
                UnityAction onComplete,
                UnityAction onInteraction) {
            transform.localPosition = Vector3.zero;

            graphicRaycaster.enabled = true;
            if (animated) {
                MakeFadeTransition(FadeTransition.TransitionIn, onComplete);
            } else {
                View.Alpha = 1;
                onComplete?.Invoke();
            }

            onClick.AddListenerSafe(onInteraction);
            onDrag.AddListenerSafe(onInteraction);

            // WORKAROUND: Disabling game input, when any kind of fader is visible
            // InputController.IsInputEnabled = false;
        }

        public void Hide(bool animated, UnityAction onComplete) {
            graphicRaycaster.enabled = false;
            if (animated) {
                MakeFadeTransition(FadeTransition.TransitionOut, onComplete);
            } else {
                View.Alpha = 0;
                onComplete?.Invoke();
            }

            RemoveEventListeners();

            // WORKAROUND: Enabling game input, when fader is hiding or hidden
            // InputController.IsInputEnabled = true;
        }

        // MARK: - Helpers
        private void MakeFadeTransition(FadeTransition transition, UnityAction onComplete) {
            float currentAlpha = View.Alpha;
            float duration;
            float value;

            if (transition == FadeTransition.TransitionIn) {
                duration = durationCalculator.EnterDuration(currentAlpha);
                value = 1.0f;
            } else {
                duration = durationCalculator.ExitDuration(currentAlpha);
                value = 0.0f;
            }

            MyCoroutine coroutine = new MyCoroutine(
                    View.FadeTo(
                            value,
                            0f,
                            duration,
                            Easing.Linear,
                            null)
            );
            
            queue.OnQueueComplete.AddListenerSafe(onComplete);
            queue.CancelAllCoroutines();
            queue.AddCoroutine(coroutine);
        }

        private void RemoveEventListeners() {
            onClick.RemoveAllListeners();
            onDrag.RemoveAllListeners();
        }
    }
}