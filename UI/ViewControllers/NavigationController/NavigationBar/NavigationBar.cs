﻿using UnityEngine;
using MGU.Common;

namespace MGU.UI {
    public class NavigationBar: View {
        // MARK: - Declarations
        [SerializeField] private float navigationBarHeight = K.UI.NavigationBarHeight;

        [SerializeField] private View safeAreaView = null;
        [SerializeField] private View backgroundView = null;
        [SerializeField] private View leftView = null;
        [SerializeField] private View rightView = null;
        [SerializeField] private View centerView = null;
        [SerializeField] private Canvas canvas = null;

        public int SortingOrder {
            set => canvas.sortingOrder = value;
        }

        protected override void Awake() {
            base.Awake();
            Rect.SetAnchor(AnchorPosition.StretchTop);
            Rect.Height = navigationBarHeight;
        }

        protected override void Start() {
            base.Start();
            canvas.overrideSorting = true;
        }

        // MARK: - Overriding View
        public override void Layout() {
            if (safeAreaView != null) {
                EdgeInsets edgeInsets = ScreenUtils.SafeArea;
                safeAreaView.Rect.SetAnchor(AnchorPosition.StretchTop);
                safeAreaView.Rect.EdgeInsets = new EdgeInsets(-edgeInsets.Top, 0, 0, 0);

                Rect.Y = -edgeInsets.Top;

                backgroundView.Rect.SetAnchor(AnchorPosition.StretchTop);
                backgroundView.Rect.Y = safeAreaView.Rect.Y;
                backgroundView.Rect.Height = Rect.Height + safeAreaView.Rect.Height;
            }

            base.Layout();
        }

        // MARK: - Methods
        public void SetLeftView(View view) {
            leftView.AddSubview(view);
        }

        public void SetRightView(View view) {
            rightView.AddSubview(view);
        }

        public void SetCenterView(View view) {
            centerView.AddSubview(view);
        }

        public virtual MyCoroutine AppearCoroutine(bool animated) {
            gameObject.SetActive(true);
            return new MyCoroutine(
                    this.MoveVertically(
                            -ScreenUtils.SafeArea.Top,
                            (animated) ? K.Animation.DefaultDuration : 0.0f,
                            Easing.Exponential.EaseInOut,
                            null));
        }

        public virtual MyCoroutine DisappearCoroutine(bool animated) {
            return new MyCoroutine(
                    this.MoveVertically(
                            navigationBarHeight,
                            (animated) ? K.Animation.DefaultDuration : 0.0f,
                            Easing.Exponential.EaseInOut,
                            () => {
                                gameObject.SetActive(false);
                            }));
        }
    }
}