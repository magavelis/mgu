﻿using UnityEngine;

namespace MGU.UI {
    public class ToolBar: View {
        // Constants
        private const float toolBarHeight = 175f;

        // Declarations
        [SerializeField] private View safeAreaView = null;
        [SerializeField] private Canvas canvas = null;

        public int SortingOrder {
            set => canvas.sortingOrder = value;
        }

        // MARK: - Methods
        protected override void Awake() {
            base.Awake();
            Rect.SetAnchor(AnchorPosition.StretchBottom);
            Rect.Height = toolBarHeight;
        }

        protected override void Start() {
            base.Start();
            canvas.overrideSorting = true;
        }

        // MARK: - Overriding ASDView
        public override void Layout() {
            if (safeAreaView != null) {
                EdgeInsets edgeInsets = ScreenUtils.SafeArea;
                safeAreaView.Rect.EdgeInsets = new EdgeInsets(0, -edgeInsets.Bottom, 0, 0);
                Rect.Y = edgeInsets.Bottom;
            }

            base.Layout();
        }
    }
}