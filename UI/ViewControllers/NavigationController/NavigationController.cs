﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace MGU.UI {
    public class NavigationController: ViewController {
        public NavigationBar NavigationBar;
        public ToolBar ToolBar;

        public SortingOrder SortingOrder {
            set {
                if (NavigationBar.IsNotNull()) {
                    NavigationBar.SortingOrder = value.NavigationBar;
                }

                if (ToolBar.IsNotNull()) {
                    ToolBar.SortingOrder = value.ToolBar;
                }
            }
        }

        private CoroutineQueue queue;

        public List<ViewController> ViewControllers { get; private set; } = new List<ViewController>();

        public ViewController TopViewController => ViewControllers.LastOrDefault();

        public ViewController RootViewController {
            set {
                if (value == null) {
                    return;
                }

                SetViewControllers(new List<ViewController> { value });
            }
        }

        protected override void Awake() {
            base.Awake();

            queue = CoroutineQueue.Create(gameObject);
        }

        public override AnimatedTransition GetTransition() {
            return TopViewController.GetTransition() ?? base.GetTransition();
        }

        public override void Dismiss(bool animated, UnityAction onComplete) {
            if (PresentationStyle != PresentationStyle.Presented) {
                Debug.LogError("Can't dismiss not presented `NavigationController`");
                return;
            }

            UnityAction finalize = () => {
                ViewControllers.ForEach(viewController => viewController.DidMoveToParentViewController(null));
                DidMoveToParentViewController(null);
                ParentViewController.DidMoveToParentViewController(ParentViewController.ParentViewController);
                RemoveFromParentViewController();
                onComplete?.Invoke();
            };

            ViewControllers.ForEach(viewController => {
                viewController.SetPresentationStyle(PresentationStyle.None);
                viewController.WillMoveToParentViewController(null);
            });
            SetPresentationStyle(PresentationStyle.None);
            WillMoveToParentViewController(null);
            ParentViewController.WillMoveToParentViewController(ParentViewController.ParentViewController);

            AnimatedTransition transition = TopViewController.GetTransition() ?? GetTransition();
            if (animated && transition != null) {
                transition.DoPop(finalize);
            } else {
                finalize.Invoke();
            }
        }

        public void Push(ViewController viewController, bool animated) {
            List<ViewController> newViewControllers = new List<ViewController>(ViewControllers) { viewController };

            SetViewControllers(newViewControllers, animated);
        }

        public void Pop(bool animated) {
            if (ViewControllers.Count == 1) {
                Debug.LogWarning("Unable to pop the last controller");
                return;
            }

            ViewController currentController = TopViewController;
            ViewController previousController = ViewControllers.ElementBeforeOrDefault(currentController);
            if (currentController == null || previousController == null || previousController == currentController) {
                return;
            }

            currentController.SetPresentationStyle(PresentationStyle.None);
            currentController.WillMoveToParentViewController(null);
            previousController.WillMoveToParentViewController(this);
            ViewControllers.Remove(currentController);
            
            UnityAction finalize = () => {
                currentController.DidMoveToParentViewController(null);
                previousController.DidMoveToParentViewController(this);
                currentController.RemoveFromParentViewController();
            };

            if (animated) {
                AnimatedTransition transition = currentController.GetTransition();
                transition.SetDependencyController(previousController);
                transition.DoPop(finalize);
            } else {
                finalize.Invoke();
            }
        }

        public void SetViewControllers(ViewController[] newViewControllers, bool animated = false) {
            List<ViewController> viewControllerList = new List<ViewController>(newViewControllers);
            SetViewControllers(viewControllerList, animated);
        }

        public void SetViewControllers(List<ViewController> newViewControllers, bool animated = false) {
            ViewController newTopViewController = newViewControllers.LastOrDefault();
            if (newTopViewController == null) {
                Debug.LogError("Can't set view controllers, when they are not provided");
                return;
            }

            ViewController oldTopViewController = ViewControllers.LastOrDefault();
            List<ViewController> unwantedViewControllers = ViewControllers.Except(newViewControllers).ToList();

            List<ViewController> wantedViewControllers = newViewControllers.Except(ViewControllers).ToList();
            foreach (ViewController controller in unwantedViewControllers) {
                controller.SetPresentationStyle(PresentationStyle.None);
                controller.WillMoveToParentViewController(null);
            }

            foreach (ViewController controller in wantedViewControllers) {
                controller.NavigationController = this;
                controller.SetPresentationStyle(PresentationStyle.Pushed);
                controller.WillMoveToParentViewController(this);
            }

            if (oldTopViewController != newTopViewController) {
                // TODO: Think of a better way to handle old view controller life cycle events on push
                if (oldTopViewController != null && unwantedViewControllers.Contains(oldTopViewController) == false) {
                    oldTopViewController.WillMoveToParentViewController(null);
                }

                UnityAction finalize = () => {
                    if (oldTopViewController != null &&
                        unwantedViewControllers.Contains(oldTopViewController) == false) {
                        oldTopViewController.DidMoveToParentViewController(null);
                    }

                    newTopViewController.DidMoveToParentViewController(this);
                    foreach (ViewController controller in unwantedViewControllers) {
                        controller.DidMoveToParentViewController(null);
                        controller.RemoveFromParentViewController();
                    }
                };

                AnimatedTransition transition = newTopViewController.GetTransition();
                if (animated == false || transition == null) {
                    finalize.Invoke();
                } else {
                    transition.SetDependencyController(oldTopViewController);
                    transition.DoPush(() => { finalize.Invoke(); });
                }
            }

            ViewControllers = newViewControllers;
        }

        // MARK: - Navigation Bar Helpers
        public void SetNavigationBar(NavigationBar newNavigationBar) {
            if (newNavigationBar.IsNotNull()) {
                View.AddSubview(newNavigationBar);
            }

            if (NavigationBar.IsNotNull()) {
                Destroy(NavigationBar.gameObject);
            }

            NavigationBar = newNavigationBar;
        }

        public void SetNavigationBarHidden(bool isHidden, bool animated) {
            MyCoroutine coroutine = (isHidden)
                    ? NavigationBar.DisappearCoroutine(animated)
                    : NavigationBar.AppearCoroutine(animated);

            queue.AddCoroutine(coroutine);
        }

        public bool IsNavigationBarHidden() {
            return NavigationBar.gameObject.activeSelf;
        }

        // MARK: - Tool Bar Helpers
        public void SetToolBar(ToolBar newToolBar) {
            if (newToolBar.IsNotNull()) {
                View.AddSubview(newToolBar);
            }

            if (ToolBar.IsNotNull()) {
                Destroy(ToolBar.gameObject);
            }

            ToolBar = newToolBar;
        }
    }
}