﻿using UnityEngine;
using UnityEngine.Events;

namespace MGU.UI {
    public class DrawerController: ViewController {
        // MARK: - Constants
        /// <summary>
        /// The time drawer will take to open/close right or left size.
        /// </summary>
        private const float defaultDrawerSlideAnimationDuration = 0.4f;

        private const float defaultDrawerFaderAnimationDuration = 1.2f;

        /// <summary>
        /// Drawer size in percents. 
        /// 1 - Maximum size. Will occupy a full screen. 
        /// 0 - Minimum size. Won't be visible on screen.
        /// </summary>
        private const float defaultDrawerSize = 0.8f;

        // MARK: - Declarations
        public Color FaderColor = new Color(0.0f, 0.0f, 0.0f, 0.5f);

        public View LeftView;
        public View RightView;
        public View CenterView;

        public event UnityAction OnDrawerHideStart;
        public event UnityAction OnDrawerHideComplete;

        private Window window;

        private ViewController leftViewController;

        private ViewController LeftViewController {
            get => leftViewController;
            set {
                leftViewController = value;

                float leading = (RootCanvasWidth - RootCanvasWidth * defaultDrawerSize);
                leftViewController.View.Rect.EdgeInsets = new EdgeInsets(0, 0, leading, 0);
            }
        }

        private ViewController rightViewController;

        private ViewController RightViewController {
            get => rightViewController;
            set {
                rightViewController = value;

                float trailing = (RootCanvasWidth - RootCanvasWidth * defaultDrawerSize);
                rightViewController.View.Rect.EdgeInsets = new EdgeInsets(0, 0, 0, trailing);
            }
        }
        private ViewController CenterViewController { get; set; }
        [SerializeField] private FaderViewController faderViewController = null;

        private float RootCanvasWidth {
            get {
                RectTransform rect = window.Rect;
                Vector2 sizeDelta = rect.sizeDelta;
                return sizeDelta.x;
            }
        }

        private DurationCalculator durationCalculator;
        private CoroutineQueue queue;

        // MARK: - Override ViewController
        protected override void ViewDidLoad() {
            base.ViewDidLoad();

            durationCalculator = new DurationCalculator(defaultDrawerSlideAnimationDuration,
                                                        0.0f,
                                                        -RootCanvasWidth * defaultDrawerSize);

            queue = CoroutineQueue.Create(gameObject);

            SetUpFaderViewController();
            UpdateLayouts();

            if (LeftViewController != null) {
                LeftView.BackgroundColor = LeftViewController.View.BackgroundColor;
                LeftViewController.SetPresentationStyle(PresentationStyle.None);
                LeftViewController.WillMoveToParentViewController(this, LeftView);
                LeftViewController.DidMoveToParentViewController(this);
            }

            if (RightViewController != null) {
                RightView.BackgroundColor = RightViewController.View.BackgroundColor;
                RightViewController.SetPresentationStyle(PresentationStyle.None);
                RightViewController.WillMoveToParentViewController(this, RightView);
                RightViewController.DidMoveToParentViewController(this);
            }

            if (CenterViewController != null) {
                CenterView.BackgroundColor = CenterViewController.View.BackgroundColor;
                CenterViewController.SetPresentationStyle(PresentationStyle.None);
                CenterViewController.WillMoveToParentViewController(this, CenterView);
                CenterViewController.DidMoveToParentViewController(this);
            }
        }

        // MARK: - Methods
        public void SetWindow(Window window) {
            this.window = window;
        }
        
        public void SetViewControllers(
                ViewController center,
                ViewController left = null,
                ViewController right = null) {
            if (center != null) {
                center.DrawerController = this;
                CenterViewController = center;
            }

            if (left != null) {
                left.DrawerController = this;
                LeftViewController = left;
            }

            if (right != null) {
                right.DrawerController = this;
                RightViewController = right;
            }
        }

        public void OpenLeftDrawer() {
            float position = RootCanvasWidth * defaultDrawerSize;
            float duration = durationCalculator.EnterDuration(View.Rect.X);
            MyCoroutine coroutine = new MyCoroutine(
                    View.MoveHorizontally(position,
                            duration,
                            new Easing.Back().EaseOut,
                            null)
            );
            queue.SkipAllCoroutines();
            queue.AddCoroutine(coroutine);

            ShowShadow(RenderOrder.AboveBars, true, CloseDrawers);
        }

        public void OpenRightDrawer() {
            float position = -RootCanvasWidth * defaultDrawerSize;
            float duration = durationCalculator.EnterDuration(View.Rect.X);
            MyCoroutine coroutine = new MyCoroutine(
                    View.MoveHorizontally(
                            position,
                            duration,
                            new Easing.Back().EaseOut,
                            null)
            );

            queue.SkipAllCoroutines();
            queue.AddCoroutine(coroutine);

            ShowShadow(RenderOrder.AboveToolBar, true, CloseDrawers);
        }

        public void CloseDrawers() {
            OnDrawerHideStart?.Invoke();

            float duration = durationCalculator.ExitDuration(View.Rect.X);
            MyCoroutine coroutine = new MyCoroutine(
                    View.MoveHorizontally(0,
                            duration,
                            new Easing.Back().EaseOut,
                            null)
            );

            queue.SkipAllCoroutines();
            queue.OnQueueComplete.AddListenerSafe(OnDrawerHideComplete);
            queue.AddCoroutine(coroutine);

            HideShadow(true);
        }

        public void ShowShadow(RenderOrder renderOrder, bool animated, UnityAction onInteraction) {
            SortingOrder sortingOrder = new SortingOrder(renderOrder);
            NavigationController.SortingOrder = sortingOrder;
            faderViewController.SortingOrder = sortingOrder.FaderViewController;
            faderViewController.Show(animated, null, onInteraction);
        }

        public void HideShadow(bool animated) {
            faderViewController.Hide(animated,null);
        }

        // MARK: - Helpers
        private void SetUpFaderViewController() {
            faderViewController.Color = FaderColor;
            faderViewController.AnimationDuration = defaultDrawerFaderAnimationDuration;
        }

        private void UpdateLayouts() {
            float width = RootCanvasWidth;
            LeftView.Rect.X = -width;
            RightView.Rect.X = width;
            faderViewController.View.Rect.EdgeInsets = new EdgeInsets(0, 0, -width, -width);
        }
    }
}