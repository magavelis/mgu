﻿using System.Collections.Generic;
using UnityEngine.Events;

namespace MGU.UI {
    public interface IAlertActionConfigurable {
        string ActionName { get; }
        UnityAction Action { get; }
    }

    public class CancelActionConfiguration: IAlertActionConfigurable {
        public string ActionName { get; }
        public UnityAction Action { get; }
        
        public CancelActionConfiguration() {
            ActionName = Localizable.Cancel;
            Action = null;
        }
    }
    
    public class OkActionConfiguration: IAlertActionConfigurable {
        public string ActionName { get; }
        public UnityAction Action { get; }
        
        public OkActionConfiguration() {
            ActionName = Localizable.Ok;
            Action = null;
        }
    }

    public class AlertActionConfiguration: IAlertActionConfigurable {
        public string ActionName { get; }
        public UnityAction Action { get; }

        public AlertActionConfiguration(string actionName, UnityAction actionHandler) {
            ActionName = actionName;
            Action = actionHandler;
        }
    }

    public class AlertDisplayConfiguration {
        public readonly string Title;
        public readonly string Description;

        private readonly List<IAlertActionConfigurable> actionList = new List<IAlertActionConfigurable>();

        public AlertDisplayConfiguration(string title, string description) {
            Title = title;
            Description = description;
        }

        public void AddAction(string title, UnityAction action) {
            AlertActionConfiguration configuration = new AlertActionConfiguration(
                    title,
                    action
            );
            
            AddAction(configuration);
        }
        
        public void AddAction(IAlertActionConfigurable configuration) {
            actionList.Add(configuration);
        }

        public IAlertActionConfigurable ActionAt(int index) {
            if (index < 0) {
                return null;
            }

            return index >= actionList.Count ? null : actionList[index];
        }
    }
}