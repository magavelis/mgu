﻿using UnityEngine;

namespace MGU.UI {
    public class AlertViewController: ViewController {
        // MARK: - Declarations
        [SerializeField] private View backgroundView = null;
        [SerializeField] private View contentView = null;
        [SerializeField] private LabelView titleLabelView = null;
        [SerializeField] private LabelView descriptionLabelView = null;
        [SerializeField] private ButtonView[] buttons = null;

        private AlertDisplayConfiguration configuration;

        // MARK: - Methods
        // MARK: - Overriding ViewController
        public override AnimatedTransition GetTransition() {
            return new AlertTransition(
                    this,
                    backgroundView,
                    contentView);
        }

        // MARK: - Helpers
        public void Construct(AlertDisplayConfiguration displayConfiguration) {
            configuration = displayConfiguration;
            titleLabelView.Text = configuration.Title;
            descriptionLabelView.Text = configuration.Description;

            int buttonsCount = buttons.Length;
            for (int i = 0; i < buttonsCount; i++) {
                ButtonView button = buttons[i];
                IAlertActionConfigurable actionConfiguration = configuration.ActionAt(i);
                if (actionConfiguration == null) {
                    button.gameObject.SetActive(false);
                } else {
                    button.Title = actionConfiguration.ActionName;
                    button.OnClick.AddListenerSafe(actionConfiguration.Action);
                    button.OnClick.AddListener(Dismiss);
                }
            }
        }

        private void Dismiss() {
            Dismiss(true);
        }
    }
}