﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace MGU.UI {
    public enum PresentationStyle {
        None,
        Pushed,
        Presented
    }

    public partial class ViewController: MonoBehaviour {
        // MARK: - Declarations
        private bool isViewLoaded;

        [SerializeField] private View view;

        public View View {
            get {
                LoadViewIfNecessary();
                return view;
            }
            set => view = value;
        }

        protected PresentationStyle PresentationStyle { get; private set; } = PresentationStyle.None;

        [HideInInspector] public NavigationController NavigationController;
        [HideInInspector] public DrawerController DrawerController;

        [HideInInspector] public ViewController ParentViewController;
        private event UnityAction OnViewWillAppear;
        private bool wasViewWillAppearCalled;

        private event UnityAction OnViewDidAppear;
        private bool wasViewDidAppearCalled;

        private Canvas presentationCanvas;

        // MARK: - Methods
        protected virtual void Awake() {
            StartMonitoringLocalizationEvents();
        }

        protected virtual void OnDestroy() {
            StopMonitoringLocalizationEvents();
            UnloadView();
            Deinit();
        }

        // MARK: - Overridable
        /// <summary>
        /// Do not access `View` property on this method, otherwise it will be recreated
        /// </summary>
        protected virtual void Deinit() {
            // Debug.Log(GetType().Name + " [Deinit] " + GetInstanceID());
        }

        protected virtual void ViewDidLoad() {
            // Debug.Log(GetType().Name + " [ViewDidLoad] " + GetInstanceID());
        }

        protected virtual void ViewWillAppear() {
            // Debug.Log(GetType().Name + " [ViewWillAppear] " + GetInstanceID());
            wasViewWillAppearCalled = true;
            OnViewWillAppear?.Invoke();
            if (ParentViewController.IsNotNull()) {
                ParentViewController.OnViewWillAppear -= ViewWillAppear;
            }
        }

        protected virtual void ViewDidAppear() {
            Debug.Log(GetType().Name + " [ViewDidAppear] " + GetInstanceID());
            wasViewDidAppearCalled = true;
            OnViewDidAppear?.Invoke();
            if (ParentViewController.IsNotNull()) {
                ParentViewController.OnViewDidAppear -= ViewDidAppear;
            }
        }

        protected virtual void ViewWillDisappear() {
            Debug.Log(GetType().Name + " [ViewWillDisappear] " + GetInstanceID());
        }

        protected virtual void ViewDidDisappear() {
            Debug.Log(GetType().Name + " [ViewDidDisappear] " + GetInstanceID());
        }

        protected virtual void ViewDidLayout() {
            // May be called multiple times, so we will not log anything
            if (PresentationStyle == PresentationStyle.Presented) {
                presentationCanvas.overrideSorting = true;
            }
        }

        // MARK: - Lifecycle Controls
        public void SetPresentationStyle(PresentationStyle presentationStyle) {
            PresentationStyle = presentationStyle;
            SetUpPresentationStyle();
        }

        public void WillMoveToParentViewController(ViewController parentController, View overridenView = null) {
            if (parentController == null) {
                ViewWillDisappear();
                return;
            }

            View parentView = overridenView ? overridenView : parentController.View;
            if (View != null) {
                if (parentController.wasViewWillAppearCalled) {
                    ViewWillAppear();
                } else {
                    parentController.OnViewWillAppear += ViewWillAppear;
                }

                parentView.AddSubview(View);
                ViewDidLayout();
            }

            ParentViewController = parentController;
        }

        public void DidMoveToParentViewController(ViewController parentController) {
            if (parentController == null) {
                ViewDidDisappear();
                return;
            }

            if (parentController.wasViewDidAppearCalled) {
                ViewDidAppear();
            } else {
                parentController.OnViewDidAppear += ViewDidAppear;
            }

            ParentViewController = parentController;
        }

        public void RemoveFromParentViewController() {
            Destroy(gameObject);
        }

        public void MakeVisible() {
            ViewDidLayout();
            ViewWillAppear();
            ViewDidAppear();
        }

        // MARK: - Presenting
        public virtual AnimatedTransition GetTransition() {
            return new FadeTransition(this);
        }

        public virtual bool ShouldOverrideTransition() {
            return false;
        }

        public void Present(ViewController viewController, bool animated, UnityAction onComplete) {
            UnityAction finalize = () => {
                DidMoveToParentViewController(null);
                viewController.DidMoveToParentViewController(this);
                onComplete?.Invoke();
            };

            viewController.SetPresentationStyle(PresentationStyle.Presented);
            WillMoveToParentViewController(null);
            viewController.WillMoveToParentViewController(this);

            AnimatedTransition transition = viewController.GetTransition();
            if (animated && transition != null) {
                transition.DoPush(finalize);
            } else {
                finalize.Invoke();
            }
        }

        public virtual void Dismiss(bool animated, UnityAction onComplete = null) {
            // TODO: Fix bug when dismissing does not call topviewcontroller lifecycle events
            if (NavigationController == null) {
                if (PresentationStyle != PresentationStyle.Presented) {
                    Debug.LogError("Can't dismiss not presented `ViewController`");
                    return;
                }

                UnityAction finalize = () => {
                    DidMoveToParentViewController(null);
                    ParentViewController.DidMoveToParentViewController(ParentViewController.ParentViewController);
                    RemoveFromParentViewController();
                    onComplete?.Invoke();
                };

                SetPresentationStyle(PresentationStyle.None);
                WillMoveToParentViewController(null);
                ParentViewController.WillMoveToParentViewController(ParentViewController.ParentViewController);

                AnimatedTransition transition = GetTransition();
                if (animated && transition != null) {
                    transition.DoPop(finalize);
                } else {
                    finalize.Invoke();
                }
            } else {
                NavigationController.Dismiss(animated, onComplete);
            }
        }

        // MARK: - Helpers
        private void LoadViewIfNecessary() {
            if (isViewLoaded) {
                return;
            }

            LoadView();
        }

        private void LoadView() {
            view.Transform = transform;
            view.OnViewLayout += ViewDidLayout;
            isViewLoaded = true;
            ViewDidLoad();
        }

        private void UnloadView() {
            if (view != null) {
                view.OnViewLayout -= ViewDidLayout;
            }

            isViewLoaded = false;
        }

        private void SetUpPresentationStyle() {
            if (PresentationStyle == PresentationStyle.Presented) {
                presentationCanvas = gameObject.GetComponent<Canvas>();
                if (presentationCanvas == null) {
                    presentationCanvas = gameObject.AddComponent<Canvas>();
                }

                presentationCanvas.sortingOrder = SortingOrder.PresentedViewController;
                presentationCanvas.sortingLayerName = Window.Main.SortingLayerName;

                gameObject.AddComponent<GraphicRaycaster>();
            }
        }

        #region - Localizations
        partial void StartMonitoringLocalizationEvents();
        partial void StopMonitoringLocalizationEvents();
        #endregion
    }
}