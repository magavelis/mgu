﻿using UnityEngine;
using UnityEngine.Events;
using MGU;

namespace MGU.UI.Animations {
    public class AntiParallelMovementAnimator {
        private readonly CoroutineQueue queue;
        private readonly View firstView;
        private readonly View secondView;
        private readonly float animatableWidth;
        private readonly float centerStopOffset;

        private bool inProgress = false;

        public AntiParallelMovementAnimator(
                GameObject ownerObject,
                View firstView,
                View secondView,
                float animatableWidth,
                float centerStopOffset) {
            this.queue = CoroutineQueue.Create(ownerObject);
            this.firstView = firstView;
            this.secondView = secondView;
            this.animatableWidth = animatableWidth;
            this.centerStopOffset = centerStopOffset;
        }

        public void Animate(UnityAction onComplete) {
            if (inProgress) {
                return;
            }

            inProgress = true;

            MyCoroutine coroutine1 = new MyCoroutine(new[] {
                            firstView.MoveHorizontally(
                                    centerStopOffset,
                                    1,
                                    Easing.Exponential.EaseIn,
                                    null),
                            secondView.MoveHorizontally(
                                    -centerStopOffset,
                                    1,
                                    Easing.Exponential.EaseIn,
                                    null)
                    }
            );

            MyCoroutine coroutine2 = new MyCoroutine(new[] {
                            firstView.MoveHorizontally(
                                    -centerStopOffset,
                                    1,
                                    Easing.Linear,
                                    null),
                            secondView.MoveHorizontally(
                                    centerStopOffset,
                                    1,
                                    Easing.Linear,
                                    null)
                    }
            );

            MyCoroutine coroutine3 = new MyCoroutine(new[] {
                            firstView.MoveHorizontally(
                                    -animatableWidth,
                                    1,
                                    Easing.Exponential.EaseOut,
                                    null),
                            secondView.MoveHorizontally(
                                    animatableWidth,
                                    1,
                                    Easing.Exponential.EaseOut,
                                    null)
                    }
            );

            queue.AddCoroutine(coroutine1);
            queue.AddCoroutine(coroutine2);
            queue.AddCoroutine(coroutine3);
            queue.OnQueueComplete.AddListenerSafe(onComplete);
        }
    }
}