namespace MGU.Common {
    public partial class K {
        public partial class Tag {
            public const string Player = "Player";

            private Tag() {}
        }

        public partial class Math {
            public const float Tolerance = 0.01f;

            private Math() {}
        }
        
        public partial class Animation {
            public const float DefaultDuration = 0.25f;
            
            private Animation() {}
        }
        
        public partial class UI {
            public const float NavigationBarHeight = 175f;
            public const float DefaultFaderAnimationDuration = 1f;

            private UI() { }
        }

        public partial class SortingLayer {
            public const string Common = "Default";
            public const string Ui = "UI";
            
            private SortingLayer() { }
        }

        private K() { }
    }
}