namespace MGU.Common {
    #if UNITY_EDITOR
    public class FirebaseDefine: AssetDefine {
        public override string Name() {
            return "FIREBASE";
        }

        protected override string AssetFilename() {
            return "Firebase.App.dll.mdb";
        }
    }
    #endif
}