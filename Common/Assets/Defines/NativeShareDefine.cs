namespace MGU.Common {
    #if UNITY_EDITOR
    public class NativeShareDefine: AssetDefine {
        public override string Name() {
            return "NATIVE_SHARE";
        }

        protected override string AssetFilename() {
            return "NativeShare.cs";
        }
    }
    #endif
}