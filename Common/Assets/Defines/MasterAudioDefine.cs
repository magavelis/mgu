namespace MGU.Common {
    #if UNITY_EDITOR
    public class MasterAudioDefine: AssetDefine {
        public override string Name() {
            return "MASTER_AUDIO";
        }

        protected override string AssetFilename() {
            return "MasterAudio.cs";
        }
    }
    #endif
}