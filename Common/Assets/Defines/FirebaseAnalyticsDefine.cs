namespace MGU.Common {
    #if UNITY_EDITOR
    public class FirebaseAnalyticsDefine: AssetDefine {
        public override string Name() {
            return "FIREBASE_ANALYTICS";
        }

        protected override string AssetFilename() {
            return "Firebase.Analytics.dll.mdb";
        }
    }
    #endif
}