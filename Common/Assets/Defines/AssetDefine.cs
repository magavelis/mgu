using UnityEditor;
using System.IO;

namespace MGU.Common {
    #if UNITY_EDITOR
    public abstract class AssetDefine {
        public abstract string Name();
        protected abstract string AssetFilename();

        public bool IsValid() {
            string filter = Path.GetFileNameWithoutExtension(AssetFilename());
            string[] codes = AssetDatabase.FindAssets(filter);
            foreach (string code in codes) {
                string path = AssetDatabase.GUIDToAssetPath(code);
                string fileName = Path.GetFileName(path);
                if (fileName == AssetFilename()) {
                    return true;
                }
            }

            return false;
        }
    }
    #endif
}