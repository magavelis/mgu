﻿using System.Linq;
using System.Collections.Generic;
using UnityEditor;

namespace MGU.Common {
    #if UNITY_EDITOR
    [InitializeOnLoad]
    public partial class AssetsDefineManager: AssetPostprocessor {
        static partial void AdjustDefinesBasedOnProject(ref List<AssetDefine> defineList);

        static AssetsDefineManager() {
            UpdateProjectDefines();
        }

        [InitializeOnLoadMethod]
        private static void UpdateProjectDefines() {
            BuildTargetGroup targetGroup = EditorUserBuildSettings.selectedBuildTargetGroup;
            if (targetGroup == BuildTargetGroup.Unknown) {
                return;
            }

            List<AssetDefine> defineList = Defines();
            List<string> defineValueList = CurrentDefineValues(targetGroup);
            foreach (AssetDefine define in defineList) {
                if (defineValueList.Contains(define.Name()) && !define.IsValid()) {
                    defineValueList.Remove(define.Name());
                } else if (!defineValueList.Contains(define.Name()) && define.IsValid()) {
                    defineValueList.Add(define.Name());
                }
            }

            SetDefineValues(defineValueList, targetGroup);
        }

        private static List<AssetDefine> Defines() {
            List<AssetDefine> adjustedDefineList = new List<AssetDefine>();
            AdjustDefinesBasedOnProject(ref adjustedDefineList);
            return adjustedDefineList;
        }
        
        private static List<string> CurrentDefineValues(BuildTargetGroup targetGroup) {
            string definesString = PlayerSettings.GetScriptingDefineSymbolsForGroup(targetGroup).Trim();
            return definesString.Split(';').ToList();
        }

        private static void SetDefineValues(List<string> defineValueList, BuildTargetGroup targetGroup) {
            string definesString = string.Join(";", defineValueList.ToArray());
            PlayerSettings.SetScriptingDefineSymbolsForGroup(targetGroup, definesString);
        }
    }
    #endif
}